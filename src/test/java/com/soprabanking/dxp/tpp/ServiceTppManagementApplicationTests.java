package com.soprabanking.dxp.tpp;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * Class testing application startup
 */
@SpringBootTest
class ServiceTppManagementApplicationTests {

    @Test
    void contextLoads() {
    }
}