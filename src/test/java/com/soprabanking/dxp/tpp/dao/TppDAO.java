package com.soprabanking.dxp.tpp.dao;

import com.soprabanking.dxp.commons.error.DxpSbsFeedback;
import com.soprabanking.dxp.commons.test.Dao;
import com.soprabanking.dxp.tpp.model.entity.Tpp;
import com.soprabanking.dxp.tpp.model.entity.TppContact;
import com.soprabanking.dxp.tpp.model.entity.TppCredentials;
import com.soprabanking.dxp.tpp.model.entity.TppNationalAccreditation;
import com.soprabanking.dxp.tpp.model.entity.TppPhysicalContact;
import com.soprabanking.dxp.tpp.model.entity.TppState;
import kotlin.collections.SetsKt;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.soprabanking.dxp.commons.error.DxpBusinessCode.RESOURCE_NOT_FOUND;
import static com.soprabanking.dxp.commons.psd2.security.test.PersonaFactory.amumu;
import static com.soprabanking.dxp.commons.psd2.security.test.PersonaFactory.gragas;
import static com.soprabanking.dxp.commons.psd2.security.test.PersonaFactory.lux;
import static com.soprabanking.dxp.commons.psd2.security.test.PersonaFactory.veigar;
import static com.soprabanking.dxp.tpp.model.entity.CompetentAuthorityStatus.AUTHORISED;
import static com.soprabanking.dxp.tpp.model.entity.TppAccreditation.AIS;
import static com.soprabanking.dxp.tpp.model.entity.TppAccreditation.CIS;
import static com.soprabanking.dxp.tpp.model.entity.TppAccreditation.PIS;
import static com.soprabanking.dxp.tpp.model.entity.TppContactType.BUSINESS_CONTACT;
import static com.soprabanking.dxp.tpp.model.entity.TppContactType.TECHNICAL_CONTACT;
import static com.soprabanking.dxp.tpp.model.entity.TppStatus.ACTIVE;
import static com.soprabanking.dxp.tpp.model.entity.TppStatus.BLOCKED;
import static java.lang.String.format;
import static java.util.stream.Collectors.toList;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * DAO managing {@link Tpp}
 */
public class TppDAO extends Dao<Tpp> {

    public static final String tppIdBlocked = veigar.getTppInformation().getId();

    public static final String tppIdActiveWithoutAccreditations = gragas.getTppInformation().getId();

    public static final String tppIdActiveWithAccreditations = lux.getTppInformation().getId();

    public static final String tppIdActiveWithAccreditationsButNoClient = amumu.getTppInformation().getId();

    private static final Logger LOGGER = getLogger(TppDAO.class);

    TppDAO(MongoTemplate mongoTemplate) {
        super(Tpp.class, mongoTemplate);
    }

    private static List<Tpp> allTpps() {
        return Stream.of(createTppOralB(),
                         createTppActivia(),
                         createTppBookee(),
                         createTppSensodyne(),
                         createABCBank(),
                         createSolidarityBank(),
                         createDxpPlatformTpp(),
                         createDotaTpp(),
                         createNewTpp(),
                         createNewTppStandard(),
                         createNewTppEnable(),
                         createNewTppFalse(),
                         createNewTppDisable(),
                         createNewTppEmpty(),
                         createNewTppWithoutAccreditations(),
                         createNewTppBlocked())
                     .collect(toList());
    }

    public static Tpp createTppOralB() {
        return new Tpp()
                .withId(new ObjectId(tppIdBlocked))
                .setName("Oral B")
                .setComments("Oral B is a blocked TPP.")
                .setState(new TppState().setStatus(BLOCKED).setMonitored(true))
                .setAccreditations(new HashSet<>())
                .setCredentials(new TppCredentials().setClientId("PSDFR-BDE-ORALB").setSecret("123456")
                                                    .setTrustedRedirectUrls(SetsKt.setOf("api.redirectUrl.com"))
                                                    .setScopes(new HashSet<>()))
                .setWebSite("oralb.com")
                .setNationalAccreditationData(new TppNationalAccreditation().setOrganizationIdentifier("PSDFR-BDE-ORALB")
                                                                            .setCompetentAuthorityCountryCode("FR")
                                                                            .setCompetentAuthorityIdentifier("BDE")
                                                                            .setAuthorizationNumber("ORALB")
                                                                            .setCompetentAuthorityName("French registry")
                                                                            .setCompetentAuthorityStatus(AUTHORISED)
                                                                            .setRegistrationDate(LocalDate.of(2018, 2, 28))
                                                                            .setAccreditations(new HashSet<>()))
                .setContact(new TppContact().setCity("Paris")
                                            .setEmail("oralb@mail.com")
                                            .setPhoneNumber("0123456789")
                                            .setAddressLines(Arrays.asList("32 rue des Chicos.", "1 rue de la Carie."))
                                            .setCountry("France").setPostCode("4321"))
                .setPhysicalContacts(Arrays.asList(new TppPhysicalContact()
                                                           .setContactType(BUSINESS_CONTACT)
                                                           .setEmail("jeanmarc.morandini@oralb.com")
                                                           .setName("Morandini")
                                                           .setFirstName("Jean-marc")
                                                           .setJobTitle("Clown")
                                                           .setPhoneNumber("0123456789"),
                                                   new TppPhysicalContact()
                                                           .setContactType(TECHNICAL_CONTACT)
                                                           .setEmail("blake.lively@oralb.com")
                                                           .setName("Lively")
                                                           .setFirstName("Blake")
                                                           .setJobTitle("CTO")
                                                           .setPhoneNumber("0123456789")));
    }

    private static Tpp createTppActivia() {
        return new Tpp()
                .withId(new ObjectId(tppIdActiveWithoutAccreditations))
                .setName("Activia")
                .setComments("Activia is a TPP active without accreditations")
                .setState(new TppState().setStatus(ACTIVE).setMonitored(true))
                .setAccreditations(new HashSet<>())
                .setCredentials(new TppCredentials().setClientId("PSDFR-BDE-ACTIVIA").setSecret("123456")
                                                    .setTrustedRedirectUrls(SetsKt.setOf("api.redirectUrl.com"))
                                                    .setScopes(new HashSet<>()))
                .setWebSite("activia.com")
                .setNationalAccreditationData(new TppNationalAccreditation().setOrganizationIdentifier("PSDFR-BDE-ACTIVIA")
                                                                            .setCompetentAuthorityCountryCode("FR")
                                                                            .setCompetentAuthorityIdentifier("BDE")
                                                                            .setAuthorizationNumber("ACTIVIA")
                                                                            .setCompetentAuthorityName("French registry")
                                                                            .setCompetentAuthorityStatus(AUTHORISED)
                                                                            .setRegistrationDate(LocalDate.of(2018, 2, 28))
                                                                            .setAccreditations(new HashSet<>()))
                .setContact(new TppContact().setCity("Dunkerque")
                                            .setEmail("activia@mail.com")
                                            .setPhoneNumber("0123456789")
                                            .setAddressLines(Arrays.asList("1 rue de la Pie.", "2 boulevard de la legereté."))
                                            .setCountry("France").setPostCode("4321"))
                .setPhysicalContacts(Arrays.asList(new TppPhysicalContact()
                                                           .setContactType(BUSINESS_CONTACT)
                                                           .setEmail("othman.charolaise@activia.com")
                                                           .setName("Charolaise")
                                                           .setFirstName("Othman")
                                                           .setJobTitle("CEO")
                                                           .setPhoneNumber("0123456789"),
                                                   new TppPhysicalContact()
                                                           .setContactType(TECHNICAL_CONTACT)
                                                           .setEmail("angus.alex@activia.com")
                                                           .setName("Angus")
                                                           .setFirstName("Alex")
                                                           .setJobTitle("CTO")
                                                           .setPhoneNumber("0123456789")));
    }

    private static Tpp createTppSensodyne() {
        return new Tpp()
                .withId(new ObjectId(tppIdActiveWithAccreditations))
                .setName("Sensodyne")
                .setComments("Tpp active with all accreditations")
                .setState(new TppState().setStatus(ACTIVE).setMonitored(true))
                .setAccreditations(Stream.of(AIS, PIS, CIS).collect(Collectors.toSet()))
                .setCredentials(new TppCredentials().setClientId("PSDFR-BDE-SENSODYNE").setSecret("123456")
                                                    .setTrustedRedirectUrls(SetsKt.setOf("api.redirectUrl.com"))
                                                    .setScopes(Stream.of("openid", "aisp", "pisp", "cisp").collect(Collectors.toSet())))
                .setWebSite("sensodyne.com")
                .setNationalAccreditationData(new TppNationalAccreditation().setOrganizationIdentifier("PSDFR-BDE-SENSODYNE")
                                                                            .setCompetentAuthorityCountryCode("FR")
                                                                            .setCompetentAuthorityIdentifier("BDE")
                                                                            .setAuthorizationNumber("SENSODYNE")
                                                                            .setCompetentAuthorityName("French registry")
                                                                            .setCompetentAuthorityStatus(AUTHORISED)
                                                                            .setRegistrationDate(LocalDate.of(2018, 2, 28))
                                                                            .setAccreditations(
                                                                                    Stream.of(AIS, PIS, CIS).collect(Collectors.toSet())))
                .setContact(new TppContact().setCity("Nancy")
                                            .setEmail("sensodyne@mail.com")
                                            .setPhoneNumber("0123456789")
                                            .setAddressLines(Arrays.asList("24 boulevard de la Couronne", "12 rue de l'email"))
                                            .setCountry("France").setPostCode("4321"))
                .setPhysicalContacts(Arrays.asList(new TppPhysicalContact()
                                                           .setContactType(BUSINESS_CONTACT)
                                                           .setEmail("mabel.dentition@sensodyne.com")
                                                           .setName("Mabel")
                                                           .setFirstName("Dentition")
                                                           .setJobTitle("CEO")
                                                           .setPhoneNumber("0123456789"),
                                                   new TppPhysicalContact()
                                                           .setContactType(TECHNICAL_CONTACT)
                                                           .setEmail("jean.cive@sensodyne.com")
                                                           .setName("Jean")
                                                           .setFirstName("Cive")
                                                           .setJobTitle("CTO")
                                                           .setPhoneNumber("0123456789")));
    }

    private static Tpp createTppBookee() {
        return new Tpp()
                .withId(new ObjectId(tppIdActiveWithAccreditationsButNoClient))
                .setName("Bookee")
                .setComments("Tpp active with all accreditations but no client")
                .setState(new TppState().setStatus(ACTIVE).setMonitored(true))
                .setAccreditations(Stream.of(AIS, PIS, CIS).collect(Collectors.toSet()))
                .setCredentials(new TppCredentials().setClientId("PSDBE-BDE-BOOKEE").setSecret("123456")
                                                    .setTrustedRedirectUrls(SetsKt.setOf("api.redirectUrl.com"))
                                                    .setScopes(Stream.of("openid", "aisp", "pisp", "cisp").collect(Collectors.toSet())))
                .setWebSite("bookee.com")
                .setNationalAccreditationData(new TppNationalAccreditation().setOrganizationIdentifier("PSDBE-BDE-BOOKEE")
                                                                            .setCompetentAuthorityCountryCode("BE")
                                                                            .setCompetentAuthorityIdentifier("BDE")
                                                                            .setAuthorizationNumber("BOOKEE")
                                                                            .setCompetentAuthorityName("Belgium registry")
                                                                            .setCompetentAuthorityStatus(AUTHORISED)
                                                                            .setRegistrationDate(LocalDate.of(2018, 2, 28))
                                                                            .setAccreditations(
                                                                                    Stream.of(AIS, PIS, CIS).collect(Collectors.toSet())))
                .setContact(new TppContact().setCity("Liege")
                                            .setEmail("bookee@mail.com")
                                            .setPhoneNumber("0123456789")
                                            .setAddressLines(Arrays.asList("20 rue du client douteux", "1 rue de solitude"))
                                            .setCountry("Belgium").setPostCode("4321"))
                .setPhysicalContacts(Arrays.asList(new TppPhysicalContact()
                                                           .setContactType(BUSINESS_CONTACT)
                                                           .setEmail("jépade.client@bookee.com")
                                                           .setName("Client")
                                                           .setFirstName("Jépade")
                                                           .setJobTitle("CEO")
                                                           .setPhoneNumber("0123456789"),
                                                   new TppPhysicalContact()
                                                           .setContactType(TECHNICAL_CONTACT)
                                                           .setEmail("sareuh.crute@sensodyne.com")
                                                           .setName("Crute")
                                                           .setFirstName("Sareuh")
                                                           .setJobTitle("CTO")
                                                           .setPhoneNumber("0123456789")));
    }

    public static Tpp createSolidarityBank() {
        return new Tpp()
                .setName("Solidarity bank")
                .setComments("Strange bank")
                .setState(new TppState().setStatus(BLOCKED).setMonitored(true))
                .setAccreditations(Stream.of(AIS, PIS).collect(Collectors.toSet()))
                .setCredentials(new TppCredentials().setClientId("PSDFR-BDE-SOLIDARITYBANK").setSecret("123456")
                                                    .setTrustedRedirectUrls(SetsKt.setOf("api.redirectUrl.com"))
                                                    .setScopes(Stream.of("openid", "aisp", "pisp").collect(Collectors.toSet())))
                .setWebSite("solidarity.com")
                .setNationalAccreditationData(new TppNationalAccreditation().setOrganizationIdentifier("PSDFR-BDE-SOLIDARITYBANK")
                                                                            .setCompetentAuthorityCountryCode("FR")
                                                                            .setCompetentAuthorityIdentifier("BDE")
                                                                            .setAuthorizationNumber("SOLIDARITYBANK")
                                                                            .setCompetentAuthorityName("French registry")
                                                                            .setCompetentAuthorityStatus(AUTHORISED)
                                                                            .setRegistrationDate(LocalDate.of(2018, 2, 28))
                                                                            .setAccreditations(
                                                                                    Stream.of(AIS, PIS).collect(Collectors.toSet())))
                .setContact(new TppContact().setCity("Toulouse")
                                            .setEmail("solidarity@mail.com")
                                            .setPhoneNumber("0123456789")
                                            .setAddressLines(Arrays.asList("random line", "road 10"))
                                            .setCountry("France").setPostCode("4321"))
                .setPhysicalContacts(Arrays.asList(new TppPhysicalContact()
                                                           .setContactType(BUSINESS_CONTACT)
                                                           .setEmail("john.doe@solidarity.com")
                                                           .setName("Doe")
                                                           .setFirstName("John")
                                                           .setJobTitle("CEO")
                                                           .setPhoneNumber("0123456789"),
                                                   new TppPhysicalContact()
                                                           .setContactType(TECHNICAL_CONTACT)
                                                           .setEmail("jim.forest@solidarity.com")
                                                           .setName("forest")
                                                           .setFirstName("Jim")
                                                           .setJobTitle("CTO")
                                                           .setPhoneNumber("0123456789")));
    }

    private static Tpp createABCBank() {
        return new Tpp()
                .withId(new ObjectId("59b13f31fb7b7dcdeb0becfe"))
                .setName("ABC bank")
                .setComments("ABC first bank")
                .setState(new TppState().setStatus(ACTIVE).setMonitored(false))
                .setContact(new TppContact().setCity("Paris").setEmail("abc@mail.com").setPhoneNumber("0123456789"))
                .setAccreditations(Stream.of(AIS, CIS).collect(Collectors.toSet()))
                .setCredentials(new TppCredentials().setClientId("PSDFR-BDE-ABCBANK").setSecret("123456")
                                                    .setTrustedRedirectUrls(SetsKt.setOf("api.redirectUrl.com"))
                                                    .setScopes(Stream.of("openid", "aisp", "cisp").collect(Collectors.toSet())))
                .setWebSite("abc.com")
                .setNationalAccreditationData(new TppNationalAccreditation().setOrganizationIdentifier("PSDFR-BDE-ABCBANK")
                                                                            .setCompetentAuthorityCountryCode("FR")
                                                                            .setCompetentAuthorityIdentifier("BDE")
                                                                            .setAuthorizationNumber("ABCBANK")
                                                                            .setCompetentAuthorityName("French registry")
                                                                            .setCompetentAuthorityStatus(AUTHORISED)
                                                                            .setRegistrationDate(LocalDate.of(2018, 2, 28))
                                                                            .setAccreditations(
                                                                                    Stream.of(AIS, CIS).collect(Collectors.toSet())))
                .setContact(new TppContact().setCity("Paris")
                                            .setEmail("abc@mail.com")
                                            .setPhoneNumber("0123456789")
                                            .setAddressLines(Arrays.asList("random line", "road 10"))
                                            .setCountry("France").setPostCode("4321"))
                .setPhysicalContacts(Arrays.asList(new TppPhysicalContact()
                                                           .setContactType(BUSINESS_CONTACT)
                                                           .setEmail("john.doe@solidarity.com")
                                                           .setName("Doe")
                                                           .setFirstName("John")
                                                           .setJobTitle("CEO")
                                                           .setPhoneNumber("0123456789"),
                                                   new TppPhysicalContact()
                                                           .setContactType(TECHNICAL_CONTACT)
                                                           .setEmail("jim.forest@solidarity.com")
                                                           .setName("forest")
                                                           .setFirstName("Jim")
                                                           .setJobTitle("CTO")
                                                           .setPhoneNumber("0123456789")));
    }

    private static Tpp createDxpPlatformTpp() {
        return new Tpp()
                .setName("DxP platform")
                .setComments("DxP platform")
                .setState(new TppState().setStatus(ACTIVE).setMonitored(false))
                .setContact(new TppContact().setCity("Paris").setEmail("dxp@mail.com").setPhoneNumber("0123456789"))
                .setAccreditations(Stream.of(AIS, CIS, PIS).collect(Collectors.toSet()))
                .setCredentials(new TppCredentials().setClientId("PSDFR-BDE-DXP").setSecret("123456")
                                                    .setTrustedRedirectUrls(SetsKt.setOf("api.redirectUrl.com"))
                                                    .setScopes(Stream.of("platform").collect(Collectors.toSet())))
                .setWebSite("dxp.com")
                .setNationalAccreditationData(new TppNationalAccreditation().setOrganizationIdentifier("PSDFR-BDE-DXP")
                                                                            .setCompetentAuthorityCountryCode("FR")
                                                                            .setCompetentAuthorityIdentifier("BDE")
                                                                            .setAuthorizationNumber("DXP")
                                                                            .setCompetentAuthorityName("French registry")
                                                                            .setCompetentAuthorityStatus(AUTHORISED)
                                                                            .setRegistrationDate(LocalDate.of(2018, 2, 28))
                                                                            .setAccreditations(
                                                                                    Stream.of(AIS, CIS, PIS).collect(Collectors.toSet())))
                .setContact(new TppContact().setCity("Paris")
                                            .setEmail("dxp@mail.com")
                                            .setPhoneNumber("0123456789")
                                            .setAddressLines(Arrays.asList("random line", "road 10"))
                                            .setCountry("France")
                                            .setPostCode("4321"))
                .setPhysicalContacts(Arrays.asList(new TppPhysicalContact()
                                                           .setContactType(BUSINESS_CONTACT)
                                                           .setEmail("john.doe@dxp.com")
                                                           .setName("Doe")
                                                           .setFirstName("John")
                                                           .setJobTitle("CEO")
                                                           .setPhoneNumber("0123456789"),
                                                   new TppPhysicalContact()
                                                           .setContactType(TECHNICAL_CONTACT)
                                                           .setEmail("jim.forest@dxp.com")
                                                           .setName("forest")
                                                           .setFirstName("Jim")
                                                           .setJobTitle("CTO")
                                                           .setPhoneNumber("0123456789")));
    }

    private static Tpp createDotaTpp() {
        return new Tpp()
                .setName("DOTA")
                .setComments("Defence of the ancients")
                .setState(new TppState().setStatus(ACTIVE).setMonitored(true))
                .setAccreditations(Stream.of(PIS, AIS).collect(Collectors.toSet()))
                .setCredentials(new TppCredentials().setClientId("PSDUS-BDE-DOTA").setSecret("123456")
                                                    .setTrustedRedirectUrls(SetsKt.setOf("api.redirectUrl.com"))
                                                    .setScopes(Stream.of("openid", "aisp", "pisp").collect(Collectors.toSet())))
                .setWebSite("dota.com")
                .setNationalAccreditationData(new TppNationalAccreditation().setOrganizationIdentifier("PSDUS-BDE-DOTA")
                                                                            .setCompetentAuthorityCountryCode("US")
                                                                            .setCompetentAuthorityIdentifier("BDE")
                                                                            .setAuthorizationNumber("DOTA")
                                                                            .setCompetentAuthorityName("USA registry")
                                                                            .setCompetentAuthorityStatus(AUTHORISED)
                                                                            .setRegistrationDate(LocalDate.of(2018, 2, 28))
                                                                            .setAccreditations(
                                                                                    Stream.of(AIS, PIS).collect(Collectors.toSet())))
                .setContact(new TppContact().setCity("Seattle")
                                            .setEmail("dota@volvo.com")
                                            .setPhoneNumber("012345789")
                                            .setAddressLines(Arrays.asList("random line", "road 10"))
                                            .setCountry("USA").setPostCode("4321"))
                .setPhysicalContacts(Arrays.asList(new TppPhysicalContact()
                                                           .setContactType(BUSINESS_CONTACT)
                                                           .setEmail("john.doe@dota.com")
                                                           .setName("Doe")
                                                           .setFirstName("John")
                                                           .setJobTitle("CEO")
                                                           .setPhoneNumber("0123456789"),
                                                   new TppPhysicalContact()
                                                           .setContactType(TECHNICAL_CONTACT)
                                                           .setEmail("jim.forest@dota.com")
                                                           .setName("forest")
                                                           .setFirstName("Jim")
                                                           .setJobTitle("CTO")
                                                           .setPhoneNumber("0123456789")));
    }

    private static Tpp createNewTpp() {
        return new Tpp()
                .setName("NewTpp")
                .setComments("Adding NewTpp for user-auth contract compliance")
                .setState(new TppState().setStatus(ACTIVE).setMonitored(true))
                .setContact(new TppContact().setCity("Saint-Denis").setEmail("natures.prophet@dotamail.com").setPhoneNumber("33601010101"))
                .setAccreditations(Stream.of(PIS, AIS).collect(Collectors.toSet()))
                .setCredentials(new TppCredentials().setClientId("PSDFR-BDE-NEWTPP").setSecret("HtEGKO")
                                                    .setTrustedRedirectUrls(SetsKt.setOf("api.redirectUrl.com"))
                                                    .setScopes(Stream.of("openid", "aisp", "pisp").collect(Collectors.toSet())))
                .setWebSite("dotamail.com")
                .setNationalAccreditationData(new TppNationalAccreditation().setOrganizationIdentifier("PSDFR-BDE-NEWTPP")
                                                                            .setCompetentAuthorityCountryCode("FR")
                                                                            .setCompetentAuthorityIdentifier("BDE")
                                                                            .setAuthorizationNumber("NEWTPP")
                                                                            .setCompetentAuthorityName("French registry")
                                                                            .setCompetentAuthorityStatus(AUTHORISED)
                                                                            .setRegistrationDate(LocalDate.of(2018, 2, 28))
                                                                            .setAccreditations(
                                                                                    Stream.of(AIS, PIS).collect(Collectors.toSet())))
                .setContact(new TppContact().setCity("Saint-Denis")
                                            .setEmail("natures.prophet@dotamail.com")
                                            .setPhoneNumber("33601010101")
                                            .setAddressLines(Arrays.asList("random line", "road 10"))
                                            .setCountry("France").setPostCode("4321"))
                .setPhysicalContacts(Arrays.asList(new TppPhysicalContact()
                                                           .setContactType(BUSINESS_CONTACT)
                                                           .setEmail("john.doe@dota.com")
                                                           .setName("Doe")
                                                           .setFirstName("John")
                                                           .setJobTitle("CEO")
                                                           .setPhoneNumber("0123456789"),
                                                   new TppPhysicalContact()
                                                           .setContactType(TECHNICAL_CONTACT)
                                                           .setEmail("jim.forest@dota.com")
                                                           .setName("forest")
                                                           .setFirstName("Jim")
                                                           .setJobTitle("CTO")
                                                           .setPhoneNumber("0123456789")));
    }

    private static Tpp createNewTppStandard() {
        return new Tpp()
                .setName("ID_TPP")
                .setComments("Adding ID_TPP for user-auth contract compliance")
                .setState(new TppState().setStatus(ACTIVE).setMonitored(true))
                .setAccreditations(Stream.of(PIS, AIS).collect(Collectors.toSet()))
                .setCredentials(new TppCredentials().setClientId("PSDFR-BDE-ID_TPP").setSecret("HtEGKO")
                                                    .setTrustedRedirectUrls(SetsKt.setOf("api.redirectUrl.com"))
                                                    .setScopes(Stream.of("openid", "aisp", "pisp").collect(Collectors.toSet())))
                .setWebSite("dotamail.com")
                .setNationalAccreditationData(new TppNationalAccreditation().setOrganizationIdentifier("PSDFR-BDE-ID_TPP")
                                                                            .setCompetentAuthorityCountryCode("FR")
                                                                            .setCompetentAuthorityIdentifier("BDE")
                                                                            .setAuthorizationNumber("ID_TPP")
                                                                            .setCompetentAuthorityName("French registry")
                                                                            .setCompetentAuthorityStatus(AUTHORISED)
                                                                            .setRegistrationDate(LocalDate.of(2018, 2, 28))
                                                                            .setAccreditations(
                                                                                    Stream.of(AIS, PIS).collect(Collectors.toSet())))
                .setContact(new TppContact().setCity("Saint-Denis")
                                            .setEmail("natures.prophet@dotamail.com")
                                            .setPhoneNumber("33601010101")
                                            .setAddressLines(Arrays.asList("random line", "road 10"))
                                            .setCountry("France").setPostCode("4321"))
                .setPhysicalContacts(Arrays.asList(new TppPhysicalContact()
                                                           .setContactType(BUSINESS_CONTACT)
                                                           .setEmail("john.doe@dota.com")
                                                           .setName("Doe")
                                                           .setFirstName("John")
                                                           .setJobTitle("CEO")
                                                           .setPhoneNumber("0123456789"),
                                                   new TppPhysicalContact()
                                                           .setContactType(TECHNICAL_CONTACT)
                                                           .setEmail("jim.forest@dota.com")
                                                           .setName("forest")
                                                           .setFirstName("Jim")
                                                           .setJobTitle("CTO")
                                                           .setPhoneNumber("0123456789")));
    }

    private static Tpp createNewTppEnable() {
        return new Tpp()
                .setName("ID_TPP_ACTIVE_CATALOG_ENABLE")
                .setComments("Adding ID_TPP for user-auth contract compliance")
                .setState(new TppState().setStatus(ACTIVE).setMonitored(true))
                .setAccreditations(Stream.of(PIS, AIS).collect(Collectors.toSet()))
                .setCredentials(new TppCredentials().setClientId("PSDFR-BDE-ID_TPP_ACTIVE_CATALOG_ENABLE").setSecret("HtEGKO")
                                                    .setTrustedRedirectUrls(SetsKt.setOf("api.redirectUrl.com"))
                                                    .setScopes(Stream.of("openid", "aisp", "pisp").collect(Collectors.toSet())))
                .setWebSite("dotamail.com")
                .setNationalAccreditationData(
                        new TppNationalAccreditation().setOrganizationIdentifier("PSDFR-BDE-ID_TPP_ACTIVE_CATALOG_ENABLE")
                                                      .setCompetentAuthorityCountryCode("FR")
                                                      .setCompetentAuthorityIdentifier("BDE")
                                                      .setAuthorizationNumber("ID_TPP_ACTIVE_CATALOG_ENABLE")
                                                      .setCompetentAuthorityName("French registry")
                                                      .setCompetentAuthorityStatus(AUTHORISED)
                                                      .setRegistrationDate(LocalDate.of(2018, 2, 28))
                                                      .setAccreditations(
                                                              Stream.of(AIS, PIS).collect(Collectors.toSet())))
                .setContact(new TppContact().setCity("Saint-Denis")
                                            .setEmail("natures.prophet@dotamail.com")
                                            .setPhoneNumber("33601010101")
                                            .setAddressLines(Arrays.asList("random line", "road 10"))
                                            .setCountry("France").setPostCode("4321"))
                .setPhysicalContacts(Arrays.asList(new TppPhysicalContact()
                                                           .setContactType(BUSINESS_CONTACT)
                                                           .setEmail("john.doe@dota.com")
                                                           .setName("Doe")
                                                           .setFirstName("John")
                                                           .setJobTitle("CEO")
                                                           .setPhoneNumber("0123456789"),
                                                   new TppPhysicalContact()
                                                           .setContactType(TECHNICAL_CONTACT)
                                                           .setEmail("jim.forest@dota.com")
                                                           .setName("forest")
                                                           .setFirstName("Jim")
                                                           .setJobTitle("CTO")
                                                           .setPhoneNumber("0123456789")));
    }

    private static Tpp createNewTppFalse() {
        return new Tpp()
                .setName("ID_TPP_FALSE")
                .setComments("Adding ID_TPP for user-auth contract compliance")
                .setState(new TppState().setStatus(ACTIVE).setMonitored(true))
                .setAccreditations(Stream.of(PIS, AIS).collect(Collectors.toSet()))
                .setCredentials(new TppCredentials().setClientId("PSDFR-BDE-ID_TPP_FALSE").setSecret("HtEGKO")
                                                    .setTrustedRedirectUrls(SetsKt.setOf("api.redirectUrl.com"))
                                                    .setScopes(Stream.of("openid", "aisp", "pisp").collect(Collectors.toSet())))
                .setWebSite("dotamail.com")
                .setNationalAccreditationData(new TppNationalAccreditation().setOrganizationIdentifier("PSDFR-BDE-ID_TPP_FALSE")
                                                                            .setCompetentAuthorityCountryCode("FR")
                                                                            .setCompetentAuthorityIdentifier("BDE")
                                                                            .setAuthorizationNumber("ID_TPP_FALSE")
                                                                            .setCompetentAuthorityName("French registry")
                                                                            .setCompetentAuthorityStatus(AUTHORISED)
                                                                            .setRegistrationDate(LocalDate.of(2018, 2, 28))
                                                                            .setAccreditations(
                                                                                    Stream.of(AIS, PIS).collect(Collectors.toSet())))
                .setContact(new TppContact().setCity("Saint-Denis")
                                            .setEmail("natures.prophet@dotamail.com")
                                            .setPhoneNumber("33601010101")
                                            .setAddressLines(Arrays.asList("random line", "road 10"))
                                            .setCountry("France").setPostCode("4321"))
                .setPhysicalContacts(Arrays.asList(new TppPhysicalContact()
                                                           .setContactType(BUSINESS_CONTACT)
                                                           .setEmail("john.doe@dota.com")
                                                           .setName("Doe")
                                                           .setFirstName("John")
                                                           .setJobTitle("CEO")
                                                           .setPhoneNumber("0123456789"),
                                                   new TppPhysicalContact()
                                                           .setContactType(TECHNICAL_CONTACT)
                                                           .setEmail("jim.forest@dota.com")
                                                           .setName("forest")
                                                           .setFirstName("Jim")
                                                           .setJobTitle("CTO")
                                                           .setPhoneNumber("0123456789")));
    }

    private static Tpp createNewTppDisable() {
        return new Tpp()
                .setName("ID_TPP_ACTIVE_CATALOG_DISABLE")
                .setComments("Adding ID_TPP for user-auth contract compliance")
                .setState(new TppState().setStatus(ACTIVE).setMonitored(true))
                .setAccreditations(Stream.of(PIS, AIS).collect(Collectors.toSet()))
                .setCredentials(new TppCredentials().setClientId("PSDFR-BDE-ID_TPP_ACTIVE_CATALOG_DISABLE").setSecret("HtEGKO")
                                                    .setTrustedRedirectUrls(SetsKt.setOf("api.redirectUrl.com"))
                                                    .setScopes(Stream.of("openid", "aisp", "pisp").collect(Collectors.toSet())))
                .setWebSite("dotamail.com")
                .setNationalAccreditationData(
                        new TppNationalAccreditation().setOrganizationIdentifier("PSDFR-BDE-ID_TPP_ACTIVE_CATALOG_DISABLE")
                                                      .setCompetentAuthorityCountryCode("FR")
                                                      .setCompetentAuthorityIdentifier("BDE")
                                                      .setAuthorizationNumber("ID_TPP_ACTIVE_CATALOG_DISABLE")
                                                      .setCompetentAuthorityName("French registry")
                                                      .setCompetentAuthorityStatus(AUTHORISED)
                                                      .setRegistrationDate(LocalDate.of(2018, 2, 28))
                                                      .setAccreditations(
                                                              Stream.of(AIS, PIS).collect(Collectors.toSet())))
                .setContact(new TppContact().setCity("Saint-Denis")
                                            .setEmail("natures.prophet@dotamail.com")
                                            .setPhoneNumber("33601010101")
                                            .setAddressLines(Arrays.asList("random line", "road 10"))
                                            .setCountry("France").setPostCode("4321"))
                .setPhysicalContacts(Arrays.asList(new TppPhysicalContact()
                                                           .setContactType(BUSINESS_CONTACT)
                                                           .setEmail("john.doe@dota.com")
                                                           .setName("Doe")
                                                           .setFirstName("John")
                                                           .setJobTitle("CEO")
                                                           .setPhoneNumber("0123456789"),
                                                   new TppPhysicalContact()
                                                           .setContactType(TECHNICAL_CONTACT)
                                                           .setEmail("jim.forest@dota.com")
                                                           .setName("forest")
                                                           .setFirstName("Jim")
                                                           .setJobTitle("CTO")
                                                           .setPhoneNumber("0123456789")));
    }

    private static Tpp createNewTppEmpty() {
        return new Tpp()
                .setName("ID_TPP_EMPTY")
                .setComments("Adding ID_TPP for user-auth contract compliance")
                .setState(new TppState().setStatus(ACTIVE).setMonitored(true))
                .setAccreditations(Stream.of(PIS, AIS).collect(Collectors.toSet()))
                .setCredentials(new TppCredentials().setClientId("PSDFR-BDE-ID_TPP_EMPTY").setSecret("HtEGKO")
                                                    .setTrustedRedirectUrls(SetsKt.setOf("api.redirectUrl.com"))
                                                    .setScopes(Stream.of("openid", "aisp", "pisp").collect(Collectors.toSet())))
                .setWebSite("dotamail.com")
                .setNationalAccreditationData(new TppNationalAccreditation().setOrganizationIdentifier("PSDFR-BDE-ID_TPP_EMPTY")
                                                                            .setCompetentAuthorityCountryCode("FR")
                                                                            .setCompetentAuthorityIdentifier("BDE")
                                                                            .setAuthorizationNumber("ID_TPP_EMPTY")
                                                                            .setCompetentAuthorityName("French registry")
                                                                            .setCompetentAuthorityStatus(AUTHORISED)
                                                                            .setRegistrationDate(LocalDate.of(2018, 2, 28))
                                                                            .setAccreditations(
                                                                                    Stream.of(AIS, PIS).collect(Collectors.toSet())))
                .setContact(new TppContact().setCity("Saint-Denis")
                                            .setEmail("natures.prophet@dotamail.com")
                                            .setPhoneNumber("33601010101")
                                            .setAddressLines(Arrays.asList("random line", "road 10"))
                                            .setCountry("France").setPostCode("4321"))
                .setPhysicalContacts(Arrays.asList(new TppPhysicalContact()
                                                           .setContactType(BUSINESS_CONTACT)
                                                           .setEmail("john.doe@dota.com")
                                                           .setName("Doe")
                                                           .setFirstName("John")
                                                           .setJobTitle("CEO")
                                                           .setPhoneNumber("0123456789"),
                                                   new TppPhysicalContact()
                                                           .setContactType(TECHNICAL_CONTACT)
                                                           .setEmail("jim.forest@dota.com")
                                                           .setName("forest")
                                                           .setFirstName("Jim")
                                                           .setJobTitle("CTO")
                                                           .setPhoneNumber("0123456789")));
    }

    private static Tpp createNewTppWithoutAccreditations() {
        return new Tpp()
                .withId(new ObjectId("5b22859f26f9690d8bbd1761"))
                .setName("ID_TPP_ACTIVE_WITHOUT_ACCREDITATION")
                .setComments("Adding ID_TPP for user-auth contract compliance")
                .setState(new TppState().setStatus(ACTIVE).setMonitored(true))
                .setAccreditations(Stream.of(CIS).collect(Collectors.toSet()))
                .setCredentials(new TppCredentials().setClientId("PSDFR-BDE-ID_TPP_ACTIVE_WITHOUT_ACCREDITATION").setSecret("HtEGKO")
                                                    .setTrustedRedirectUrls(SetsKt.setOf("api.redirectUrl.com"))
                                                    .setScopes(Stream.of("openid", "aisp", "pisp").collect(Collectors.toSet())))
                .setWebSite("dotamail.com")
                .setNationalAccreditationData(
                        new TppNationalAccreditation().setOrganizationIdentifier("PSDFR-BDE-ID_TPP_ACTIVE_WITHOUT_ACCREDITATION")
                                                      .setCompetentAuthorityCountryCode("FR")
                                                      .setCompetentAuthorityIdentifier("BDE")
                                                      .setAuthorizationNumber("ID_TPP_ACTIVE_WITHOUT_ACCREDITATION")
                                                      .setCompetentAuthorityName("French registry")
                                                      .setCompetentAuthorityStatus(AUTHORISED)
                                                      .setRegistrationDate(LocalDate.of(2018, 2, 28))
                                                      .setAccreditations(Stream.of(AIS, CIS, PIS).collect(Collectors.toSet())))
                .setContact(new TppContact().setCity("Saint-Denis")
                                            .setEmail("natures.prophet@dotamail.com")
                                            .setPhoneNumber("33601010101")
                                            .setAddressLines(Arrays.asList("random line", "road 10"))
                                            .setCountry("France").setPostCode("4321"))
                .setPhysicalContacts(Arrays.asList(new TppPhysicalContact()
                                                           .setContactType(BUSINESS_CONTACT)
                                                           .setEmail("john.doe@dota.com")
                                                           .setName("Doe")
                                                           .setFirstName("John")
                                                           .setJobTitle("CEO")
                                                           .setPhoneNumber("0123456789"),
                                                   new TppPhysicalContact()
                                                           .setContactType(TECHNICAL_CONTACT)
                                                           .setEmail("jim.forest@dota.com")
                                                           .setName("forest")
                                                           .setFirstName("Jim")
                                                           .setJobTitle("CTO")
                                                           .setPhoneNumber("0123456789")));
    }

    private static Tpp createNewTppBlocked() {
        return new Tpp()
                .setName("ID_TPP_BLOCKED")
                .setComments("Adding ID_TPP for user-auth contract compliance")
                .setState(new TppState().setStatus(BLOCKED).setMonitored(true))
                .setAccreditations(Stream.of(PIS, AIS).collect(Collectors.toSet()))
                .setCredentials(new TppCredentials().setClientId("PSDFR-BDE-ID_TPP_BLOCKED").setSecret("HtEGKO")
                                                    .setTrustedRedirectUrls(SetsKt.setOf("api.redirectUrl.com"))
                                                    .setScopes(Stream.of("openid", "aisp", "pisp").collect(Collectors.toSet())))
                .setWebSite("dotamail.com")
                .setNationalAccreditationData(new TppNationalAccreditation().setOrganizationIdentifier("PSDFR-BDE-ID_TPP_BLOCKED")
                                                                            .setCompetentAuthorityCountryCode("FR")
                                                                            .setCompetentAuthorityIdentifier("BDE")
                                                                            .setAuthorizationNumber("ID_TPP_BLOCKED")
                                                                            .setCompetentAuthorityName("French registry")
                                                                            .setCompetentAuthorityStatus(AUTHORISED)
                                                                            .setRegistrationDate(LocalDate.of(2018, 2, 28))
                                                                            .setAccreditations(
                                                                                    Stream.of(AIS, PIS).collect(Collectors.toSet())))
                .setContact(new TppContact().setCity("Saint-Denis")
                                            .setEmail("natures.prophet@dotamail.com")
                                            .setPhoneNumber("33601010101")
                                            .setAddressLines(Arrays.asList("random line", "road 10"))
                                            .setCountry("France").setPostCode("4321"))
                .setPhysicalContacts(Arrays.asList(new TppPhysicalContact()
                                                           .setContactType(BUSINESS_CONTACT)
                                                           .setEmail("john.doe@dota.com")
                                                           .setName("Doe")
                                                           .setFirstName("John")
                                                           .setJobTitle("CEO")
                                                           .setPhoneNumber("0123456789"),
                                                   new TppPhysicalContact()
                                                           .setContactType(TECHNICAL_CONTACT)
                                                           .setEmail("jim.forest@dota.com")
                                                           .setName("forest")
                                                           .setFirstName("Jim")
                                                           .setJobTitle("CTO")
                                                           .setPhoneNumber("0123456789")));
    }

    public Tpp findAnyByPredicate(Predicate<Tpp> predicate) {
        return findAll().stream()
                        .filter(predicate)
                        .findAny()
                        .orElseThrow(() -> new DxpSbsFeedback(format("No TPP matching predicate %s found", predicate), RESOURCE_NOT_FOUND,
                                                              LOGGER::info).toException());
    }

    public List<Tpp> findByName(String name) {
        return findAll().stream()
                        .filter(t -> t.getName().equals(name))
                        .collect(toList());
    }

    @Override
    public List<Tpp> defaultEntities() {
        return allTpps();
    }
}
