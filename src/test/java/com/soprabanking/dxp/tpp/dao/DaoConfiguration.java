package com.soprabanking.dxp.tpp.dao;

import com.soprabanking.dxp.commons.test.Dao;
import com.soprabanking.dxp.tpp.model.entity.Tpp;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;

/**
 * Initializes all daos
 */
@Configuration
public class DaoConfiguration {

    @Bean
    public Dao<Tpp> tppDao(MongoTemplate mongoTemplate) {
        return new TppDAO(mongoTemplate);
    }
}