package com.soprabanking.dxp.tpp.mapper;

import com.soprabanking.dxp.commons.error.DxpSbsException;
import com.soprabanking.dxp.tpp.model.api.TppApiDTO;
import com.soprabanking.dxp.tpp.model.api.TppContactApiDTO;
import com.soprabanking.dxp.tpp.model.api.TppNationalAccreditationApiDTO;
import com.soprabanking.dxp.tpp.model.api.TppPhysicalContactApiDTO;
import com.soprabanking.dxp.tpp.model.api.TppStateApiDTO;
import com.soprabanking.dxp.tpp.model.entity.Tpp;
import com.soprabanking.dxp.tpp.model.entity.TppContact;
import com.soprabanking.dxp.tpp.model.entity.TppCredentials;
import com.soprabanking.dxp.tpp.model.entity.TppNationalAccreditation;
import com.soprabanking.dxp.tpp.model.entity.TppPhysicalContact;
import com.soprabanking.dxp.tpp.model.entity.TppState;
import com.soprabanking.dxp.tpp.model.entity.TppStatus;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.soprabanking.dxp.tpp.mapper.TppMapper.map;
import static com.soprabanking.dxp.tpp.mapper.TppMapper.merge;
import static com.soprabanking.dxp.tpp.model.entity.TppAccreditation.AIS;
import static com.soprabanking.dxp.tpp.model.entity.TppAccreditation.CIS;
import static com.soprabanking.dxp.tpp.model.entity.TppAccreditation.PIS;
import static com.soprabanking.dxp.tpp.model.entity.TppContactType.BUSINESS_CONTACT;
import static com.soprabanking.dxp.tpp.model.entity.TppContactType.TECHNICAL_CONTACT;
import static java.util.stream.Collectors.toSet;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatExceptionOfType;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

/**
 * Tests related to TPP mappings
 */
class TppMapperTest {

    @Test
    void map_shouldMapDTOToEntity() {
        TppApiDTO dto = new TppApiDTO()
                .withId(new ObjectId().toString())
                .setName("ABC bank")
                .setComments("ABC first bank")
                .setState(new TppStateApiDTO().setStatus(TppStatus.ACTIVE).setMonitored(true))
                .setContact(new TppContactApiDTO().setCity("Paris").setEmail("abc@mail.com").setPhoneNumber("0123456789")
                                                  .setAddressLines(Arrays.asList("random line", "road 10"))
                                                  .setCountry("France").setPostCode("4321"))
                .setAccreditations(Stream.of(AIS, CIS).collect(Collectors.toSet()))
                .setWebSite("dotamail.com")
                .setNationalAccreditationData(new TppNationalAccreditationApiDTO().setOrganizationIdentifier("ID_TPP")
                                                                                  .setCompetentAuthorityCountryCode("FR")
                                                                                  .setCompetentAuthorityName("french registry")
                                                                                  .setRegistrationDate(LocalDate.now())
                                                                                  .setAccreditations(
                                                                                          Stream.of(AIS, PIS).collect(Collectors.toSet())))
                .setPhysicalContacts(Arrays.asList(new TppPhysicalContactApiDTO()
                                                           .setContactType(BUSINESS_CONTACT)
                                                           .setEmail("john.doe@dota.com")
                                                           .setName("Doe")
                                                           .setFirstName("John")
                                                           .setJobTitle("CEO")
                                                           .setPhoneNumber("0123456789"),
                                                   new TppPhysicalContactApiDTO()
                                                           .setContactType(TECHNICAL_CONTACT)
                                                           .setEmail("jim.forest@dota.com")
                                                           .setName("forest")
                                                           .setFirstName("Jim")
                                                           .setJobTitle("CTO")
                                                           .setPhoneNumber("0123456789")));
        Tpp entity = map(dto);
        assertThat(entity.getId().toString()).isEqualTo(dto.getId());
        assertThat(entity.getName()).isEqualTo(dto.getName());
        assertThat(entity.getComments()).isEqualTo(dto.getComments());
        assertThat(entity.getState()).isNotNull();
        assertThat(entity.getContact()).isNotNull();
        assertThat(entity.getAccreditations()).isEqualTo(dto.getAccreditations());
        assertThat(entity.getWebSite()).isEqualTo(dto.getWebSite());
        assertThat(entity.getNationalAccreditationData()).isNotNull();
        assertThat(entity.getPhysicalContacts()).hasSize(2);

    }

    @Test
    void map_shouldMapDTOToEntityWithNullStatus() {
        TppApiDTO dto = new TppApiDTO();
        Tpp entity = map(dto);
        assertThat(entity.getState()).isNull();
    }

    @Test
    void map_shouldMapDTOToEntityWithNullId() {
        TppApiDTO dto = new TppApiDTO();
        Tpp entity = map(dto);
        assertThat(entity.getId()).isNull();
    }

    @Test
    void map_shouldMapDTOToEntityWithEmptyPhysicalContacts() {
        TppApiDTO dto = new TppApiDTO();
        Tpp entity = map(dto);
        assertThat(entity.getPhysicalContacts()).isEmpty();
    }

    @Test
    void map_shouldMapEntityToDTOWithEmptyPhysicalContacts() {
        Tpp entity = new Tpp();
        TppApiDTO dto = map(entity);
        assertThat(dto.getPhysicalContacts()).isEmpty();
    }

    @Test
    void map_shouldMapEntityToDTO() {
        Tpp entity = new Tpp()
                .withId(new ObjectId())
                .setName("ABC bank")
                .setComments("ABC first bank")
                .setState(new TppState().setStatus(TppStatus.ACTIVE).setMonitored(true))
                .setContact(new TppContact().setCity("Paris").setEmail("abc@mail.com").setPhoneNumber("0123456789")
                                            .setAddressLines(Arrays.asList("random line", "road 10"))
                                            .setCountry("France").setPostCode("4321"))
                .setCredentials(new TppCredentials().setClientId("abcClientId").setSecret("secret").setScopes(Collections.singleton("sco")))
                .setAccreditations(Stream.of(AIS, CIS).collect(Collectors.toSet()))
                .setWebSite("dotamail.com")
                .setNationalAccreditationData(new TppNationalAccreditation().setOrganizationIdentifier("ID_TPP")
                                                                            .setCompetentAuthorityCountryCode("FR")
                                                                            .setCompetentAuthorityName("french registry")
                                                                            .setRegistrationDate(LocalDate.now())
                                                                            .setAccreditations(
                                                                                    Stream.of(AIS, PIS).collect(Collectors.toSet())))
                .setPhysicalContacts(Arrays.asList(new TppPhysicalContact()
                                                           .setContactType(BUSINESS_CONTACT)
                                                           .setEmail("john.doe@dota.com")
                                                           .setName("Doe")
                                                           .setFirstName("John")
                                                           .setJobTitle("CEO")
                                                           .setPhoneNumber("0123456789"),
                                                   new TppPhysicalContact()
                                                           .setContactType(TECHNICAL_CONTACT)
                                                           .setEmail("jim.forest@dota.com")
                                                           .setName("forest")
                                                           .setFirstName("Jim")
                                                           .setJobTitle("CTO")
                                                           .setPhoneNumber("0123456789")));
        TppApiDTO dto = map(entity);
        assertThat(dto.getId()).isEqualTo(entity.getId().toString());
        assertThat(dto.getName()).isEqualTo(entity.getName());
        assertThat(dto.getComments()).isEqualTo(entity.getComments());
        assertThat(dto.getState()).isNotNull();
        assertThat(dto.getContact()).isNotNull();
        assertThat(dto.getCredentials()).isNotNull();
        assertThat(dto.getAccreditations()).isEqualTo(entity.getAccreditations());
        assertThat(dto.getWebSite()).isEqualTo(dto.getWebSite());
        assertThat(dto.getNationalAccreditationData()).isNotNull();
        assertThat(dto.getPhysicalContacts()).hasSize(2);
    }

    @Test
    void merge_shouldNotChangeCuzAllFieldsEmpty() {
        Tpp entity = new Tpp()
                .withId(new ObjectId())
                .setName("ABC bank")
                .setComments("ABC first bank")
                .setState(new TppState().setStatus(TppStatus.ACTIVE).setMonitored(true))
                .setContact(new TppContact().setCity("Paris").setEmail("abc@mail.com").setPhoneNumber("0123456789")
                                            .setAddressLines(Arrays.asList("random line", "road 10"))
                                            .setCountry("France").setPostCode("4321"))
                .setAccreditations(Stream.of(AIS, CIS).collect(Collectors.toSet()))
                .setWebSite("dotamail.com")
                .setNationalAccreditationData(new TppNationalAccreditation().setOrganizationIdentifier("ID_TPP")
                                                                            .setCompetentAuthorityCountryCode("France")
                                                                            .setCompetentAuthorityName("french registry")
                                                                            .setRegistrationDate(LocalDate.now())
                                                                            .setAccreditations(
                                                                                    Stream.of(AIS, PIS).collect(Collectors.toSet())))
                .setPhysicalContacts(Arrays.asList(new TppPhysicalContact()
                                                           .setContactType(BUSINESS_CONTACT)
                                                           .setEmail("john.doe@dota.com")
                                                           .setName("Doe")
                                                           .setFirstName("John")
                                                           .setJobTitle("CEO")
                                                           .setPhoneNumber("0123456789"),
                                                   new TppPhysicalContact()
                                                           .setContactType(TECHNICAL_CONTACT)
                                                           .setEmail("jim.forest@dota.com")
                                                           .setName("forest")
                                                           .setFirstName("Jim")
                                                           .setJobTitle("CTO")
                                                           .setPhoneNumber("0123456789")));
        assertThat(merge(new Tpp(), entity)).isEqualToComparingFieldByField(entity);
    }

    @Test
    void merge_shouldNotChangeCuzAllFieldsAreTheSame() {
        Tpp entity = new Tpp()
                .withId(new ObjectId())
                .setName("ABC bank")
                .setComments("ABC first bank")
                .setState(new TppState().setStatus(TppStatus.ACTIVE).setMonitored(true))
                .setContact(new TppContact().setCity("Paris").setEmail("abc@mail.com").setPhoneNumber("0123456789")
                                            .setAddressLines(Arrays.asList("random line", "road 10"))
                                            .setCountry("France").setPostCode("4321"))
                .setAccreditations(Stream.of(AIS, CIS).collect(Collectors.toSet()))
                .setWebSite("dotamail.com")
                .setNationalAccreditationData(new TppNationalAccreditation().setOrganizationIdentifier("ID_TPP")
                                                                            .setCompetentAuthorityCountryCode("France")
                                                                            .setCompetentAuthorityName("french registry")
                                                                            .setRegistrationDate(LocalDate.now())
                                                                            .setAccreditations(
                                                                                    Stream.of(AIS, PIS).collect(Collectors.toSet())))
                .setPhysicalContacts(Arrays.asList(new TppPhysicalContact()
                                                           .setContactType(BUSINESS_CONTACT)
                                                           .setEmail("john.doe@dota.com")
                                                           .setName("Doe")
                                                           .setFirstName("John")
                                                           .setJobTitle("CEO")
                                                           .setPhoneNumber("0123456789"),
                                                   new TppPhysicalContact()
                                                           .setContactType(TECHNICAL_CONTACT)
                                                           .setEmail("jim.forest@dota.com")
                                                           .setName("forest")
                                                           .setFirstName("Jim")
                                                           .setJobTitle("CTO")
                                                           .setPhoneNumber("0123456789")));
        assertThat(merge(new Tpp().setName("ABC bank"), entity)).isEqualToComparingFieldByField(entity);
    }

    @Test
    void merge_shouldChangeNationalId() {
        Tpp entity = new Tpp()
                .withId(new ObjectId())
                .setName("ABC bank")
                .setComments("ABC first bank")
                .setState(new TppState().setStatus(TppStatus.ACTIVE).setMonitored(true))
                .setContact(new TppContact().setCity("Paris").setEmail("abc@mail.com").setPhoneNumber("0123456789")
                                            .setAddressLines(Arrays.asList("random line", "road 10"))
                                            .setCountry("France").setPostCode("4321"))
                .setAccreditations(Stream.of(AIS, CIS).collect(Collectors.toSet()))
                .setWebSite("dotamail.com")
                .setNationalAccreditationData(new TppNationalAccreditation().setOrganizationIdentifier("ID_TPP")
                                                                            .setCompetentAuthorityCountryCode("France")
                                                                            .setCompetentAuthorityName("french registry")
                                                                            .setRegistrationDate(LocalDate.now())
                                                                            .setAccreditations(
                                                                                    Stream.of(AIS, PIS).collect(Collectors.toSet())))
                .setPhysicalContacts(Arrays.asList(new TppPhysicalContact()
                                                           .setContactType(BUSINESS_CONTACT)
                                                           .setEmail("john.doe@dota.com")
                                                           .setName("Doe")
                                                           .setFirstName("John")
                                                           .setJobTitle("CEO")
                                                           .setPhoneNumber("0123456789"),
                                                   new TppPhysicalContact()
                                                           .setContactType(TECHNICAL_CONTACT)
                                                           .setEmail("jim.forest@dota.com")
                                                           .setName("forest")
                                                           .setFirstName("Jim")
                                                           .setJobTitle("CTO")
                                                           .setPhoneNumber("0123456789")));
        String nationalId = "changed";
        Tpp merged = merge(new Tpp().setNationalAccreditationData(
                new TppNationalAccreditation()
                        .setOrganizationIdentifier(nationalId)
        ), entity);
        assertThat(merged).isEqualToIgnoringGivenFields(entity, "nationalId", "name");
        assertThat(merged.getNationalAccreditationData()).isNotNull();
        assertThat(merged.getNationalAccreditationData().getOrganizationIdentifier()).isEqualTo(nationalId);
    }

    @Test
    void merge_shouldChangeMultipleFields() {
        Tpp entity = new Tpp()
                .withId(new ObjectId())
                .setName("ABC bank")
                .setComments("ABC first bank")
                .setState(new TppState().setStatus(TppStatus.ACTIVE).setMonitored(true))
                .setContact(new TppContact().setCity("Paris").setEmail("abc@mail.com").setPhoneNumber("0123456789")
                                            .setAddressLines(Arrays.asList("random line", "road 10"))
                                            .setCountry("France").setPostCode("4321"))
                .setAccreditations(Stream.of(AIS, CIS).collect(Collectors.toSet()))
                .setWebSite("dotamail.com")
                .setNationalAccreditationData(new TppNationalAccreditation().setOrganizationIdentifier("ID_TPP")
                                                                            .setCompetentAuthorityCountryCode("France")
                                                                            .setCompetentAuthorityName("french registry")
                                                                            .setRegistrationDate(LocalDate.now())
                                                                            .setAccreditations(
                                                                                    Stream.of(AIS, PIS).collect(Collectors.toSet())))
                .setPhysicalContacts(Arrays.asList(new TppPhysicalContact()
                                                           .setContactType(BUSINESS_CONTACT)
                                                           .setEmail("john.doe@dota.com")
                                                           .setName("Doe")
                                                           .setFirstName("John")
                                                           .setJobTitle("CEO")
                                                           .setPhoneNumber("0123456789"),
                                                   new TppPhysicalContact()
                                                           .setContactType(TECHNICAL_CONTACT)
                                                           .setEmail("jim.forest@dota.com")
                                                           .setName("forest")
                                                           .setFirstName("Jim")
                                                           .setJobTitle("CTO")
                                                           .setPhoneNumber("0123456789")));
        String nationalId = "changedEbaId";
        String name = "changedName";
        Tpp merged = merge(new Tpp().setNationalAccreditationData(
                new TppNationalAccreditation()
                        .setOrganizationIdentifier(nationalId)
        ).setName(name), entity);
        assertThat(merged).isEqualToIgnoringGivenFields(entity, "nationalId", "name");
        assertThat(merged.getNationalAccreditationData()).isNotNull();
        assertThat(merged.getNationalAccreditationData().getOrganizationIdentifier()).isEqualTo(nationalId);
        assertThat(merged.getName()).isEqualTo(name);
    }

    @Test
    void extractNationalId_shouldMapTppDataToNationalId() {
        Tpp dto = new Tpp()
                .setNationalAccreditationData(new TppNationalAccreditation()
                                                      .setOrganizationIdentifier("ID_TPP")
                                                      .setCompetentAuthorityCountryCode("France")
                                                      .setCompetentAuthorityName("french registry")
                                                      .setRegistrationDate(LocalDate.of(2000, 1, 1))
                                                      .setAccreditations(Stream.of(AIS, PIS).collect(toSet()))
                );
        String nationalId = TppMapper.extractOrganizationIdentifier(dto);
        assertThat(nationalId).isEqualTo(dto.getNationalAccreditationData().getOrganizationIdentifier());
    }

    @Test
    void extractNationalId_shouldFailCauzNationalIdNull() {
        Tpp dto = new Tpp()
                .setNationalAccreditationData(new TppNationalAccreditation()
                                                      .setCompetentAuthorityCountryCode("France")
                                                      .setCompetentAuthorityName("french registry")
                                                      .setRegistrationDate(LocalDate.of(2000, 1, 1))
                                                      .setAccreditations(Stream.of(AIS, PIS).collect(toSet()))
                );
        assertThatExceptionOfType(DxpSbsException.class)
                .isThrownBy(() -> TppMapper.extractOrganizationIdentifier(dto));
    }

    @Test
    void extractNationalId_shouldFailCauzNationalAccreditationNull() {
        Tpp dto = new Tpp();
        assertThatExceptionOfType(DxpSbsException.class)
                .isThrownBy(() -> TppMapper.extractOrganizationIdentifier(dto));
    }
}