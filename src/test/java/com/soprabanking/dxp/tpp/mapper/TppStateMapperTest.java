package com.soprabanking.dxp.tpp.mapper;

import com.soprabanking.dxp.tpp.model.api.TppStateApiDTO;
import com.soprabanking.dxp.tpp.model.entity.TppState;
import org.junit.jupiter.api.Test;

import static com.soprabanking.dxp.tpp.model.entity.TppStatus.ACTIVE;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test related to {@link TppState} mappings
 */
class TppStateMapperTest {

    @Test
    void map_shouldMapDTOToEntity() {
        TppStateApiDTO dto = new TppStateApiDTO()
                .setStatus(ACTIVE)
                .setMonitored(true);
        TppState entity = TppStateMapper.map(dto);
        assertThat(entity).isNotNull();
        assertThat(entity.getStatus()).isEqualTo(dto.getStatus());
        assertThat(entity.isMonitored()).isEqualTo(dto.isMonitored());
    }

    @Test
    void map_shouldMapDTOToEntityWithDefaultToActive() {
        TppStateApiDTO dto = new TppStateApiDTO().setMonitored(false);
        TppState entity = TppStateMapper.map(dto);
        assertThat(entity).isNotNull();
        assertThat(entity.getStatus()).isEqualTo(ACTIVE);
        assertThat(entity.isMonitored()).isEqualTo(dto.isMonitored());
    }

    @Test
    void map_shouldMapEntityToDTO() {
        TppState entity = new TppState()
                .setStatus(ACTIVE)
                .setMonitored(true);
        TppStateApiDTO dto = TppStateMapper.map(entity);
        assertThat(dto).isNotNull();
        assertThat(dto.getStatus()).isEqualTo(entity.getStatus());
        assertThat(dto.isMonitored()).isEqualTo(entity.isMonitored());
    }

}