package com.soprabanking.dxp.tpp.mapper;

import com.soprabanking.dxp.tpp.model.api.TppCredentialApiDTO;
import com.soprabanking.dxp.tpp.model.entity.Tpp;
import com.soprabanking.dxp.tpp.model.entity.TppCredentials;
import kotlin.collections.SetsKt;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

/**
 * Tests related to {@link TppCredentials}
 */
class TppCredentialsMapperTest {

    @Test
    void map_shouldMapDTOToEntity() {
        TppCredentialApiDTO dto = new TppCredentialApiDTO().setClientId("abcId")
                                                           .setSecret("somesecret")
                                                           .setTrustedRedirectUrls(SetsKt.hashSetOf("a", "b", "c"));
        TppCredentials entity = TppCredentialsMapper.map(dto);
        assertThat(entity.getClientId()).isEqualTo(dto.getClientId());
        assertThat(entity.getSecret()).isEqualTo(dto.getSecret());
        assertThat(entity.getTrustedRedirectUrls()).isEqualTo(dto.getTrustedRedirectUrls());
    }

    @Test
    void map_shouldMapEntityToDTO() {
        TppCredentials entity = new TppCredentials().setClientId("abcId")
                                                    .setSecret("somesecret")
                                                    .setTrustedRedirectUrls(SetsKt.hashSetOf("a", "b", "c"));
        ObjectId tppId = new ObjectId();
        TppCredentialApiDTO dto = TppCredentialsMapper.map(entity, tppId);
        assertThat(dto.getTppId()).isEqualTo(tppId.toString());
        assertThat(dto.getClientId()).isEqualTo(entity.getClientId());
        assertThat(dto.getSecret()).isEqualTo(entity.getSecret());
        assertThat(dto.getTrustedRedirectUrls()).isEqualTo(entity.getTrustedRedirectUrls());
    }

    @Test
    void map_shouldMapTppToCredentialsDTO() {
        TppCredentials credentials = new TppCredentials().setClientId("abcId").setSecret("somesecret");
        Tpp tpp = new Tpp().withId(new ObjectId()).setCredentials(credentials);
        TppCredentialApiDTO dto = TppCredentialsMapper.map(tpp);
        assertThat(dto.getTppId()).isEqualTo(tpp.getId().toString());
        assertThat(dto.getClientId()).isEqualTo(credentials.getClientId());
        assertThat(dto.getSecret()).isEqualTo(credentials.getSecret());
    }
}