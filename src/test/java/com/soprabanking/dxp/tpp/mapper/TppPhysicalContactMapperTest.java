package com.soprabanking.dxp.tpp.mapper;

import com.soprabanking.dxp.tpp.model.api.TppPhysicalContactApiDTO;
import com.soprabanking.dxp.tpp.model.entity.TppPhysicalContact;
import org.junit.jupiter.api.Test;

import static com.soprabanking.dxp.tpp.model.entity.TppContactType.BUSINESS_CONTACT;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

/**
 * @author jntakpe
 */
class TppPhysicalContactMapperTest {

    @Test
    void map_shouldMapEntity2Dto() {
        TppPhysicalContact entity = new TppPhysicalContact()
                .setContactType(BUSINESS_CONTACT)
                .setEmail("john.doe@dota.com")
                .setName("Doe")
                .setFirstName("John")
                .setJobTitle("CEO")
                .setPhoneNumber("0123456789");
        TppPhysicalContactApiDTO dto = TppPhysicalContactMapper.map(entity);
        assertThat(dto).isNotNull();
        assertThat(dto.getContactType()).isEqualTo(entity.getContactType());
        assertThat(dto.getEmail()).isEqualTo(entity.getEmail());
        assertThat(dto.getName()).isEqualTo(entity.getName());
        assertThat(dto.getFirstName()).isEqualTo(entity.getFirstName());
        assertThat(dto.getJobTitle()).isEqualTo(entity.getJobTitle());
        assertThat(dto.getPhoneNumber()).isEqualTo(entity.getPhoneNumber());
    }

    @Test
    void map_shouldMapDto2Entity() {
        TppPhysicalContactApiDTO dto = new TppPhysicalContactApiDTO()
                .setContactType(BUSINESS_CONTACT)
                .setEmail("john.doe@dota.com")
                .setName("Doe")
                .setFirstName("John")
                .setJobTitle("CEO")
                .setPhoneNumber("0123456789");
        TppPhysicalContact entity = TppPhysicalContactMapper.map(dto);
        assertThat(entity).isNotNull();
        assertThat(entity.getContactType()).isEqualTo(dto.getContactType());
        assertThat(entity.getEmail()).isEqualTo(dto.getEmail());
        assertThat(entity.getName()).isEqualTo(dto.getName());
        assertThat(entity.getFirstName()).isEqualTo(dto.getFirstName());
        assertThat(entity.getJobTitle()).isEqualTo(dto.getJobTitle());
        assertThat(entity.getPhoneNumber()).isEqualTo(dto.getPhoneNumber());
    }

}