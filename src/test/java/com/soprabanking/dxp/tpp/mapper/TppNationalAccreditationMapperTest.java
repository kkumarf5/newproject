package com.soprabanking.dxp.tpp.mapper;

import com.soprabanking.dxp.tpp.model.api.TppNationalAccreditationApiDTO;
import com.soprabanking.dxp.tpp.model.entity.TppNationalAccreditation;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.stream.Stream;

import static com.soprabanking.dxp.tpp.model.entity.CompetentAuthorityStatus.AUTHORISED;
import static com.soprabanking.dxp.tpp.model.entity.TppAccreditation.AIS;
import static com.soprabanking.dxp.tpp.model.entity.TppAccreditation.PIS;
import static java.util.stream.Collectors.toSet;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

/**
 * Tests related to {@link TppNationalAccreditationMapper}
 */
class TppNationalAccreditationMapperTest {

    @Test
    void map_shouldMapEntity2Dto() {
        TppNationalAccreditation entity = new TppNationalAccreditation()
                .setOrganizationIdentifier("ID_TPP")
                .setCompetentAuthorityIdentifier("IDENTIFIER")
                .setCompetentAuthorityName("french registry")
                .setCompetentAuthorityCountryCode("FR")
                .setAuthorizationNumber("ABC")
                .setCompetentAuthorityStatus(AUTHORISED)
                .setRegistrationDate(LocalDate.of(2000, 1, 1))
                .setAccreditations(Stream.of(AIS, PIS).collect(toSet()));
        TppNationalAccreditationApiDTO dto = TppNationalAccreditationMapper.map(entity);
        assertThat(dto).isNotNull();
        assertThat(dto.getOrganizationIdentifier()).isEqualTo(entity.getOrganizationIdentifier());
        assertThat(dto.getCompetentAuthorityIdentifier()).isEqualTo(entity.getCompetentAuthorityIdentifier());
        assertThat(dto.getCompetentAuthorityName()).isEqualTo(entity.getCompetentAuthorityName());
        assertThat(dto.getCompetentAuthorityCountryCode()).isEqualTo(entity.getCompetentAuthorityCountryCode());
        assertThat(dto.getAuthorizationNumber()).isEqualTo(entity.getAuthorizationNumber());
        assertThat(dto.getCompetentAuthorityStatus()).isEqualTo(entity.getCompetentAuthorityStatus());
        assertThat(dto.getRegistrationDate()).isEqualTo(entity.getRegistrationDate());
        assertThat(dto.getAccreditations()).isEqualTo(entity.getAccreditations());
    }

    @Test
    void map_shouldMapDto2Entity() {
        TppNationalAccreditationApiDTO dto = new TppNationalAccreditationApiDTO()
                .setOrganizationIdentifier("ID_TPP")
                .setCompetentAuthorityIdentifier("IDENTIFIER")
                .setCompetentAuthorityName("french registry")
                .setCompetentAuthorityCountryCode("FR")
                .setAuthorizationNumber("ABC")
                .setCompetentAuthorityStatus(AUTHORISED)
                .setRegistrationDate(LocalDate.of(2000, 1, 1))
                .setAccreditations(Stream.of(AIS, PIS).collect(toSet()));
        TppNationalAccreditation entity = TppNationalAccreditationMapper.map(dto);
        assertThat(entity).isNotNull();
        assertThat(entity.getOrganizationIdentifier()).isEqualTo(dto.getOrganizationIdentifier());
        assertThat(entity.getCompetentAuthorityIdentifier()).isEqualTo(dto.getCompetentAuthorityIdentifier());
        assertThat(entity.getCompetentAuthorityName()).isEqualTo(dto.getCompetentAuthorityName());
        assertThat(entity.getCompetentAuthorityCountryCode()).isEqualTo(dto.getCompetentAuthorityCountryCode());
        assertThat(entity.getAuthorizationNumber()).isEqualTo(dto.getAuthorizationNumber());
        assertThat(entity.getCompetentAuthorityStatus()).isEqualTo(dto.getCompetentAuthorityStatus());
        assertThat(entity.getRegistrationDate()).isEqualTo(dto.getRegistrationDate());
        assertThat(entity.getAccreditations()).isEqualTo(dto.getAccreditations());
    }

}