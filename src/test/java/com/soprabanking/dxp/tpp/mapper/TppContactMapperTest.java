package com.soprabanking.dxp.tpp.mapper;

import com.soprabanking.dxp.tpp.model.api.TppContactApiDTO;
import com.soprabanking.dxp.tpp.model.entity.TppContact;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.assertj.core.api.Java6Assertions.assertThat;

/**
 * Tests related to {@link TppContact}
 */
class TppContactMapperTest {

    @Test
    void map_shouldMapDTOToEntity() {
        TppContactApiDTO dto = new TppContactApiDTO()
                .setCity("Paris")
                .setEmail("abc@mail.com")
                .setPhoneNumber("12345")
                .setAddressLines(Arrays.asList("random line", "road 10"))
                .setCountry("France")
                .setPostCode("4321");
        TppContact entity = TppContactMapper.map(dto);
        assertThat(entity.getCity()).isEqualTo(dto.getCity());
        assertThat(entity.getEmail()).isEqualTo(dto.getEmail());
        assertThat(entity.getPhoneNumber()).isEqualTo(dto.getPhoneNumber());
        assertThat(entity.getAddressLines()).isNotEmpty();
        assertThat(entity.getAddressLines()).containsOnlyElementsOf(dto.getAddressLines());
        assertThat(entity.getCountry()).isEqualTo(dto.getCountry());
        assertThat(entity.getPostCode()).isEqualTo(dto.getPostCode());
    }

    @Test
    void map_shouldMapEntityToDTO() {
        TppContact entity = new TppContact()
                .setCity("Paris")
                .setEmail("abc@mail.com")
                .setPhoneNumber("12345")
                .setAddressLines(Arrays.asList("random line", "road 10"))
                .setCountry("France")
                .setPostCode("4321");
        TppContactApiDTO dto = TppContactMapper.map(entity);
        assertThat(dto.getCity()).isEqualTo(entity.getCity());
        assertThat(dto.getEmail()).isEqualTo(entity.getEmail());
        assertThat(dto.getPhoneNumber()).isEqualTo(entity.getPhoneNumber());
        assertThat(dto.getAddressLines()).isNotEmpty();
        assertThat(dto.getAddressLines()).containsOnlyElementsOf(entity.getAddressLines());
        assertThat(dto.getCountry()).isEqualTo(entity.getCountry());
        assertThat(dto.getPostCode()).isEqualTo(entity.getPostCode());
    }

}