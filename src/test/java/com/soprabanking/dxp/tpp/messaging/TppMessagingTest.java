package com.soprabanking.dxp.tpp.messaging;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.soprabanking.dxp.commons.constants.CrudType;
import com.soprabanking.dxp.commons.error.DxpSbsException;
import com.soprabanking.dxp.commons.kafka.DefaultKafkaHeaders;
import com.soprabanking.dxp.commons.kafka.DefaultKafkaPayload;
import com.soprabanking.dxp.commons.kafka.KafkaProducer;
import com.soprabanking.dxp.commons.kafka.VersionedKafkaPayload;
import com.soprabanking.dxp.commons.kafka.VersionedValue;
import com.soprabanking.dxp.tpp.dao.TppDAO;
import com.soprabanking.dxp.tpp.mapper.TppMapper;
import com.soprabanking.dxp.tpp.model.api.TppApiDTO;
import com.soprabanking.dxp.tpp.model.entity.Tpp;
import com.soprabanking.dxp.tpp.repository.TppRepository;
import org.apache.kafka.clients.producer.MockProducer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import reactor.core.publisher.Mono;

import static com.soprabanking.dxp.commons.test.KafkaTestExtensionsKt.metadataVerifier;
import static com.soprabanking.dxp.commons.test.SecurityTestExtensionsKt.testWithUser;
import static com.soprabanking.dxp.commons.test.TestConstants.DEFAULT_USER;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatExceptionOfType;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.mockito.Mockito.when;

/**
 * Tests related to TPP message connection
 */
@SpringBootTest
class TppMessagingTest {

    private static final String CREATE_TOPIC = "tpp-create";

    private static final String UPDATE_TOPIC = "tpp-update";

    private final TppRepository tppRepository;

    private final TppMessaging tppMessaging;

    private final MockProducer<String, String> mockProducer;

    private final TppDAO tppDAO;

    private final ObjectMapper objectMapper;

    @Mock
    private KafkaProducer kafkaProducer;

    public TppMessagingTest(@Autowired TppRepository tppRepository,
                            @Autowired TppMessaging tppMessaging,
                            @Autowired MockProducer<String, String> mockProducer,
                            @Autowired TppDAO tppDAO,
                            @Autowired ObjectMapper objectMapper) {
        this.tppRepository = tppRepository;
        this.tppMessaging = tppMessaging;
        this.mockProducer = mockProducer;
        this.tppDAO = tppDAO;
        this.objectMapper = objectMapper;
    }

    @BeforeEach
    void beforeEach() {
        MockitoAnnotations.initMocks(this);
        tppDAO.initTest();
        mockProducer.clear();
    }

    @Test
    void sendCreation_shouldCallKafkaAndCreateKey() {
        Tpp tpp = tppDAO.findAny();
        testWithUser(tppMessaging.sendCreation(tpp))
                .expectSubscription()
                .consumeNextWith(t -> {
                    assertThat(t).isEqualTo(tpp);
                    String payload = metadataVerifier(mockProducer.history(), CREATE_TOPIC, tpp.getId().toString(), CrudType.CREATE);
                    assertThat(deserialize(payload, new TypeReference<TppApiDTO>() {
                    })).isEqualTo(TppMapper.map(tpp));
                })
                .verifyComplete();
    }

    @Test
    void sendCreation_shouldRollbackWhenKafkaFails() {
        Tpp tpp = tppDAO.findAny();
        TppApiDTO dto = TppMapper.map(tpp);
        DefaultKafkaHeaders headers = new DefaultKafkaHeaders(CrudType.CREATE, DEFAULT_USER.getBearerToken(), dto.getId());
        String errorMsg = "Kafka error";
        when(kafkaProducer.send(CREATE_TOPIC, headers, new DefaultKafkaPayload<>(dto)))
                .thenReturn(Mono.error(new IllegalStateException(errorMsg)));
        testWithUser(new TppMessaging(kafkaProducer, tppRepository).sendCreation(tpp))
                .expectSubscription()
                .consumeErrorWith(t -> {
                    assertThat(t).isInstanceOf(IllegalStateException.class);
                    assertThat(t.getMessage()).isEqualTo(errorMsg);
                    assertThatExceptionOfType(DxpSbsException.class).isThrownBy(() -> tppDAO.findById(dto.getId()));
                })
                .verify();
    }

    @Test
    void sendUpdate_shouldCallKafkaAndCreateKey() {
        Tpp previous = tppDAO.findAny();
        TppApiDTO dto = TppMapper.map(previous);
        Tpp updated = tppDAO.findAny().setName("updated");
        testWithUser(tppMessaging.sendUpdate(updated, previous))
                .expectSubscription()
                .consumeNextWith(t -> {
                    assertThat(t).isEqualTo(updated);
                    String payload = metadataVerifier(mockProducer.history(), UPDATE_TOPIC, dto.getId(), CrudType.UPDATE);
                    VersionedValue payloadObj = deserialize(payload, new TypeReference<VersionedValue<TppApiDTO>>() {
                    });
                    assertThat(payloadObj.getPrevious()).isEqualTo(dto);
                    assertThat(payloadObj.getActual()).isEqualTo(TppMapper.map(updated));
                })
                .verifyComplete();
    }

    @Test
    void sendUpdate_shouldRollbackWhenKafkaFails() {
        Tpp previous = tppDAO.findAny();
        String oldName = previous.getName();
        TppApiDTO previousDTO = TppMapper.map(previous);
        Tpp updated = tppDAO.findAny().setName("updated");
        TppApiDTO updatedDTO = TppMapper.map(updated);
        DefaultKafkaHeaders headers = new DefaultKafkaHeaders(CrudType.UPDATE, DEFAULT_USER.getBearerToken(), previousDTO.getId());
        String errorMsg = "Kafka error";
        when(kafkaProducer.send(UPDATE_TOPIC, headers, new VersionedKafkaPayload<>(updatedDTO, previousDTO)))
                .thenReturn(Mono.error(new IllegalStateException(errorMsg)));
        testWithUser(new TppMessaging(kafkaProducer, tppRepository).sendUpdate(updated, previous))
                .expectSubscription()
                .consumeErrorWith(t -> {
                    assertThat(t).isInstanceOf(IllegalStateException.class);
                    assertThat(t.getMessage()).isEqualTo(errorMsg);
                    Tpp current = tppDAO.findById(previousDTO.getId());
                    assertThat(current).isNotNull();
                    assertThat(current.getName()).isEqualTo(oldName);
                })
                .verify();
    }

    private <T> T deserialize(String payload, TypeReference<T> typeReference) {
        try {
            return objectMapper.readValue(payload, typeReference);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}