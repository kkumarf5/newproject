package com.soprabanking.dxp.tpp.service;

import com.soprabanking.dxp.commons.error.DxpSbsException;
import com.soprabanking.dxp.commons.page.Range;
import com.soprabanking.dxp.tpp.dao.TppDAO;
import com.soprabanking.dxp.tpp.model.entity.Tpp;
import com.soprabanking.dxp.tpp.model.entity.TppAccreditation;
import com.soprabanking.dxp.tpp.model.entity.TppCredentials;
import com.soprabanking.dxp.tpp.model.entity.TppNationalAccreditation;
import com.soprabanking.dxp.tpp.model.entity.TppState;
import kotlin.collections.SetsKt;
import org.assertj.core.data.TemporalUnitWithinOffset;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

import static com.soprabanking.dxp.commons.error.DxpBusinessCode.RESOURCE_ALREADY_EXIST;
import static com.soprabanking.dxp.commons.error.DxpBusinessCode.RESOURCE_NOT_FOUND;
import static com.soprabanking.dxp.commons.psd2.security.test.PersonaFactory.draven;
import static com.soprabanking.dxp.commons.psd2.security.test.PersonaFactory.jarvan;
import static com.soprabanking.dxp.commons.psd2.security.test.TppFactory.tppSensodyne;
import static com.soprabanking.dxp.commons.test.ExceptionVerifiersKt.expectSbsException;
import static com.soprabanking.dxp.commons.test.SecurityTestExtensionsKt.testWithUser;
import static com.soprabanking.dxp.tpp.dao.TppDAO.createSolidarityBank;
import static com.soprabanking.dxp.tpp.model.entity.TppAccreditation.AIS;
import static com.soprabanking.dxp.tpp.model.entity.TppAccreditation.CIS;
import static com.soprabanking.dxp.tpp.model.entity.TppAccreditation.PIS;
import static com.soprabanking.dxp.tpp.model.entity.TppStatus.ACTIVE;
import static java.time.LocalDateTime.now;
import static java.time.temporal.ChronoUnit.SECONDS;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import static kotlin.collections.CollectionsKt.listOf;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.data.domain.Sort.unsorted;
import static reactor.test.StepVerifier.create;

/**
 * Tests related to TPP business logic
 */
@SpringBootTest
class TppServiceTest {

    private static final Range DEFAULT_RANGE = new Range(0, 50, unsorted());

    private static final Duration CACHE_DELAY = Duration.ofMillis(10);

    private final TppService tppService;

    private final TppDAO tppDAO;

    private final Cache cacheId;

    private final Cache cacheClientId;

    public TppServiceTest(@Autowired TppDAO tppDAO, @Autowired TppService tppService, @Autowired CacheManager cacheManager) {
        this.tppDAO = tppDAO;
        this.tppService = tppService;
        cacheId = cacheManager.getCache("tppsById");
        cacheClientId = cacheManager.getCache("tppsByClientId");
    }

    @BeforeEach
    void setup() {
        tppDAO.initTest();
        cacheId.clear();
        cacheClientId.clear();
    }

    @Test
    void create_shouldUpdateCacheId() {
        Tpp newTpp = newTpp();
        List<String> ids = new ArrayList<>();
        testWithUser(tppService.create(newTpp), draven)
                .expectSubscription()
                .consumeNextWith(tpp -> {
                    assertThat(tpp).isEqualTo(newTpp);
                    ids.add(requireNonNull(tpp.getId()).toString());
                })
                .verifyComplete();
        create(Mono.delay(CACHE_DELAY).map(d -> idFromCache(ids.get(0))))
                .expectSubscription()
                .consumeNextWith(tpp -> assertThat(tpp).isPresent().hasValue(newTpp))
                .verifyComplete();
    }

    @Test
    void create_shouldUpdateCacheClientId() {
        Tpp newTpp = newTpp();
        List<String> clientIds = new ArrayList<>();
        testWithUser(tppService.create(newTpp), draven)
                .expectSubscription()
                .consumeNextWith(tpp -> {
                    assertThat(tpp).isEqualTo(newTpp);
                    clientIds.add(tpp.getCredentials().getClientId());
                })
                .verifyComplete();
        create(Mono.delay(CACHE_DELAY).map(d -> clientIdFromCache(clientIds.get(0))))
                .expectSubscription()
                .consumeNextWith(tpp -> assertThat(tpp).isPresent().hasValue(newTpp))
                .verifyComplete();
    }

    @Test
    void create_shouldCreateNewTpp() {
        testWithUser(tppService.create(newTpp()))
                .expectSubscription()
                .consumeNextWith(tpp -> {
                    assertThat(tpp).isNotNull();
                    assertThat(tpp.getId()).isNotNull();
                })
                .verifyComplete();
    }

    @Test
    @Disabled("Pending https://jira.spring.io/browse/DATACMNS-1231")
    void create_shouldSetCreatorName() {
        testWithUser(tppService.create(newTpp()), jarvan)
                .expectSubscription()
                .consumeNextWith(tpp -> assertThat(tpp.getCreatedBy()).isNotNull().isEqualTo("jarvan"))
                .verifyComplete();
    }

    @Test
    void findAll_shouldFindSomeTPPs() {
        create(tppService.findAll(DEFAULT_RANGE))
                .expectSubscription()
                .expectNextCount(tppDAO.count())
                .verifyComplete();
    }

    @Test
    void findAll_shouldNotFindAnyTPP() {
        tppDAO.deleteAll();
        create(tppService.findAll(DEFAULT_RANGE))
                .expectSubscription()
                .expectNextCount(0)
                .verifyComplete();
    }

    @Test
    void findById_shouldFindATppFromDb() {
        ObjectId id = tppDAO.findAny().getId();
        assertThat(cacheId.get(requireNonNull(id).toString())).isNull();
        testWithUser(tppService.findById(id), draven)
                .expectSubscription()
                .consumeNextWith(tpp -> {
                    assertThat(tpp).isNotNull();
                    assertThat(tpp.getId()).isNotNull().isEqualTo(id);
                })
                .verifyComplete();
    }

    @Test
    void findById_shouldFindATppFromCache() {
        Tpp anyTpp = tppDAO.findAny();
        ObjectId id = anyTpp.getId();
        cacheId.putIfAbsent(requireNonNull(id).toString(), anyTpp);
        tppDAO.deleteAll();
        assertThat(cacheId.get(id.toString())).isNotNull();
        testWithUser(tppService.findById(id), draven)
                .expectSubscription()
                .consumeNextWith(tpp -> {
                    assertThat(tpp).isNotNull();
                    assertThat(tpp.getId()).isNotNull().isEqualTo(id);
                })
                .verifyComplete();
    }

    @Test
    void findById_shouldFailCuzIdDoesNotExist() {
        testWithUser(tppService.findById(new ObjectId()), draven)
                .expectSubscription()
                .consumeErrorWith(e -> expectSbsException(e, RESOURCE_NOT_FOUND))
                .verify();
    }

    @Test
    @Disabled("Waiting for Spring Data to handle security reactive context")
    void create_shouldSetCreatedDate() {
        testWithUser(tppService.create(newTpp()))
                .expectSubscription()
                .consumeNextWith(tpp -> assertThat(tpp.getCreatedAt()).isNotNull()
                                                                      .isCloseTo(now(), new TemporalUnitWithinOffset(2, SECONDS)))
                .verifyComplete();
    }

    @Test
    void create_shouldGenerateCredentials() {
        testWithUser(tppService.create(newTpp()))
                .expectSubscription()
                .consumeNextWith(t -> {
                    assertThat(t.getCredentials()).isNotNull();
                    assertThat(t.getCredentials().getClientId()).isNotNull()
                                                                .isEqualTo(t.getNationalAccreditationData().getOrganizationIdentifier());
                    assertThat(t.getCredentials().getSecret()).isNotNull();
                })
                .verifyComplete();
    }

    @Test
    void create_shouldFailCuzOrganizationIdentifierAlreadyTaken() {
        create(tppService.create(newTpp().setNationalAccreditationData(
                new TppNationalAccreditation().setOrganizationIdentifier(
                        tppDAO.findAny().getNationalAccreditationData().getOrganizationIdentifier())))).expectSubscription()
                                                                                                       .consumeErrorWith(
                                                                                                               e -> expectSbsException(e,
                                                                                                                                       RESOURCE_ALREADY_EXIST))
                                                                                                       .verify();
    }

    @Test
    void create_shouldFailCuzTppNameAlreadyTaken() {
        create(tppService.create(newTpp().setName(tppDAO.findAny().getName())))
                .expectSubscription()
                .consumeErrorWith(e -> expectSbsException(e, RESOURCE_ALREADY_EXIST))
                .verify();
    }

    @Test
    void update_shouldUpdateExisting() {
        Tpp toUpdate = tppDAO.findAny();
        String updatedName = "Updated name";
        toUpdate.setName(updatedName);
        testWithUser(tppService.update(toUpdate.getId(), toUpdate))
                .expectSubscription()
                .consumeNextWith(tpp -> {
                    assertThat(tpp).isNotNull();
                    assertThat(tpp.getId()).isEqualTo(toUpdate.getId());
                    assertThat(tpp.getName()).isEqualTo(updatedName);
                })
                .verifyComplete();
    }

    @Test
    @Disabled("Pending https://jira.spring.io/browse/DATACMNS-1231")
    void update_shouldSetLastModifiedUser() {
        Tpp toUpdate = tppDAO.findAny();
        String updatedName = "Updated name";
        toUpdate.setName(updatedName);
        testWithUser(tppService.update(toUpdate.getId(), toUpdate), jarvan)
                .expectSubscription()
                .consumeNextWith(tpp -> assertThat(tpp.getLastModifiedBy()).isNotNull().isEqualTo("jarvan"))
                .verifyComplete();
    }

    @Test
    @Disabled("Waiting for Spring Data to handle security reactive context")
    void update_shouldSetLastModifiedDate() {
        Tpp toUpdate = tppDAO.findAny();
        String updatedName = "Updated name";
        toUpdate.setName(updatedName);
        testWithUser(tppService.update(toUpdate.getId(), toUpdate))
                .expectSubscription()
                .consumeNextWith(tpp -> assertThat(tpp.getLastModifiedAt()).isNotNull()
                                                                           .isCloseTo(now(), new TemporalUnitWithinOffset(2, SECONDS)))
                .verifyComplete();
    }

    @Test
    void update_shouldUpdateExistingChangingOrganizationIdentifier() {
        Tpp toUpdate = tppDAO.findAny();
        toUpdate.setNationalAccreditationData(new TppNationalAccreditation().setOrganizationIdentifier("PSDFR-BDE-UPDATED")
                                                                            .setCompetentAuthorityCountryCode("FR")
                                                                            .setCompetentAuthorityIdentifier("BDE")
                                                                            .setAuthorizationNumber("UPDATED")
                                                                            .setCompetentAuthorityName("French registry")
                                                                            .setRegistrationDate(LocalDate.of(2018, 2, 28))
                                                                            .setAccreditations(new HashSet<>()));
        testWithUser(tppService.update(toUpdate.getId(), toUpdate))
                .expectSubscription()
                .consumeNextWith(tpp -> {
                    assertThat(tpp).isNotNull();
                    assertThat(tpp.getId()).isEqualTo(toUpdate.getId());
                    assertThat(tpp.getNationalAccreditationData()).isEqualToComparingFieldByFieldRecursively(
                            toUpdate.getNationalAccreditationData());
                })
                .verifyComplete();
    }

    @Test
    void update_shouldUpdateExistingCopyingCredentials() {
        Tpp toUpdate = tppDAO.findAny();
        TppCredentials credentials = toUpdate.getCredentials();
        testWithUser(tppService.update(toUpdate.getId(), toUpdate))
                .expectSubscription()
                .consumeNextWith(tpp -> {
                    assertThat(tpp).isNotNull();
                    assertThat(tpp.getCredentials()).isEqualToComparingFieldByField(credentials);
                })
                .verifyComplete();
    }

    @Test
    void patch_shouldPatchExisting() {
        String updatedName = "Updated name";
        Tpp toUpdate = new Tpp().setName(updatedName);
        ObjectId id = tppDAO.findAny().getId();
        testWithUser(tppService.patch(id, toUpdate), draven)
                .expectSubscription()
                .consumeNextWith(tpp -> {
                    assertThat(tpp).isNotNull();
                    assertThat(tpp.getId()).isEqualTo(id);
                    assertThat(tpp.getName()).isEqualTo(updatedName);
                })
                .verifyComplete();
    }

    @Test
    @Disabled("Pending https://jira.spring.io/browse/DATACMNS-1231")
    void patch_shouldSetLastModifiedUser() {
        String updatedName = "Updated name";
        Tpp toUpdate = new Tpp().setName(updatedName);
        testWithUser(tppService.patch(tppDAO.findAny().getId(), toUpdate), jarvan)
                .expectSubscription()
                .consumeNextWith(tpp -> assertThat(tpp.getLastModifiedBy()).isNotNull().isEqualTo("jarvan"))
                .verifyComplete();
    }

    @Test
    @Disabled("Waiting for Spring Data to handle security reactive context")
    void patch_shouldSetLastModifiedDate() {
        Tpp toUpdate = new Tpp().setName("Updated name");
        testWithUser(tppService.patch(tppDAO.findAny().getId(), toUpdate))
                .expectSubscription()
                .consumeNextWith(tpp -> assertThat(tpp.getLastModifiedAt()).isNotNull()
                                                                           .isCloseTo(now(), new TemporalUnitWithinOffset(2, SECONDS)))
                .verifyComplete();
    }

    @Test
    void patch_shouldPatchExistingChangingOrganizationIdentifier() {
        Tpp toUpdate = new Tpp().setNationalAccreditationData(new TppNationalAccreditation().setOrganizationIdentifier("PSDFR-BDE-UPDATED")
                                                                                            .setCompetentAuthorityCountryCode("FR")
                                                                                            .setCompetentAuthorityIdentifier("BDE")
                                                                                            .setAuthorizationNumber("UPDATED")
                                                                                            .setCompetentAuthorityName("French registry")
                                                                                            .setRegistrationDate(LocalDate.of(2018, 2, 28))
                                                                                            .setAccreditations(new HashSet<>()));
        ObjectId id = tppDAO.findAny().getId();
        testWithUser(tppService.patch(id, toUpdate))
                .expectSubscription()
                .consumeNextWith(tpp -> {
                    assertThat(tpp).isNotNull();
                    assertThat(tpp.getId()).isEqualTo(id);
                    assertThat(tpp.getNationalAccreditationData()).isEqualToComparingFieldByFieldRecursively(
                            toUpdate.getNationalAccreditationData());
                })
                .verifyComplete();
    }

    @Test
    void patch_shouldPatchExistingCopyingCredentials() {
        Tpp toUpdate = new Tpp();
        Tpp existing = tppDAO.findAny();
        testWithUser(tppService.patch(existing.getId(), toUpdate))
                .expectSubscription()
                .consumeNextWith(tpp -> {
                    assertThat(tpp).isNotNull();
                    assertThat(tpp.getCredentials()).isEqualToComparingFieldByField(existing.getCredentials());
                })
                .verifyComplete();
    }

    @Test
    void patch_shouldPatchExistingChangingAccreditations() {
        Set<TppAccreditation> newAccreditations = Stream.of(AIS).collect(toSet());
        Tpp toUpdate = new Tpp().setAccreditations(newAccreditations);
        Tpp existing = tppDAO.findAnyByPredicate(t -> t.getNationalAccreditationData().getAccreditations().contains(AIS));
        testWithUser(tppService.patch(existing.getId(), toUpdate))
                .expectSubscription()
                .consumeNextWith(tpp -> assertThat(tpp.getAccreditations()).isEqualTo(newAccreditations))
                .verifyComplete();
    }

    @Test
    void patch_shouldFailCuzEbaIdAlreadyTaken() {
        List<Tpp> tpps = tppDAO.findAll();
        assertThat(tpps.size()).isGreaterThanOrEqualTo(2);
        String existingNationalId = tpps.get(1).getNationalAccreditationData().getOrganizationIdentifier();
        Tpp toUpdate = new Tpp().setNationalAccreditationData(
                new TppNationalAccreditation()
                        .setOrganizationIdentifier(existingNationalId)
        );
        create(tppService.patch(tppDAO.findAny().getId(), toUpdate))
                .expectSubscription()
                .consumeErrorWith(e -> expectSbsException(e, RESOURCE_ALREADY_EXIST))
                .verify();
    }

    @Test
    void findAccreditations_shouldRetrieveAccreditations() {
        testWithUser(tppService.findAccreditations(new ObjectId(tppSensodyne.getId())), draven)
                .expectSubscription()
                .expectNextCount(3)
                .verifyComplete();
    }

    @Test
    void findAccreditations_shouldBeEmpty() {
        Tpp tpp = tppDAO.create(new Tpp().setName("No accreditations")
                                         .setCredentials(new TppCredentials().setClientId("noacc").setSecret("123456")
                                                                             .setScopes(Stream.of("openid", "aisp", "pisp")
                                                                                              .collect(toSet()))));
        testWithUser(tppService.findAccreditations(tpp.getId()), draven)
                .expectSubscription()
                .verifyComplete();
    }

    @Test
    void findAccreditations_shouldFailCuzIdDoesNotExist() {
        testWithUser(tppService.findAccreditations(new ObjectId()), draven)
                .expectSubscription()
                .consumeErrorWith(e -> expectSbsException(e, RESOURCE_NOT_FOUND))
                .verify();
    }

    @Test
    void findState_shouldRetrieveStatus() {
        Tpp tpp = tppDAO.findAny();
        assertThat(tpp.getState()).isNotNull();
        create(tppService.findState(tpp.getId()))
                .expectSubscription()
                .consumeNextWith(state -> {
                    assertThat(state).isNotNull();
                    assertThat(state.getStatus()).isNotNull().isEqualTo(tpp.getState().getStatus());
                    assertThat(state.isMonitored()).isEqualTo(tpp.getState().isMonitored());
                })
                .verifyComplete();
    }

    @Test
    void findByNameContaining_shouldFindOneWithExactMatch() {
        Tpp anyTpp = tppDAO.findAny();
        create(tppService.findByNameContaining(anyTpp.getName(), DEFAULT_RANGE))
                .expectSubscription()
                .consumeNextWith(t -> {
                    assertThat(t.getId()).isEqualTo(anyTpp.getId());
                    assertThat(t.getName()).isEqualTo(anyTpp.getName());
                })
                .verifyComplete();
    }

    @Test
    void findByNameContaining_shouldFindOneIgnoringCase() {
        Tpp anyTpp = tppDAO.findAny();
        create(tppService.findByNameContaining(anyTpp.getName().toUpperCase(), DEFAULT_RANGE))
                .expectSubscription()
                .consumeNextWith(t -> {
                    assertThat(t.getId()).isEqualTo(anyTpp.getId());
                    assertThat(t.getName()).isEqualTo(anyTpp.getName());
                })
                .verifyComplete();
    }

    @Test
    void findByNameContaining_shouldFindOneWithPartialMatch() {
        create(tppService.findByNameContaining("Solidari", DEFAULT_RANGE))
                .expectSubscription()
                .consumeNextWith(t -> {
                    Tpp toMatch = createSolidarityBank();
                    assertThat(t.getName()).isEqualTo(toMatch.getName());
                })
                .verifyComplete();
    }

    @Test
    void findByNameContaining_shouldFindAtLeastTwo() {
        create(tppService.findByNameContaining("Ban", DEFAULT_RANGE))
                .expectSubscription()
                .expectNextCount(2)
                .verifyComplete();
    }

    @Test
    void findByNameContaining_shouldBeEmpty() {
        create(tppService.findByNameContaining("Unknown", DEFAULT_RANGE))
                .expectSubscription()
                .expectNextCount(0)
                .verifyComplete();
    }

    @Test
    void findByClientId_shouldFind() {
        String clientId = createSolidarityBank().getCredentials().getClientId();
        create(tppService.findByClientId(clientId))
                .expectSubscription()
                .consumeNextWith(tpp -> {
                    assertThat(tpp.getCredentials().getClientId()).isEqualTo(clientId);
                })
                .verifyComplete();
    }

    @Test
    void findByClientId_shouldBeEmpty() {
        create(tppService.findByClientId("Unknown"))
                .expectSubscription()
                .verifyComplete();
    }

    @Test
    void findState_shouldFailCuzIdDoesNotExist() {
        create(tppService.findState(new ObjectId()))
                .expectSubscription()
                .verifyError(DxpSbsException.class);
    }

    @Test
    void update_shouldFailCuzOrganizationIdentifierAlreadyTaken() {
        List<Tpp> tpps = tppDAO.findAll();
        assertThat(tpps.size()).isGreaterThanOrEqualTo(2);
        Tpp toUpdate = tpps.get(0);
        String existingOrganizationIdentifier = tpps.get(1).getNationalAccreditationData().getOrganizationIdentifier();
        toUpdate.setNationalAccreditationData(
                new TppNationalAccreditation()
                        .setOrganizationIdentifier(existingOrganizationIdentifier)
        );
        create(tppService.update(toUpdate.getId(), toUpdate))
                .expectSubscription()
                .verifyError(DxpSbsException.class);
    }

    @Test
    void update_shouldFailCuzNameAlreadyTaken() {
        List<Tpp> dbTpps = tppDAO.findAll();
        assertThat(dbTpps.size()).isGreaterThanOrEqualTo(2);
        Tpp toUpdate = dbTpps.get(0);
        String existingName = dbTpps.get(1).getName();
        toUpdate.setName(existingName);
        create(tppService.update(toUpdate.getId(), toUpdate))
                .expectSubscription()
                .consumeErrorWith(e -> expectSbsException(e, RESOURCE_ALREADY_EXIST))
                .verify();
    }

    @Test
    void update_withTppIdInBodyNull_shouldPass() {
        String updatedName = "Updated name";
        Tpp newTpp = tppDAO.findAny().setName(updatedName).setCredentials(null);
        ObjectId id = newTpp.getId();
        newTpp.setId(null);
        testWithUser(tppService.update(id, newTpp))
                .expectSubscription()
                .consumeNextWith(t -> {
                    assertThat(t.getId()).isNotNull();
                    assertThat(t.getId()).isEqualTo(id);
                }).verifyComplete();
    }

    @Test
    void findByIds_shouldFindSomeTpps() {
        List<ObjectId> allIds = tppDAO.findAll().stream().map(Tpp::getId).collect(toList());
        List<Tpp> recorded = new ArrayList<>();
        create(tppService.findByIds(allIds))
                .expectSubscription()
                .recordWith(() -> recorded)
                .expectNextCount(tppDAO.count())
                .consumeRecordedWith(l -> assertThat(l).extracting(Tpp::getId).containsExactlyInAnyOrderElementsOf(allIds))
                .verifyComplete();
    }

    @Test
    void findByIds_shouldFindOneTpp() {
        ObjectId id = tppDAO.findAny().getId();
        List<Tpp> recorded = new ArrayList<>();
        create(tppService.findByIds(listOf(id)))
                .expectSubscription()
                .recordWith(() -> recorded)
                .expectNextCount(1)
                .consumeRecordedWith(l -> assertThat(l).extracting(Tpp::getId).containsExactly(id))
                .verifyComplete();
    }

    @Test
    void findByIds_shouldNotFindAnyTpp() {
        List<ObjectId> randomsIds = listOf(ObjectId.get(), ObjectId.get(), ObjectId.get());
        create(tppService.findByIds(randomsIds))
                .expectSubscription()
                .expectNextCount(0)
                .verifyComplete();
    }

    private Optional<Tpp> idFromCache(String id) {
        return Optional.ofNullable(cacheId).map(c -> c.get(id, Tpp.class));
    }

    private Optional<Tpp> clientIdFromCache(String clientId) {
        return Optional.ofNullable(cacheClientId).map(c -> c.get(clientId, Tpp.class));
    }

    private Tpp newTpp() {
        return new Tpp()
                .setNationalAccreditationData(new TppNationalAccreditation().setOrganizationIdentifier("PSDES-BDE-3DFD21")
                                                                            .setCompetentAuthorityName("Name")
                                                                            .setCompetentAuthorityIdentifier("BDE")
                                                                            .setCompetentAuthorityCountryCode("ES")
                                                                            .setAuthorizationNumber("3DFD21")
                                                                            .setAccreditations(SetsKt.setOf(AIS, CIS, PIS)))
                .setName("EBA name")
                .setComments("EBA comments")
                .setState(new TppState().setStatus(ACTIVE).setMonitored(true))
                .setAccreditations(Stream.of(AIS, PIS).collect(toSet()));
    }
}