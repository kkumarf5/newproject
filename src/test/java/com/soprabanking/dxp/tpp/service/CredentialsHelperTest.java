package com.soprabanking.dxp.tpp.service;

import com.soprabanking.dxp.tpp.model.entity.Tpp;
import com.soprabanking.dxp.tpp.model.entity.TppCredentials;
import com.soprabanking.dxp.tpp.model.entity.TppNationalAccreditation;
import kotlin.collections.SetsKt;
import org.junit.jupiter.api.Test;

import static com.soprabanking.dxp.tpp.service.CredentialsHelper.generateCredentials;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

/**
 * Tests related to credentials business logic
 */
class CredentialsHelperTest {

    @Test
    void generateCredentials_shouldCreateClientIdMatchingOrganizationIdentifier() {
        String organizationIdentifier = "abcbank";
        Tpp tpp = new Tpp().setNationalAccreditationData(new TppNationalAccreditation().setOrganizationIdentifier(organizationIdentifier));
        TppCredentials credentials = generateCredentials(tpp);
        assertThat(credentials.getClientId()).isEqualTo(organizationIdentifier);
    }

    @Test
    void generateCredentials_shouldCreateClientIdAndKeepTrustedRedirectUrls() {
        String organizationIdentifier = "abcbank";
        Tpp tpp = new Tpp().setNationalAccreditationData(new TppNationalAccreditation().setOrganizationIdentifier(organizationIdentifier))
                           .setCredentials(new TppCredentials().setTrustedRedirectUrls(SetsKt.setOf("api.redirectUrl.com")));
        TppCredentials credentials = generateCredentials(tpp);
        assertThat(credentials.getClientId()).isEqualTo(organizationIdentifier);
        assertThat(credentials.getTrustedRedirectUrls().contains("api.redirectUrl.com")).isTrue();
    }

    @Test
    void generateCredentials_shouldCreateClientIdRemovingSpecialChars() {
        String organizationIdentifier = "abc&bank";
        Tpp tpp = new Tpp().setNationalAccreditationData(new TppNationalAccreditation().setOrganizationIdentifier(organizationIdentifier));
        TppCredentials credentials = generateCredentials(tpp);
        assertThat(credentials.getClientId()).isEqualTo("abcbank");
    }

    @Test
    void generateCredentials_shouldCreateClientIdRemovingAccents() {
        String organizationIdentifier = "âbcbank";
        Tpp tpp = new Tpp().setNationalAccreditationData(new TppNationalAccreditation().setOrganizationIdentifier(organizationIdentifier));
        TppCredentials credentials = generateCredentials(tpp);
        assertThat(credentials.getClientId()).isEqualTo("abcbank");
    }

    @Test
    void generateCredentials_shouldCreateClientIdRemovingSpaces() {
        String organizationIdentifier = "abc bank";
        Tpp tpp = new Tpp().setNationalAccreditationData(new TppNationalAccreditation().setOrganizationIdentifier(organizationIdentifier));
        TppCredentials credentials = generateCredentials(tpp);
        assertThat(credentials.getClientId()).isEqualTo("abcbank");
    }

    @Test
    void generateCredentials_shouldCreateClientIdRemovingSpacesAccentsAndSpecialChars() {
        String organizationIdentifier = "âbc & bank";
        Tpp tpp = new Tpp().setNationalAccreditationData(new TppNationalAccreditation().setOrganizationIdentifier(organizationIdentifier));
        TppCredentials credentials = generateCredentials(tpp);
        assertThat(credentials.getClientId()).isEqualTo("abcbank");
    }

    @Test
    void generateCredentials_shouldGenerateDefaultClientIdIfNameEmpty() {
        String organizationIdentifier = "%$£";
        Tpp tpp = new Tpp().setNationalAccreditationData(new TppNationalAccreditation().setOrganizationIdentifier(organizationIdentifier));
        TppCredentials credentials = generateCredentials(tpp);
        assertThat(credentials.getClientId()).isNotBlank();
    }

    @Test
    void generateCredentials_shouldCreatePasswordWith6Chars() {
        String organizationIdentifier = "abcbank";
        Tpp tpp = new Tpp().setNationalAccreditationData(new TppNationalAccreditation().setOrganizationIdentifier(organizationIdentifier));
        TppCredentials credentials = generateCredentials(tpp);
        assertThat(credentials.getSecret()).isNotBlank().hasSize(6);
    }
}