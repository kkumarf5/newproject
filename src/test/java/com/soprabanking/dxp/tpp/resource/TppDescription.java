package com.soprabanking.dxp.tpp.resource;

import com.soprabanking.dxp.tpp.model.entity.TppAccreditation;
import com.soprabanking.dxp.tpp.model.entity.TppContactType;
import com.soprabanking.dxp.tpp.model.entity.TppStatus;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.restdocs.payload.RequestFieldsSnippet;
import org.springframework.restdocs.payload.ResponseFieldsSnippet;
import org.springframework.restdocs.request.ParameterDescriptor;

import java.util.List;
import java.util.stream.Stream;

import static java.lang.String.format;
import static java.util.Arrays.stream;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;
import static org.springframework.restdocs.payload.JsonFieldType.ARRAY;
import static org.springframework.restdocs.payload.JsonFieldType.BOOLEAN;
import static org.springframework.restdocs.payload.JsonFieldType.OBJECT;
import static org.springframework.restdocs.payload.JsonFieldType.STRING;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;

class TppDescription {

    static ResponseFieldsSnippet tppsDesc() {
        return responseFields(fieldWithPath("[]").type(ARRAY).description("An array of TPP (third party provider)"))
                .andWithPrefix("[].", tppDesc())
                .andWithPrefix("[].state.", stateDesc())
                .andWithPrefix("[].contact.", contactDesc())
                .andWithPrefix("[].nationalAccreditationData.", nationalAccreditationDataDesc())
                .andWithPrefix("[].physicalContacts[].", physicalContactDesc())
                .andWithPrefix("[].credentials.",
                               fieldWithPath("trustedRedirectUrls").type(ARRAY).description("OAuth2 redirect urls to the TPP"));
    }

    static ResponseFieldsSnippet tppsContactAndNameDesc() {
        return responseFields(fieldWithPath("[]").type(ARRAY).description("An array of TPP (third party provider)"),
                              fieldWithPath("[].id").type(STRING).description("Auto generated TPP identifier").optional(),
                              fieldWithPath("[].name").type(STRING).description("Name of the TPP"))
                .andWithPrefix("[].contact.", contactDesc());
    }

    static FieldDescriptor[] tppDesc() {
        return new FieldDescriptor[]{
                fieldWithPath("id").type(STRING).description("Auto generated TPP identifier").optional(),
                fieldWithPath("name").type(STRING).description("Name of the TPP"),
                fieldWithPath("webSite").type(STRING).description("Web site of the TPP"),
                fieldWithPath("comments").type(STRING).description("Some comments"),
                fieldWithPath("credentials.trustedRedirectUrls").type(ARRAY).description("OAuth2 redirect urls to the TPP"),
                fieldWithPath("accreditations").type(ARRAY)
                        .description(format("Arrays of accreditations. Could be any of these values :  %s",
                                            stream(TppAccreditation.values()).map(TppAccreditation::name).collect(joining(","))))};
    }

    static ParameterDescriptor tppIdPathDesc() {
        return parameterWithName("id").description("Auto generated TPP identifier");
    }

    static ParameterDescriptor tppNamePathDesc() {
        return parameterWithName("name").description("Name used by zero, one or multiple TPPs");
    }

    static ParameterDescriptor clientIdParamDesc() {
        return parameterWithName("clientId").description("Searched TPP's client id");
    }

    static ParameterDescriptor organizationIdentifierParamDesc() {
        return parameterWithName("organizationIdentifier").description("Searched TPP's organization identifier");
    }

    static ParameterDescriptor tppIdsParamDesc() {
        return parameterWithName("ids").description("List of TPP ids");
    }

    static FieldDescriptor tppAccreditationsDesc() {
        return fieldWithPath("[]").type(ARRAY)
                                  .description(format("Arrays of accreditations. Could be any of these values : %s",
                                                      stream(TppAccreditation.values()).map(TppAccreditation::name).collect(joining(","))));
    }

    static FieldDescriptor[] contactDesc() {
        return new FieldDescriptor[]{
                fieldWithPath("email").type(STRING).description("Contact email address"),
                fieldWithPath("phoneNumber").type(STRING).description("Contact phone number"),
                fieldWithPath("city").type(STRING).description("Contact city"),
                fieldWithPath("addressLines").type(ARRAY).description("Contact address lines"),
                fieldWithPath("postCode").type(STRING).description("Contact post code"),
                fieldWithPath("country").type(STRING).description("Contact contry")};
    }

    static FieldDescriptor[] nationalAccreditationDataDesc() {
        return new FieldDescriptor[]{
                fieldWithPath("organizationIdentifier").type(STRING).description("Organization identifier of the tpp"),
                fieldWithPath("competentAuthorityIdentifier").type(STRING).description(
                        "Identifier of the competent authority delivering the tpp national accreditation"),
                fieldWithPath("competentAuthorityName").type(STRING).description(
                        "Name of the competent authority delivering the tpp national accreditation"),
                fieldWithPath("competentAuthorityCountryCode").type(STRING).description(
                        "ISO 3166 two-letter country code of the competent authority delivering the tpp accreditation"),
                fieldWithPath("authorizationNumber").type(STRING).description(
                        "Authorization number of the competent authority delivered to the tpp"),
                fieldWithPath("competentAuthorityStatus").type(STRING).description("Status of the tpp in the competent authority registry"),
                fieldWithPath("registrationDate").type(STRING).description("Registration date of the national accreditation"),
                fieldWithPath("accreditations").type(ARRAY)
                        .description(format("Arrays of accreditations. Could be any of these values :  %s",
                                            stream(TppAccreditation.values()).map(TppAccreditation::name).collect(joining(","))))};
    }

    static FieldDescriptor[] physicalContactDesc() {
        return new FieldDescriptor[]{
                fieldWithPath("email").type(STRING).description("Individual business email of the contact acting on behalf of the tpp"),
                fieldWithPath("name").type(STRING).description("Name of the contact acting on behalf of the tpp"),
                fieldWithPath("firstName").type(STRING).description("First name of the contact acting on behalf of the tpp"),
                fieldWithPath("phoneNumber").type(STRING).description(
                        "Individual mobile number of the contact acting on behalf of the tpp"),
                fieldWithPath("jobTitle").type(STRING).description("Job title of the contact acting on behalf of the tpp"),
                fieldWithPath("contactType").type(STRING)
                        .description(format("Type of contact acting on behalf of the tpp. Could be any of these values :  %s",
                                            stream(TppContactType.values()).map(TppContactType::name).collect(joining(","))))};
    }

    static FieldDescriptor[] stateDesc() {
        return new FieldDescriptor[]{
                fieldWithPath("status").type(STRING)
                        .description(format("TPP status could be any of these values %s",
                                            stream(TppStatus.values()).map(TppStatus::name).collect(joining(",")))),
                fieldWithPath("monitored").type(BOOLEAN).description("Indicator checking if the TPP is monitored")};
    }

    static FieldDescriptor[] toOptional(FieldDescriptor[] fieldDescriptors) {
        return stream(fieldDescriptors)
                .map(FieldDescriptor::optional)
                .toArray(FieldDescriptor[]::new);
    }

    static ResponseFieldsSnippet detailResponseFields() {
        return responseFields(tppDesc())
                .andWithPrefix("contact.", contactDesc())
                .andWithPrefix("state.", stateDesc())
                .andWithPrefix("nationalAccreditationData.", nationalAccreditationDataDesc())
                .andWithPrefix("physicalContacts[].", physicalContactDesc());
    }

    static RequestFieldsSnippet detailRequestFields() {
        return requestFields(tppDesc())
                .andWithPrefix("contact.", contactDesc())
                .andWithPrefix("state.", stateDesc())
                .andWithPrefix("nationalAccreditationData.", nationalAccreditationDataDesc())
                .andWithPrefix("credentials.",
                               fieldWithPath("trustedRedirectUrls").type(ARRAY).description("OAuth2 redirect urls to the TPP"))
                .andWithPrefix("physicalContacts[].", physicalContactDesc());
    }

    static ResponseFieldsSnippet detailResponseFieldsWithCredentials() {
        return responseFields(tppDesc())
                .andWithPrefix("contact.", contactDesc())
                .andWithPrefix("state.", stateDesc())
                .andWithPrefix("credentials.",
                               fieldWithPath("tppId").type(STRING).description("TPP identifier").optional(),
                               fieldWithPath("clientId").type(STRING).description("OAuth2 client identifier"),
                               fieldWithPath("secret").type(STRING).description("OAuth2 client secret"),
                               fieldWithPath("trustedRedirectUrls").type(ARRAY).description("OAuth2 redirect urls to the TPP"),
                               fieldWithPath("scopes").type(ARRAY).description("Array of OAuth2 scopes"))
                .andWithPrefix("nationalAccreditationData.", nationalAccreditationDataDesc())
                .andWithPrefix("physicalContacts[].", physicalContactDesc());
    }

    static RequestFieldsSnippet optionalRequestFields() {
        List<FieldDescriptor> tppFields = stream(toOptional(tppDesc())).collect(toList());
        tppFields.addAll(Stream.of(
                fieldWithPath("contact").type(OBJECT).description("Contact details").optional(),
                fieldWithPath("state").type(OBJECT).description("State details").optional(),
                fieldWithPath("nationalAccreditationData").type(OBJECT).description("National Accreditation details").optional(),
                fieldWithPath("credentials").type(OBJECT).description("TPP credentials").optional(),
                fieldWithPath("physicalContacts").type(ARRAY).description("List of Physical Contacts").optional()).collect(toList()));
        return requestFields(tppFields.toArray(new FieldDescriptor[0]))
                .andWithPrefix("contact.", toOptional(contactDesc()))
                .andWithPrefix("state.", toOptional(stateDesc()))
                .andWithPrefix("nationalAccreditationData.", toOptional(nationalAccreditationDataDesc()))
                .andWithPrefix("credentials.",
                               fieldWithPath("trustedRedirectUrls").type(ARRAY).description("OAuth2 redirect urls to the TPP"))
                .andWithPrefix("physicalContacts[].", toOptional(physicalContactDesc()));
    }
}