package com.soprabanking.dxp.tpp.resource;

import com.soprabanking.dxp.tpp.dao.TppDAO;
import com.soprabanking.dxp.tpp.model.api.TppCredentialApiDTO;
import com.soprabanking.dxp.tpp.model.entity.Tpp;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.restdocs.payload.ResponseFieldsSnippet;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.util.List;

import static com.soprabanking.dxp.commons.psd2.security.test.Psd2PersonaId.HEIMERDINGER;
import static com.soprabanking.dxp.commons.test.WebTestClientExtensions.configureWebTestClient;
import static com.soprabanking.dxp.tpp.mapper.TppCredentialsMapper.map;
import static com.soprabanking.dxp.tpp.resource.UrlConstants.CREDENTIALS;
import static com.soprabanking.dxp.tpp.resource.UrlConstants.TPPS;
import static java.util.stream.Collectors.toList;
import static org.apache.http.HttpHeaders.AUTHORIZATION;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.springframework.cloud.contract.wiremock.restdocs.SpringCloudContractRestDocs.dslContract;
import static org.springframework.http.HttpHeaders.ACCEPT_RANGES;
import static org.springframework.http.HttpHeaders.CONTENT_RANGE;
import static org.springframework.http.HttpStatus.PARTIAL_CONTENT;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.restdocs.payload.JsonFieldType.ARRAY;
import static org.springframework.restdocs.payload.JsonFieldType.STRING;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.webtestclient.WebTestClientRestDocumentation.document;

/**
 * Tests related to {@link CredentialsResource}
 */
@SpringBootTest
@AutoConfigureWebTestClient
@ExtendWith(RestDocumentationExtension.class)
class CredentialsResourceTest {

    private final TppDAO tppDAO;

    private WebTestClient webTestClient;

    public CredentialsResourceTest(@Autowired TppDAO tppDAO, @Autowired WebTestClient webTestClient) {
        this.tppDAO = tppDAO;
        this.webTestClient = webTestClient;
    }

    @BeforeEach
    void beforeEach(RestDocumentationContextProvider restDoc) {
        tppDAO.initTest();
        webTestClient = configureWebTestClient(webTestClient, restDoc);
    }

    @Test
    void findAll_shouldFindSomeCredentials() {
        List<TppCredentialApiDTO> credentials =
                tppDAO.findAll().stream().map(Tpp::getCredentials).map(c -> map(c, new ObjectId())).collect(toList());
        int count = tppDAO.count();
        webTestClient.get()
                     .uri(TPPS + CREDENTIALS)
                     .header(AUTHORIZATION, HEIMERDINGER.bearer())
                     .exchange()
                     .expectStatus().isEqualTo(PARTIAL_CONTENT)
                     .expectHeader().contentType(APPLICATION_JSON_UTF8)
                     .expectHeader().valueEquals(ACCEPT_RANGES, "tppcredentials")
                     .expectHeader().valueEquals(CONTENT_RANGE, "tppcredentials 0-49/" + count)
                     .expectBodyList(TppCredentialApiDTO.class)
                     .hasSize(count)
                     .consumeWith(
                             c -> assertThat(c.getResponseBody()).usingElementComparatorIgnoringFields("tppId").containsAll(credentials))
                     .consumeWith(document("credentials_findAll", credentialsRespDesc(), dslContract()));
    }

    @Test
    void findAll_shouldNotFindAnyTppCredentials() {
        tppDAO.deleteAll();
        webTestClient.get()
                     .uri(TPPS + CREDENTIALS)
                     .header(AUTHORIZATION, HEIMERDINGER.bearer())
                     .exchange()
                     .expectStatus().isEqualTo(PARTIAL_CONTENT)
                     .expectHeader().contentType(APPLICATION_JSON_UTF8)
                     .expectHeader().valueEquals(ACCEPT_RANGES, "tppcredentials")
                     .expectHeader().valueEquals(CONTENT_RANGE, "tppcredentials 0-49/0")
                     .expectBodyList(TppCredentialApiDTO.class)
                     .hasSize(0);
    }

    private ResponseFieldsSnippet credentialsRespDesc() {
        return responseFields(fieldWithPath("[]").type(ARRAY).description("An array of OAuth credentials"))
                .andWithPrefix("[].",
                               fieldWithPath("tppId").type(STRING).description("TPP identifier").optional(),
                               fieldWithPath("clientId").type(STRING).description("OAuth2 client identifier"),
                               fieldWithPath("secret").type(STRING).description("OAuth2 client secret"),
                               fieldWithPath("trustedRedirectUrls").type(ARRAY).description("OAuth2 redirect urls to TPP"),
                               fieldWithPath("scopes").type(ARRAY).description("Array of OAuth2 scopes"));
    }
}