package com.soprabanking.dxp.tpp.resource;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.soprabanking.dxp.commons.error.DxpSecurityCode;
import com.soprabanking.dxp.commons.error.SbsExceptionDTO;
import com.soprabanking.dxp.tpp.dao.TppDAO;
import com.soprabanking.dxp.tpp.mapper.TppMapper;
import com.soprabanking.dxp.tpp.model.api.TppApiDTO;
import com.soprabanking.dxp.tpp.model.api.TppContactApiDTO;
import com.soprabanking.dxp.tpp.model.api.TppCredentialApiDTO;
import com.soprabanking.dxp.tpp.model.api.TppNationalAccreditationApiDTO;
import com.soprabanking.dxp.tpp.model.api.TppPhysicalContactApiDTO;
import com.soprabanking.dxp.tpp.model.api.TppStateApiDTO;
import com.soprabanking.dxp.tpp.model.entity.Tpp;
import com.soprabanking.dxp.tpp.model.entity.TppAccreditation;
import com.soprabanking.dxp.tpp.model.entity.TppContactType;
import com.soprabanking.dxp.tpp.model.entity.TppCredentials;
import com.soprabanking.dxp.tpp.model.entity.TppState;
import kotlin.collections.SetsKt;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.http.HttpStatus;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import static com.soprabanking.dxp.commons.error.DxpBusinessCode.INVALID_CONTENT;
import static com.soprabanking.dxp.commons.error.DxpBusinessCode.RESOURCE_ALREADY_EXIST;
import static com.soprabanking.dxp.commons.error.DxpBusinessCode.RESOURCE_NOT_FOUND;
import static com.soprabanking.dxp.commons.error.DxpSecurityCode.INSUFFICIENT_PERMISSION;
import static com.soprabanking.dxp.commons.psd2.security.test.PersonaFactory.gragas;
import static com.soprabanking.dxp.commons.psd2.security.test.PersonaFactory.lux;
import static com.soprabanking.dxp.commons.psd2.security.test.Psd2PersonaId.ASHE;
import static com.soprabanking.dxp.commons.psd2.security.test.Psd2PersonaId.DRAVEN;
import static com.soprabanking.dxp.commons.psd2.security.test.Psd2PersonaId.GRAGAS;
import static com.soprabanking.dxp.commons.psd2.security.test.Psd2PersonaId.HEIMERDINGER;
import static com.soprabanking.dxp.commons.psd2.security.test.Psd2PersonaId.LUX;
import static com.soprabanking.dxp.commons.psd2.security.test.TppFactory.tppSensodyne;
import static com.soprabanking.dxp.commons.test.ApiDocumentationKt.paginationResponseHeaderDesc;
import static com.soprabanking.dxp.commons.test.ApiDocumentationKt.rangeDesc;
import static com.soprabanking.dxp.commons.test.ExceptionVerifiersKt.errorFields;
import static com.soprabanking.dxp.commons.test.ExceptionVerifiersKt.verifySbsExceptionDTO;
import static com.soprabanking.dxp.commons.test.WebTestClientExtensions.configureWebTestClient;
import static com.soprabanking.dxp.tpp.mapper.TppMapper.map;
import static com.soprabanking.dxp.tpp.model.api.TppViews.Basic;
import static com.soprabanking.dxp.tpp.model.entity.CompetentAuthorityStatus.AUTHORISED;
import static com.soprabanking.dxp.tpp.model.entity.TppAccreditation.AIS;
import static com.soprabanking.dxp.tpp.model.entity.TppAccreditation.CIS;
import static com.soprabanking.dxp.tpp.model.entity.TppAccreditation.PIS;
import static com.soprabanking.dxp.tpp.model.entity.TppStatus.ACTIVE;
import static com.soprabanking.dxp.tpp.resource.TppDescription.clientIdParamDesc;
import static com.soprabanking.dxp.tpp.resource.TppDescription.detailRequestFields;
import static com.soprabanking.dxp.tpp.resource.TppDescription.detailResponseFields;
import static com.soprabanking.dxp.tpp.resource.TppDescription.detailResponseFieldsWithCredentials;
import static com.soprabanking.dxp.tpp.resource.TppDescription.optionalRequestFields;
import static com.soprabanking.dxp.tpp.resource.TppDescription.organizationIdentifierParamDesc;
import static com.soprabanking.dxp.tpp.resource.TppDescription.stateDesc;
import static com.soprabanking.dxp.tpp.resource.TppDescription.tppAccreditationsDesc;
import static com.soprabanking.dxp.tpp.resource.TppDescription.tppIdPathDesc;
import static com.soprabanking.dxp.tpp.resource.TppDescription.tppIdsParamDesc;
import static com.soprabanking.dxp.tpp.resource.TppDescription.tppNamePathDesc;
import static com.soprabanking.dxp.tpp.resource.TppDescription.tppsContactAndNameDesc;
import static com.soprabanking.dxp.tpp.resource.TppDescription.tppsDesc;
import static com.soprabanking.dxp.tpp.resource.UrlConstants.ID;
import static com.soprabanking.dxp.tpp.resource.UrlConstants.TPPS;
import static java.lang.String.join;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import static kotlin.collections.CollectionsKt.listOf;
import static org.apache.http.HttpHeaders.AUTHORIZATION;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.springframework.cloud.contract.wiremock.restdocs.SpringCloudContractRestDocs.dslContract;
import static org.springframework.http.HttpHeaders.ACCEPT_RANGES;
import static org.springframework.http.HttpHeaders.CONTENT_RANGE;
import static org.springframework.http.HttpStatus.PARTIAL_CONTENT;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.restdocs.webtestclient.WebTestClientRestDocumentation.document;

/**
 * Tests related to {@link TppResource}
 */
@SpringBootTest
@AutoConfigureWebTestClient
@ExtendWith(RestDocumentationExtension.class)
class TppResourceTest {

    private final ObjectMapper mapper;

    private final TppDAO tppDAO;

    private final Cache cacheId;

    private final Cache cacheClientId;

    private WebTestClient webTestClient;

    public TppResourceTest(@Autowired WebTestClient webTestClient,
                           @Autowired ObjectMapper mapper,
                           @Autowired TppDAO tppDAO,
                           @Autowired CacheManager cacheManager) {
        this.webTestClient = webTestClient;
        this.mapper = mapper;
        this.tppDAO = tppDAO;
        cacheId = cacheManager.getCache("tppsById");
        cacheClientId = cacheManager.getCache("tppsByClientId");
    }

    @BeforeEach
    void beforeEach(RestDocumentationContextProvider restDoc) {
        webTestClient = configureWebTestClient(webTestClient, restDoc);
        tppDAO.initTest();
        cacheId.clear();
        cacheClientId.clear();
    }

    @Test
    void findAll_shouldFindSomeTpps() {
        List<TppApiDTO> dbTpps = tppDAO.findAll().stream().map(TppMapper::map).collect(toList());
        webTestClient.get()
                     .uri(TPPS + "?range=0-49")
                     .header(AUTHORIZATION, DRAVEN.bearer())
                     .exchange()
                     .expectStatus().isEqualTo(PARTIAL_CONTENT)
                     .expectHeader().contentType(APPLICATION_JSON_UTF8)
                     .expectHeader().valueEquals(ACCEPT_RANGES, "tpps")
                     .expectHeader().valueEquals(CONTENT_RANGE, "tpps 0-49/" + dbTpps.size())
                     .expectBodyList(TppApiDTO.class)
                     .consumeWith(t -> {
                         List<TppApiDTO> tpps = t.getResponseBody();
                         assertThat(tpps).isNotEmpty().hasSameSizeAs(dbTpps);
                     })
                     //TODO request header documentation
                     .consumeWith(document("tpps_findAll",
                                           tppsDesc(),
                                           requestParameters(rangeDesc()),
                                           paginationResponseHeaderDesc(),
                                           dslContract()));
    }

    @Test
    void findAccreditationsForBank_shouldFindSomeAccreditations() {
        webTestClient.get()
                     .uri(TPPS + ID + "/accreditations", tppSensodyne.getId())
                     .header(AUTHORIZATION, DRAVEN.bearer())
                     .exchange()
                     .expectStatus().isOk()
                     .expectHeader().contentType(APPLICATION_JSON_UTF8)
                     .expectBodyList(TppAccreditation.class)
                     .consumeWith(l -> assertThat(l.getResponseBody()).isNotEmpty())
                     .consumeWith(
                             document("tpps_findAccreditations", pathParameters(tppIdPathDesc()), responseFields(tppAccreditationsDesc()),
                                      dslContract()));
    }

    @Test
    void findAccreditationsForBank_shouldBeEmpty() {
        TppCredentials credentials = new TppCredentials().setClientId("noacc")
                                                         .setSecret("123456")
                                                         .setScopes(Stream.of("openid", "aisp", "pisp").collect(toSet()));
        Tpp tpp = tppDAO.create(new Tpp().setName("No accreditations").setCredentials(credentials));
        webTestClient.get()
                     .uri(TPPS + ID + "/accreditations", tpp.getId())
                     .header(AUTHORIZATION, DRAVEN.bearer())
                     .exchange()
                     .expectStatus().isOk()
                     .expectHeader().contentType(APPLICATION_JSON_UTF8)
                     .expectBodyList(TppApiDTO.class)
                     .hasSize(0)
                     .consumeWith(document("tpps_findAccreditationsEmpty", dslContract()));
    }

    @Test
    void findAccreditationsForBank_shouldFailCuzIdNotFound() {
        webTestClient.get()
                     .uri(TPPS + ID + "/accreditations", new ObjectId().toString())
                     .header(AUTHORIZATION, DRAVEN.bearer())
                     .exchange()
                     .expectStatus().isNotFound()
                     .expectHeader().contentType(APPLICATION_JSON_UTF8)
                     .expectBody(SbsExceptionDTO.class)
                     .consumeWith(e -> verifySbsExceptionDTO(e, RESOURCE_NOT_FOUND))
                     .consumeWith(document("tpps_findAccreditationsForBankIdNotFound",
                                           dslContract()));
    }

    @Test
    void findAccreditationsForTppUser_shouldFindSomeAccreditations() {
        webTestClient.get()
                     .uri(TPPS + ID + "/accreditations", lux.getTppInformation().getId())
                     .header(AUTHORIZATION, LUX.bearer())
                     .exchange()
                     .expectStatus().isOk()
                     .expectHeader().contentType(APPLICATION_JSON_UTF8)
                     .expectBodyList(TppAccreditation.class)
                     .consumeWith(l -> assertThat(l.getResponseBody()).containsExactlyInAnyOrder(TppAccreditation.values()))
                     .consumeWith(document("tpps_findAccreditations",
                                           pathParameters(tppIdPathDesc()),
                                           responseFields(tppAccreditationsDesc()),
                                           dslContract()));
    }

    @Test
    void findByIdBasic_withTppGragas_shouldFailToFindOneBelongingToTppLux() {
        webTestClient.get()
                     .uri(TPPS + ID + "/accreditations", lux.getTppInformation().getId())
                     .header(AUTHORIZATION, GRAGAS.bearer())
                     .exchange()
                     .expectStatus().isForbidden()
                     .expectHeader().contentType(APPLICATION_JSON_UTF8)
                     .expectBody(SbsExceptionDTO.class)
                     .consumeWith(e -> verifySbsExceptionDTO(e, DxpSecurityCode.INSUFFICIENT_PERMISSION))
                     .consumeWith(document("tpps_findAccreditations_FailAccessDataNotBelongingTohim", errorFields(), dslContract()));
    }

    @Test
    void findAccreditationsForTppUser_shouldBeEmpty() {
        webTestClient.get()
                     .uri(TPPS + ID + "/accreditations", gragas.getTppInformation().getId())
                     .header(AUTHORIZATION, GRAGAS.bearer())
                     .exchange()
                     .expectStatus().isOk()
                     .expectHeader().contentType(APPLICATION_JSON_UTF8)
                     .expectBodyList(TppApiDTO.class)
                     .hasSize(0)
                     .consumeWith(document("tpps_findAccreditationsEmpty", dslContract()));
    }

    @Test
    void findAll_shouldNotFindAnyTpp() {
        tppDAO.deleteAll();
        webTestClient.get()
                     .uri(TPPS + "?range=0-49")
                     .header(AUTHORIZATION, DRAVEN.bearer())
                     .exchange()
                     .expectStatus().isEqualTo(PARTIAL_CONTENT)
                     .expectHeader().contentType(APPLICATION_JSON_UTF8)
                     .expectHeader().valueEquals(ACCEPT_RANGES, "tpps")
                     .expectHeader().valueEquals(CONTENT_RANGE, "tpps 0-49/0")
                     .expectBodyList(TppApiDTO.class)
                     .hasSize(0);
    }

    @Test
    void findById_shouldFindATppWithNonAISAccreditation() {
        TppApiDTO dbTpp = map(tppDAO.findById(new ObjectId("5b22859f26f9690d8bbd1761")));
        webTestClient.get()
                     .uri(TPPS + ID, dbTpp.getId())
                     .header(AUTHORIZATION, DRAVEN.bearer())
                     .exchange()
                     .expectStatus().isOk()
                     .expectHeader().contentType(APPLICATION_JSON_UTF8)
                     .expectBody(TppApiDTO.class)
                     .consumeWith(t -> {
                         assertThat(t.getResponseBody()).isEqualToComparingFieldByFieldRecursively(dbTpp.setCredentials(
                                 new TppCredentialApiDTO().setTrustedRedirectUrls(dbTpp.getCredentials().getTrustedRedirectUrls())));
                         assertThat(t.getResponseBody().getAccreditations()).doesNotContain(TppAccreditation.AIS);
                     })
                     .consumeWith(
                             document("tpps_findByIdWithNonAISAccreditation",
                                      dslContract()));
    }

    @Test
    void findByIdBasic_shouldFindOneBlockedForBankUser() {
        TppApiDTO dbTpp = map(tppDAO.findById(TppDAO.tppIdBlocked));
        webTestClient.get()
                     .uri(TPPS + ID, TppDAO.tppIdBlocked)
                     .header(AUTHORIZATION, DRAVEN.bearer())
                     .exchange()
                     .expectStatus().isOk()
                     .expectHeader().contentType(APPLICATION_JSON_UTF8)
                     .expectBody(TppApiDTO.class)
                     .consumeWith(t -> assertThat(t.getResponseBody()).isEqualToComparingFieldByFieldRecursively(dbTpp.setCredentials(
                             new TppCredentialApiDTO().setTrustedRedirectUrls(dbTpp.getCredentials().getTrustedRedirectUrls()))))
                     .consumeWith(
                             document("tpps_findByIdBlocked_forBankUser", pathParameters(tppIdPathDesc()), detailResponseFields(),
                                      dslContract()));
    }

    @Test
    void findByIdBasic_shouldFindOneBlockedForDxpTechnicalUser() {
        TppApiDTO dbTpp = map(tppDAO.findById(TppDAO.tppIdBlocked));
        webTestClient.get()
                     .uri(TPPS + ID, TppDAO.tppIdBlocked)
                     .header(AUTHORIZATION, HEIMERDINGER.bearer())
                     .exchange()
                     .expectStatus().isOk()
                     .expectHeader().contentType(APPLICATION_JSON_UTF8)
                     .expectBody(TppApiDTO.class)
                     .consumeWith(t -> assertThat(t.getResponseBody()).isEqualToComparingFieldByFieldRecursively(dbTpp.setCredentials(
                             new TppCredentialApiDTO().setTrustedRedirectUrls(dbTpp.getCredentials().getTrustedRedirectUrls()))))
                     .consumeWith(
                             document("tpps_findByIdBlocked_forDxpTechnicalUser", pathParameters(tppIdPathDesc()), detailResponseFields(),
                                      dslContract()));
    }

    @Test
    void findByIdBasic_shouldFindOneWithoutAccreditationsForBankUser() {
        TppApiDTO dbTpp = map(tppDAO.findById(TppDAO.tppIdActiveWithoutAccreditations));
        webTestClient.get()
                     .uri(TPPS + ID, TppDAO.tppIdActiveWithoutAccreditations)
                     .header(AUTHORIZATION, DRAVEN.bearer())
                     .exchange()
                     .expectStatus().isOk()
                     .expectHeader().contentType(APPLICATION_JSON_UTF8)
                     .expectBody(TppApiDTO.class)
                     .consumeWith(t -> assertThat(t.getResponseBody()).isEqualToComparingFieldByFieldRecursively(dbTpp.setCredentials(
                             new TppCredentialApiDTO().setTrustedRedirectUrls(dbTpp.getCredentials().getTrustedRedirectUrls()))))
                     .consumeWith(
                             document("tpps_findByIdWithoutAccreditations_forBankUser", pathParameters(tppIdPathDesc()),
                                      detailResponseFields(),
                                      dslContract()));
    }

    @Test
    void findByIdBasic_shouldFindOneWithoutAccreditationsForDxpTechnicalUser() {
        TppApiDTO dbTpp = map(tppDAO.findById(TppDAO.tppIdActiveWithoutAccreditations));
        webTestClient.get()
                     .uri(TPPS + ID, TppDAO.tppIdActiveWithoutAccreditations)
                     .header(AUTHORIZATION, HEIMERDINGER.bearer())
                     .exchange()
                     .expectStatus().isOk()
                     .expectHeader().contentType(APPLICATION_JSON_UTF8)
                     .expectBody(TppApiDTO.class)
                     .consumeWith(t -> assertThat(t.getResponseBody()).isEqualToComparingFieldByFieldRecursively(dbTpp.setCredentials(
                             new TppCredentialApiDTO().setTrustedRedirectUrls(dbTpp.getCredentials().getTrustedRedirectUrls()))))
                     .consumeWith(document("tpps_findByIdWithoutAccreditations_forDxpTechnicalUser", pathParameters(tppIdPathDesc()),
                                           detailResponseFields(),
                                           dslContract()));
    }

    @Test
    void findByIdBasic_shouldFindOneWithAccreditationsForBankUser() {
        TppApiDTO dbTpp = map(tppDAO.findById(TppDAO.tppIdActiveWithAccreditations));
        webTestClient.get()
                     .uri(TPPS + ID, TppDAO.tppIdActiveWithAccreditations)
                     .header(AUTHORIZATION, DRAVEN.bearer())
                     .exchange()
                     .expectStatus().isOk()
                     .expectHeader().contentType(APPLICATION_JSON_UTF8)
                     .expectBody(TppApiDTO.class)
                     .consumeWith(t -> assertThat(t.getResponseBody()).isEqualToComparingFieldByFieldRecursively(dbTpp.setCredentials(
                             new TppCredentialApiDTO().setTrustedRedirectUrls(dbTpp.getCredentials().getTrustedRedirectUrls()))))
                     .consumeWith(
                             document("tpps_findByIdWithAccreditations_forBankUser", pathParameters(tppIdPathDesc()),
                                      detailResponseFields(),
                                      dslContract()));
    }

    @Test
    void findByIdBasic_shouldFindOneWithAccreditationsForDxpTechnicalUser() {
        TppApiDTO dbTpp = map(tppDAO.findById(TppDAO.tppIdActiveWithAccreditations));
        webTestClient.get()
                     .uri(TPPS + ID, TppDAO.tppIdActiveWithAccreditations)
                     .header(AUTHORIZATION, HEIMERDINGER.bearer())
                     .exchange()
                     .expectStatus().isOk()
                     .expectHeader().contentType(APPLICATION_JSON_UTF8)
                     .expectBody(TppApiDTO.class)
                     .consumeWith(t -> assertThat(t.getResponseBody()).isEqualToComparingFieldByFieldRecursively(dbTpp.setCredentials(
                             new TppCredentialApiDTO().setTrustedRedirectUrls(dbTpp.getCredentials().getTrustedRedirectUrls()))))
                     .consumeWith(
                             document("tpps_findByIdWithAccreditations_forDxpTechnicalUser", pathParameters(tppIdPathDesc()),
                                      detailResponseFields(), dslContract()));
    }

    @Test
    void findByIdBasic_shouldFindOneWithAccreditationsButNoClientForBankUser() {
        TppApiDTO dbTpp = map(tppDAO.findById(TppDAO.tppIdActiveWithAccreditationsButNoClient));
        webTestClient.get()
                     .uri(TPPS + ID, TppDAO.tppIdActiveWithAccreditationsButNoClient)
                     .header(AUTHORIZATION, DRAVEN.bearer())
                     .exchange()
                     .expectStatus().isOk()
                     .expectHeader().contentType(APPLICATION_JSON_UTF8)
                     .expectBody(TppApiDTO.class)
                     .consumeWith(t -> assertThat(t.getResponseBody()).isEqualToComparingFieldByFieldRecursively(dbTpp.setCredentials(
                             new TppCredentialApiDTO().setTrustedRedirectUrls(dbTpp.getCredentials().getTrustedRedirectUrls()))))
                     .consumeWith(document("tpps_findByIdWithAccreditationsButNoClient_forBankUser", pathParameters(tppIdPathDesc()),
                                           detailResponseFields(),
                                           dslContract()));
    }

    @Test
    void findByIdBasic_shouldFindOneWithAccreditationsButNoClientForDxpTechnicalUser() {
        TppApiDTO dbTpp = map(tppDAO.findById(TppDAO.tppIdActiveWithAccreditationsButNoClient));
        webTestClient.get()
                     .uri(TPPS + ID, TppDAO.tppIdActiveWithAccreditationsButNoClient)
                     .header(AUTHORIZATION, HEIMERDINGER.bearer())
                     .exchange()
                     .expectStatus().isOk()
                     .expectHeader().contentType(APPLICATION_JSON_UTF8)
                     .expectBody(TppApiDTO.class)
                     .consumeWith(t -> assertThat(t.getResponseBody()).isEqualToComparingFieldByFieldRecursively(dbTpp.setCredentials(
                             new TppCredentialApiDTO().setTrustedRedirectUrls(dbTpp.getCredentials().getTrustedRedirectUrls()))))
                     .consumeWith(
                             document("tpps_findByIdWithAccreditationsButNoClient_forDxpTechnicalUser", pathParameters(tppIdPathDesc()),
                                      detailResponseFields(),
                                      dslContract()));
    }

    @Test
    void create_shouldCreateNewTpp() throws Exception {
        TppApiDTO newTpp = newTpp();
        webTestClient.post()
                     .uri(TPPS)
                     .header(AUTHORIZATION, DRAVEN.bearer())
                     .contentType(APPLICATION_JSON_UTF8)
                     .syncBody(mapper.writerWithView(Basic.class).writeValueAsBytes(newTpp))
                     .exchange()
                     .expectStatus().isCreated()
                     .expectHeader().contentType(APPLICATION_JSON_UTF8)
                     .expectBody(TppApiDTO.class)
                     .consumeWith(t -> {
                         TppApiDTO tpp = t.getResponseBody();
                         assertThat(tpp).isNotNull();
                         assertThat(tpp.getId()).isNotBlank();
                         assertThat(tpp).isEqualToComparingFieldByFieldRecursively(newTpp.withId(tpp.getId()));
                     })
                     .consumeWith(document("tpps_create", detailRequestFields(), detailResponseFields(), dslContract()));
    }

    @Test
    void update_shouldUpdateExistingTpp() throws Exception {
        String updatedName = "Updated name";
        TppApiDTO newTpp = map(tppDAO.findAny()).setName(updatedName);
        TppCredentialApiDTO credentials = newTpp.getCredentials();
        newTpp.setCredentials(new TppCredentialApiDTO().setTrustedRedirectUrls(credentials.getTrustedRedirectUrls()));
        webTestClient.put()
                     .uri(TPPS + ID, newTpp.getId())
                     .header(AUTHORIZATION, DRAVEN.bearer())
                     .contentType(APPLICATION_JSON_UTF8)
                     .syncBody(mapper.writerWithView(Basic.class).writeValueAsBytes(newTpp))
                     .exchange()
                     .expectStatus().isOk()
                     .expectHeader().contentType(APPLICATION_JSON_UTF8)
                     .expectBody(TppApiDTO.class)
                     .consumeWith(t -> assertThat(t.getResponseBody()).isEqualToComparingFieldByFieldRecursively(
                             newTpp.setCredentials(new TppCredentialApiDTO().setTrustedRedirectUrls(credentials.getTrustedRedirectUrls()))))
                     .consumeWith(document("tpps_update", pathParameters(tppIdPathDesc()), detailRequestFields(), detailResponseFields(),
                                           dslContract()));
    }

    @Test
    void patch_shouldPatchExistingTpp() throws Exception {
        TppApiDTO newTpp = map(new Tpp().setName("updateName").setComments("UpDateCom"));
        TppApiDTO existing = map(tppDAO.findAny());
        webTestClient.patch()
                     .uri(TPPS + ID, existing.getId())
                     .header(AUTHORIZATION, DRAVEN.bearer())
                     .contentType(APPLICATION_JSON_UTF8)
                     .syncBody(mapper.writerWithView(Basic.class).writeValueAsBytes(newTpp))
                     .exchange()
                     .expectStatus().isOk()
                     .expectHeader().contentType(APPLICATION_JSON_UTF8)
                     .expectBody(TppApiDTO.class)
                     .consumeWith(t -> {
                         TppApiDTO tpp = t.getResponseBody();
                         assertThat(tpp).isNotNull();
                         assertThat(tpp.getComments()).isEqualTo(newTpp.getComments());
                         assertThat(tpp.getName()).isEqualTo(newTpp.getName());

                     })
                     .consumeWith(document("tpps_patch", pathParameters(tppIdPathDesc()), optionalRequestFields(), detailResponseFields(),
                                           dslContract()));
    }

    @Test
    void patch_shouldUpdateAccreditations() throws Exception {
        Set<TppAccreditation> accreditations = Stream.of(AIS).collect(toSet());
        TppApiDTO newTpp = map(new Tpp().setAccreditations(accreditations));
        TppApiDTO existing = map(tppDAO.findAnyByPredicate(t -> t.getNationalAccreditationData().getAccreditations().contains(AIS)));
        existing.setAccreditations(accreditations);
        webTestClient.patch()
                     .uri(TPPS + ID, existing.getId())
                     .header(AUTHORIZATION, DRAVEN.bearer())
                     .contentType(APPLICATION_JSON_UTF8)
                     .syncBody(mapper.writerWithView(Basic.class).writeValueAsBytes(newTpp))
                     .exchange()
                     .expectStatus().isOk()
                     .expectHeader().contentType(APPLICATION_JSON_UTF8)
                     .expectBody(TppApiDTO.class)
                     .consumeWith(t -> assertThat(t.getResponseBody()).isEqualToComparingFieldByFieldRecursively(existing.setCredentials(
                             new TppCredentialApiDTO().setTrustedRedirectUrls(existing.getCredentials().getTrustedRedirectUrls()))));
        //TODO Shouldn't there be some contract for this ?
    }

    @Test
    void findState_shouldFindState() {
        Tpp anyTpp = tppDAO.findAny();
        TppState state = anyTpp.getState();
        webTestClient.get()
                     .uri(TPPS + ID + "/state", anyTpp.getId())
                     .header(AUTHORIZATION, DRAVEN.bearer())
                     .exchange()
                     .expectStatus().isOk()
                     .expectHeader().contentType(APPLICATION_JSON_UTF8)
                     .expectBody(TppState.class)
                     .consumeWith(t -> assertThat(t.getResponseBody()).isEqualToComparingFieldByField(state))
                     .consumeWith(document("tpps_findState", pathParameters(tppIdPathDesc()), responseFields(stateDesc()), dslContract()));
    }

    @Test
    void findByName_shouldFindOneTpp() {
        webTestClient.get()
                     .uri(TPPS + "?name=Solidarity&range=0-49")
                     .header(AUTHORIZATION, DRAVEN.bearer())
                     .exchange()
                     .expectStatus().isEqualTo(PARTIAL_CONTENT)
                     .expectHeader().contentType(APPLICATION_JSON_UTF8)
                     .expectHeader().valueEquals(ACCEPT_RANGES, "tpps")
                     .expectHeader().valueEquals(CONTENT_RANGE, "tpps 0-49/1")
                     .expectBodyList(TppApiDTO.class)
                     .hasSize(1)
                     .consumeWith(document("tpps_findByName",
                                           requestParameters(tppNamePathDesc(), rangeDesc()),
                                           tppsDesc(),
                                           paginationResponseHeaderDesc(),
                                           dslContract()));
    }

    @Test
    void findByName_shouldFindMultiple() {
        String bankName = "ABC bank";
        int bankNb = tppDAO.findByName(bankName).size();
        webTestClient.get()
                     .uri(TPPS + "?range=0-49&name=" + bankName)
                     .header(AUTHORIZATION, DRAVEN.bearer())
                     .exchange()
                     .expectStatus().isEqualTo(PARTIAL_CONTENT)
                     .expectHeader().contentType(APPLICATION_JSON_UTF8)
                     .expectHeader().valueEquals(ACCEPT_RANGES, "tpps")
                     .expectHeader().valueEquals(CONTENT_RANGE, "tpps 0-49/" + bankNb)
                     .expectBodyList(TppApiDTO.class)
                     .hasSize(bankNb);
    }

    @Test
    void findByName_shouldBeEmpty() {
        webTestClient.get()
                     .uri(TPPS + "?range=0-49&name=Unknown")
                     .header(AUTHORIZATION, DRAVEN.bearer())
                     .exchange()
                     .expectStatus().isEqualTo(PARTIAL_CONTENT)
                     .expectHeader().contentType(APPLICATION_JSON_UTF8)
                     .expectHeader().valueEquals(ACCEPT_RANGES, "tpps")
                     .expectHeader().valueEquals(CONTENT_RANGE, "tpps 0-49/0")
                     .expectBodyList(TppApiDTO.class)
                     .hasSize(0);
    }

    @Test
    void findByClientId_shouldFindOne() {
        TppApiDTO tpp = map(tppDAO.findByName("Sensodyne").get(0));
        String clientId = tpp.getCredentials().getClientId();
        webTestClient.get()
                     .uri(TPPS + "?clientId=" + clientId)
                     .header(AUTHORIZATION, HEIMERDINGER.bearer())
                     .exchange()
                     .expectStatus().isOk()
                     .expectHeader().contentType(APPLICATION_JSON_UTF8)
                     .expectBody(TppApiDTO.class)
                     .consumeWith(t -> assertThat(t.getResponseBody()).isEqualTo(tpp))
                     .consumeWith(document("tpps_findByClientIdSensodyne", requestParameters(clientIdParamDesc()),
                                           detailResponseFieldsWithCredentials(),
                                           dslContract()));
    }

    @Test
    void findByClientId_shouldFindBlockedOne() {
        TppApiDTO tpp = map(tppDAO.findByName("Oral B").get(0));
        String clientId = tpp.getCredentials().getClientId();
        webTestClient.get()
                     .uri(TPPS + "?clientId=" + clientId)
                     .header(AUTHORIZATION, HEIMERDINGER.bearer())
                     .exchange()
                     .expectStatus().isOk()
                     .expectHeader().contentType(APPLICATION_JSON_UTF8)
                     .expectBody(TppApiDTO.class)
                     .consumeWith(t -> assertThat(t.getResponseBody()).isEqualTo(tpp))
                     .consumeWith(document("tpps_findByClientIdOralB", requestParameters(clientIdParamDesc()),
                                           detailResponseFieldsWithCredentials(),
                                           dslContract()));
    }

    @Test
    void findByClientId_shouldNotFindOne() {
        webTestClient.get()
                     .uri(TPPS + "?clientId=unknown")
                     .header(AUTHORIZATION, HEIMERDINGER.bearer())
                     .exchange().expectStatus().isNoContent()
                     .expectBody(TppApiDTO.class)
                     .consumeWith(t -> assertThat(t.getResponseBody()).isNull())
                     .consumeWith(document("tpps_findByClientIdNotFound", requestParameters(clientIdParamDesc()), dslContract()));
    }

    @Test
    void findByIdBasic_shouldFailCuzIdNotFound() {
        webTestClient.get()
                     .uri(TPPS + ID, new ObjectId().toString())
                     .header(AUTHORIZATION, DRAVEN.bearer())
                     .exchange()
                     .expectStatus().isNotFound()
                     .expectHeader().contentType(APPLICATION_JSON_UTF8)
                     .expectBody(SbsExceptionDTO.class)
                     .consumeWith(e -> verifySbsExceptionDTO(e, RESOURCE_NOT_FOUND))
                     .consumeWith(document("tpps_findByIdBasicNotFound", errorFields(), dslContract()));
    }

    @Test
    void findByOrganizationIdentifier_shouldFindOneForDxpTechnicalUser() {
        TppApiDTO tpp = map(tppDAO.findByName("Sensodyne").get(0));
        String organizationIdentifier = tpp.getNationalAccreditationData().getOrganizationIdentifier();
        webTestClient.get()
                     .uri(TPPS + "?organizationIdentifier=" + organizationIdentifier)
                     .header(AUTHORIZATION, HEIMERDINGER.bearer())
                     .exchange()
                     .expectStatus().isOk()
                     .expectHeader().contentType(APPLICATION_JSON_UTF8)
                     .expectBody(TppApiDTO.class)
                     .consumeWith(t -> assertThat(t.getResponseBody()).isEqualTo(tpp))
                     .consumeWith(document("tpps_findByOrganizationIdentifierSensodyne_forDxpTechnicalUser",
                                           requestParameters(organizationIdentifierParamDesc()),
                                           detailResponseFields(),
                                           dslContract()));
    }

    @Test
    void findByOrganizationIdentifier_shouldFindOneForBankUser() {
        TppApiDTO tpp = map(tppDAO.findByName("Sensodyne").get(0));
        String organizationIdentifier = tpp.getNationalAccreditationData().getOrganizationIdentifier();
        webTestClient.get()
                     .uri(TPPS + "?organizationIdentifier=" + organizationIdentifier)
                     .header(AUTHORIZATION, DRAVEN.bearer())
                     .exchange()
                     .expectStatus().isOk()
                     .expectHeader().contentType(APPLICATION_JSON_UTF8)
                     .expectBody(TppApiDTO.class)
                     .consumeWith(t -> assertThat(t.getResponseBody()).isEqualTo(tpp))
                     .consumeWith(document("tpps_findByOrganizationIdentifierSensodyne_forBankUser",
                                           requestParameters(organizationIdentifierParamDesc()),
                                           detailResponseFields(),
                                           dslContract()));
    }

    @Test
    void create_shouldFailCuzMandatoryFieldMissing() {
        TppApiDTO newTpp = newTpp().setNationalAccreditationData(new TppNationalAccreditationApiDTO());
        webTestClient.post()
                     .uri(TPPS)
                     .header(AUTHORIZATION, DRAVEN.bearer())
                     .syncBody(newTpp)
                     .exchange()
                     .expectStatus().isBadRequest()
                     .expectHeader().contentType(APPLICATION_JSON_UTF8)
                     .expectBody(SbsExceptionDTO.class)
                     .consumeWith(e -> verifySbsExceptionDTO(e, INVALID_CONTENT))
                     .consumeWith(document("tpps_createMissingField", errorFields(), dslContract()));
    }

    @Test
    void create_shouldFailCuzOrganizationIdentifierAlreadyExists() {
        String nationalId = tppDAO.findAny().getNationalAccreditationData().getOrganizationIdentifier();
        TppApiDTO newTpp = newTpp();
        newTpp.getNationalAccreditationData().setOrganizationIdentifier(nationalId);
        webTestClient.post()
                     .uri(TPPS)
                     .header(AUTHORIZATION, DRAVEN.bearer())
                     .syncBody(newTpp)
                     .exchange()
                     .expectStatus().isEqualTo(HttpStatus.CONFLICT)
                     .expectHeader().contentType(APPLICATION_JSON_UTF8)
                     .expectBody(SbsExceptionDTO.class)
                     .consumeWith(e -> verifySbsExceptionDTO(e, RESOURCE_ALREADY_EXIST))
                     .consumeWith(document("tpps_createConflict", errorFields(), dslContract()));
    }

    @Test
    void update_shouldNotUpdateTppCuzIdNotFound() {
        TppApiDTO newTpp = map(tppDAO.findAny());
        newTpp.withId(new ObjectId().toString()).setCredentials(null);
        webTestClient.put()
                     .uri(TPPS + ID, newTpp.getId())
                     .header(AUTHORIZATION, DRAVEN.bearer())
                     .syncBody(newTpp)
                     .exchange()
                     .expectStatus().isNotFound()
                     .expectHeader().contentType(APPLICATION_JSON_UTF8)
                     .expectBody(SbsExceptionDTO.class)
                     .consumeWith(e -> verifySbsExceptionDTO(e, RESOURCE_NOT_FOUND))
                     .consumeWith(document("tpps_updateIdNotFound", errorFields(), dslContract()));
    }

    @Test
    void patch_shouldNotPatchTppCuzIdNotFound() {
        webTestClient.patch()
                     .uri(TPPS + ID, new ObjectId().toString())
                     .header(AUTHORIZATION, DRAVEN.bearer())
                     .syncBody(new TppApiDTO())
                     .exchange()
                     .expectStatus().isNotFound()
                     .expectHeader().contentType(APPLICATION_JSON_UTF8)
                     .expectBody(SbsExceptionDTO.class)
                     .consumeWith(e -> verifySbsExceptionDTO(e, RESOURCE_NOT_FOUND))
                     .consumeWith(document("tpps_patchIdNotFound", errorFields(), dslContract()));
    }

    @Test
    void findState_shouldFailCuzIdNotFound() {
        webTestClient.get()
                     .uri(TPPS + ID + "/state", new ObjectId().toString())
                     .header(AUTHORIZATION, DRAVEN.bearer())
                     .exchange()
                     .expectStatus().isNotFound()
                     .expectHeader().contentType(APPLICATION_JSON_UTF8)
                     .expectBody(SbsExceptionDTO.class)
                     .consumeWith(e -> verifySbsExceptionDTO(e, RESOURCE_NOT_FOUND))
                     .consumeWith(document("tpps_findStateIdNotFound", errorFields(), dslContract()));
    }

    @Test
    void update_withTppIdInBodyDifferent_shouldFail() throws Exception {
        String updatedName = "Updated name";
        TppApiDTO newTpp = map(tppDAO.findAny()).setName(updatedName).setCredentials(null);
        String id = newTpp.getId();
        newTpp.setId(new ObjectId().toString());
        webTestClient.put()
                     .uri(TPPS + ID, id)
                     .header(AUTHORIZATION, DRAVEN.bearer())
                     .contentType(APPLICATION_JSON_UTF8)
                     .syncBody(mapper.writerWithView(Basic.class).writeValueAsBytes(newTpp))
                     .exchange()
                     .expectBody(SbsExceptionDTO.class)
                     .consumeWith(e -> verifySbsExceptionDTO(e, INVALID_CONTENT))
                     .consumeWith(document("tpps_update_withTppIdInBodyDifferent_shouldFail", errorFields(), dslContract()));
    }

    @Test
    void patch_withTppIdInBodyDifferent_shouldFail() throws Exception {
        String updatedName = "Updated name";
        TppApiDTO newTpp = new TppApiDTO().setName(updatedName);
        String id = tppDAO.findAny().getId().toString();
        newTpp.setId(new ObjectId().toString());
        webTestClient.patch()
                     .uri(TPPS + ID, id)
                     .header(AUTHORIZATION, DRAVEN.bearer())
                     .contentType(APPLICATION_JSON_UTF8)
                     .syncBody(mapper.writerWithView(Basic.class).writeValueAsBytes(newTpp))
                     .exchange()
                     .expectBody(SbsExceptionDTO.class)
                     .consumeWith(e -> verifySbsExceptionDTO(e, INVALID_CONTENT))
                     .consumeWith(document("tpps_update_withTppIdInBodyDifferent_shouldFail", errorFields(), dslContract()));
    }

    @Test
    void findByIds_shouldFindSomeTpps() {
        List<String> allIds = tppDAO.findAll().stream().map(Tpp::getId).map(ObjectId::toString).collect(toList());
        String idsRequestParam = "?ids=" + join(",", allIds);
        webTestClient.get()
                     .uri(TPPS + idsRequestParam)
                     .header(AUTHORIZATION, ASHE.bearer())
                     .exchange()
                     .expectStatus().isOk()
                     .expectBodyList(TppApiDTO.class)
                     .consumeWith(r -> {
                         List<TppApiDTO> tpps = r.getResponseBody();
                         assertThat(tpps).hasSize(tppDAO.count());
                         assertThat(tpps).extracting(TppApiDTO::getId).containsExactlyInAnyOrderElementsOf(allIds);
                     })
                     .consumeWith(document("tpp_findByIds",
                                           requestParameters(tppIdsParamDesc()),
                                           tppsContactAndNameDesc(),
                                           dslContract()));
    }

    @Test
    void findByIds_shouldFindOneTpp() {
        String id = tppDAO.findAny().getId().toString();
        String idsRequestParam = "?ids=" + id;
        webTestClient.get()
                     .uri(TPPS + idsRequestParam)
                     .header(AUTHORIZATION, ASHE.bearer())
                     .exchange()
                     .expectStatus().isOk()
                     .expectBodyList(TppApiDTO.class)
                     .consumeWith(r -> {
                         List<TppApiDTO> tpps = r.getResponseBody();
                         assertThat(tpps).hasSize(1);
                         assertThat(tpps).extracting(TppApiDTO::getId).containsExactly(id);
                     })
                     .consumeWith(document("tpp_findByIds_oneTpp",
                                           requestParameters(tppIdsParamDesc()),
                                           tppsContactAndNameDesc(),
                                           dslContract()));
    }

    @Test
    void findByIds_shouldNotFindAnyTpp() {
        List<String> randomsIds = listOf(ObjectId.get().toString(), ObjectId.get().toString(), ObjectId.get().toString());
        String idsRequestParam = "?ids=" + join(",", randomsIds);
        webTestClient.get()
                     .uri(TPPS + idsRequestParam)
                     .header(AUTHORIZATION, ASHE.bearer())
                     .exchange()
                     .expectStatus().isOk()
                     .expectBodyList(TppApiDTO.class)
                     .consumeWith(r -> {
                         List<TppApiDTO> tpps = r.getResponseBody();
                         assertThat(tpps).isEmpty();
                     })
                     .consumeWith(document("tpp_findByIds_empty",
                                           requestParameters(tppIdsParamDesc()),
                                           dslContract()));
    }

    @Test
    void findByIds_shouldFailBecauseBadObjectId() {
        List<String> randomsIds = listOf(ObjectId.get().toString(), "BAD OBJECT ID");
        String idsRequestParam = "?ids=" + join(",", randomsIds);
        webTestClient.get()
                     .uri(TPPS + idsRequestParam)
                     .header(AUTHORIZATION, ASHE.bearer())
                     .exchange()
                     .expectStatus().isBadRequest()
                     .expectBody(SbsExceptionDTO.class)
                     .consumeWith(e -> verifySbsExceptionDTO(e, INVALID_CONTENT))
                     .consumeWith(document("tpps_findByIds_shouldFailBecauseBadObjectId", errorFields(), dslContract()));
    }

    @Test
    void findByIds_shouldFailBecauseBadSecurityUser() {
        List<String> allIds = tppDAO.findAll().stream().map(Tpp::getId).map(ObjectId::toString).collect(toList());
        String idsRequestParam = "?ids=" + join(",", allIds);
        webTestClient.get()
                     .uri(TPPS + idsRequestParam)
                     .header(AUTHORIZATION, LUX.bearer())
                     .exchange()
                     .expectStatus().isForbidden()
                     .expectBody(SbsExceptionDTO.class)
                     .consumeWith(e -> verifySbsExceptionDTO(e, INSUFFICIENT_PERMISSION))
                     .consumeWith(document("tpps_findByIds_shouldFailBecauseBadSecurityUser", errorFields(), dslContract()));
    }

    private TppApiDTO newTpp() {
        return new TppApiDTO()
                .setName("Defense of the ancients bank")
                .setComments("Not a bank for casuals")
                .setContact(new TppContactApiDTO().setCity("Seattle").setEmail("dota'‘®mail.com").setPhoneNumber("1337"))
                .setState(new TppStateApiDTO().setStatus(ACTIVE).setMonitored(true))
                .setCredentials(new TppCredentialApiDTO().setTrustedRedirectUrls(SetsKt.setOf("api.redirectUrls.com")))
                .setAccreditations(Stream.of(AIS, PIS).collect(toSet()))
                .setWebSite("dota.com")
                .setNationalAccreditationData(new TppNationalAccreditationApiDTO().setOrganizationIdentifier("PSDES-BDE-3DFD21")
                                                                                  .setCompetentAuthorityName("Name")
                                                                                  .setCompetentAuthorityIdentifier("BDE")
                                                                                  .setCompetentAuthorityCountryCode("ES")
                                                                                  .setAuthorizationNumber("3DFD21")
                                                                                  .setCompetentAuthorityStatus(AUTHORISED)
                                                                                  .setAccreditations(SetsKt.setOf(AIS, CIS, PIS))
                                                                                  .setRegistrationDate(LocalDate.of(2018, 2, 28)))
                .setContact(new TppContactApiDTO().setCity("Seattle")
                                                  .setEmail("dota'‘®mail.com")
                                                  .setPhoneNumber("1337")
                                                  .setAddressLines(Arrays.asList("random line", "road 10"))
                                                  .setCountry("USA").setPostCode("4321"))
                .setPhysicalContacts(Arrays.asList(new TppPhysicalContactApiDTO()
                                                           .setContactType(TppContactType.BUSINESS_CONTACT)
                                                           .setEmail("john.doe@dota.com")
                                                           .setName("Doe")
                                                           .setFirstName("John")
                                                           .setJobTitle("CEO")
                                                           .setPhoneNumber("0123456789"),
                                                   new TppPhysicalContactApiDTO()
                                                           .setContactType(TppContactType.TECHNICAL_CONTACT)
                                                           .setEmail("jim.forest@dota.com")
                                                           .setName("forest")
                                                           .setFirstName("Jim")
                                                           .setJobTitle("CTO")
                                                           .setPhoneNumber("0123456789")));
    }
}