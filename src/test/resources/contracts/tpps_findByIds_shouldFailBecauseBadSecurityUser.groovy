import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        method 'GET'
        url '/api/tpps?ids=a00000000000000000000000,a00000000000000000000001'
        headers {
            header(authorization(), $(p('Bearer lux'), c('Bearer TPP')))
        }
    }
    response {
        status 403
        body('''
        {
  "returnCode" : "ERROR",
  "traceId" : "da01ecd55ba54e64",
  "ticketId" : "1i9azn",
  "feedbacks" : [ {
    "code" : "INSUFFICIENT_PERMISSION",
    "label" : "Client does not have sufficient permission. This can happen because the user doesn't match the type of users supported by this API.",
    "severity" : "ERROR",
    "type" : "SEC",
    "spanId" : "da01ecd55ba54e64",
    "origin" : "service-tpp-management",
    "source" : null,
    "parameters" : [ ],
    "internal" : {
      "message" : "Wrong user type",
      "parameters" : [ ]
    }
  } ]
}
        ''')
        bodyMatchers {
            jsonPath('$.ticketId', byRegex(nonEmpty()))
            jsonPath('$.feedbacks[*].spanId', byRegex(nonEmpty()))
            jsonPath('$.traceId', byRegex(nonEmpty()))
            jsonPath('$.feedbacks[*].internal.message', byRegex(nonEmpty()))
        }
        headers {
            header('''Content-Type''', '''application/json;charset=UTF-8''')
        }
    }
}