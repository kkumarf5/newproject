import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        method 'GET'
        url '/api/tpps/5b22859f26f9690d8bbd1761'
        headers {
            header(authorization(), $(
                    p('Bearer draven'),
                    c('Bearer BANK_USER'))
            )
        }
    }
    response {
        status 200
        body('''
        {
  "id" : "5b22859f26f9690d8bbd1761",
  "name" : "ID_TPP_ACTIVE_WITHOUT_ACCREDITATION",
  "comments" : "Adding ID_TPP for user-auth contract compliance",
  "webSite" : "dotamail.com",
  "state" : {
    "status" : "ACTIVE",
    "monitored" : true
  },
  "contact" : {
    "email" : "natures.prophet@dotamail.com",
    "phoneNumber" : "33601010101",
    "city" : "Saint-Denis",
    "addressLines" : [ "random line", "road 10" ],
    "postCode" : "4321",
    "country" : "France"
  },
  "physicalContacts" : [ {
    "contactType" : "BUSINESS_CONTACT",
    "name" : "Doe",
    "firstName" : "John",
    "jobTitle" : "CEO",
    "email" : "john.doe@dota.com",
    "phoneNumber" : "0123456789"
  }, {
    "contactType" : "TECHNICAL_CONTACT",
    "name" : "forest",
    "firstName" : "Jim",
    "jobTitle" : "CTO",
    "email" : "jim.forest@dota.com",
    "phoneNumber" : "0123456789"
  } ],
  "accreditations" : [ "CIS" ],
  "nationalAccreditationData" : {
    "organizationIdentifier" : "PSDFR-BDE-ID_TPP_ACTIVE_WITHOUT_ACCREDITATION",
    "competentAuthorityIdentifier" : "BDE",
    "competentAuthorityName" : "French registry",
    "competentAuthorityCountryCode" : "FR",
    "authorizationNumber" : "ID_TPP_ACTIVE_WITHOUT_ACCREDITATION",
    "competentAuthorityStatus" : "AUTHORISED",
    "registrationDate" : "2018-02-28",
    "accreditations" : [ "AIS", "CIS", "PIS" ]
  },
  "credentials" : {
    "trustedRedirectUrls" : [ "api.redirectUrl.com" ]
  }
}
        ''')
        headers {
            header('''Content-Type''', '''application/json;charset=UTF-8''')
        }
    }
}