import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        method 'GET'
        url '/api/tpps/a00000000000000000000001'
        headers {
            header(authorization(), $(
                    p('Bearer draven'),
                    c('Bearer BANK_USER'))
            )
        }
    }
    response {
        status 200
        body('''
        {
          "id" : "a00000000000000000000001",
          "name" : "Activia",
          "comments" : "Activia is a TPP active without accreditations",
          "contact" : {
            "email" : "activia@mail.com",
            "phoneNumber" : "0123456789",
            "city" : "Dunkerque",
            "addressLines" : [ "1 rue de la Pie.", "2 boulevard de la legereté." ],
            "postCode" : "4321",
            "country" : "France"
          },
          "state" : {
            "status" : "ACTIVE",
            "monitored" : true
          },
          "accreditations" : [ ],
          "webSite" : "activia.com",
          "credentials" : {
            "trustedRedirectUrls":[ "api.redirectUrl.com" ]
          },
          "nationalAccreditationData" : {
            "organizationIdentifier" : "PSDFR-BDE-ACTIVIA",
            "competentAuthorityCountryCode" : "FR",
            "competentAuthorityIdentifier" : "BDE",
            "competentAuthorityName" : "French registry",
            "authorizationNumber" : "ACTIVIA",
            "competentAuthorityStatus": "AUTHORISED",
            "registrationDate" : "2018-02-28",
            "accreditations" : [ ]
          },
          "physicalContacts" : [ {
            "contactType" : "BUSINESS_CONTACT",
            "name" : "Charolaise",
            "firstName" : "Othman",
            "jobTitle" : "CEO",
            "email" : "othman.charolaise@activia.com",
            "phoneNumber" : "0123456789"
          }, {
            "contactType" : "TECHNICAL_CONTACT",
            "name" : "Angus",
            "firstName" : "Alex",
            "jobTitle" : "CTO",
            "email" : "angus.alex@activia.com",
            "phoneNumber" : "0123456789"
          } ]
        }
        ''')
        headers {
            header('''Content-Type''', '''application/json;charset=UTF-8''')
        }
    }
}