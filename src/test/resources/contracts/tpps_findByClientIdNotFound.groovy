import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        method 'GET'
        url '/api/tpps?clientId=unknown'
        headers {
            header(authorization(), $(
                    p('Bearer heimerdinger'),
                    c('Bearer DXP_TECHNICAL'))
            )
        }
    }
    response {
        status 204
    }
}