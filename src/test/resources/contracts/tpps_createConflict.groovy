import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        method 'POST'
        url '/api/tpps'
        headers {
            header(authorization(), $(
                    p('Bearer draven'),
                    c('Bearer BANK_USER'))
            )
            header(contentType(), applicationJsonUtf8())
        }
        body('''
            {
              "name" : "Defense of the ancients bank",
              "comments" : "Not a bank for casuals",
              "contact" : {
                "email" : "dota'‘®mail.com",
                "phoneNumber" : "1337",
                "city" : "Seattle",
                "addressLines" : [ "random line", "road 10" ],
                "postCode" : "4321",
                "country" : "USA"
              },
              "state" : {
                "status" : "ACTIVE",
                "monitored" : true
              },
              "accreditations" : [ "PIS", "AIS" ],
              "webSite" : "dota.com",
              "nationalAccreditationData" : {
                "organizationIdentifier" : "PSDUS-BDE-DOTA",
                "competentAuthorityCountryCode" : "US",
                "competentAuthorityIdentifier" : "BDE",
                "competentAuthorityName" : "USA registry",
                "authorizationNumber" : "DOTA",
                "competentAuthorityStatus": "AUTHORISED",
                "registrationDate" : "2018-03-29",
                "accreditations" : [ "PIS", "AIS" ]
              },
              "physicalContacts" : [ {
                "contactType" : "BUSINESS_CONTACT",
                "name" : "Doe",
                "firstName" : "John",
                "jobTitle" : "CEO",
                "email" : "john.doe@dota.com",
                "phoneNumber" : "0123456789"
              }, {
                "contactType" : "TECHNICAL_CONTACT",
                "name" : "forest",
                "firstName" : "Jim",
                "jobTitle" : "CTO",
                "email" : "jim.forest@dota.com",
                "phoneNumber" : "0123456789"
              } ]
            }
        ''')
    }
    response {
        status 409
        body('''
        {
  "returnCode" : "ERROR",
  "traceId" : "432237a4ac0add39",
  "ticketId" : "en5t0n",
  "feedbacks" : [ {
    "code" : "RESOURCE_ALREADY_EXIST",
    "label" : "The specified resource already exists",
    "severity" : "ERROR",
    "type" : "BUS",
    "spanId" : "432237a4ac0add39",
    "origin" : "service-tpp-management",
    "source" : null,
    "parameters" : [ ],
    "internal" : {
      "message" : "NationalId ID_TPP_ORAL_B is already taken by com.soprabanking.dxp.tpp.model.entity.Tpp@5ca9b20f[id=a00000000000000000000000,name=Oral B,state=com.soprabanking.dxp.tpp.model.entity.TppState@1435bd04[status=BLOCKED,monitored=true],nationalAccreditationData=com.soprabanking.dxp.tpp.model.entity.TppNationalAccreditation@2b41b0c7[nationalId=ID_TPP_ORAL_B,competentAuthorityName=French registry,competentAuthorityCountry=France]]",
      "parameters" : [ ]
    }
  } ]
}
        ''')
        bodyMatchers {
            jsonPath('$.ticketId', byRegex(nonEmpty()))
            jsonPath('$.feedbacks[*].spanId', byRegex(nonEmpty()))
            jsonPath('$.traceId', byRegex(nonEmpty()))
            jsonPath('$.feedbacks[*].internal.message', byRegex(nonEmpty()))
        }
        headers {
            header('''Content-Type''', '''application/json;charset=UTF-8''')
            header('''Cache-Control''', '''no-cache, no-store, max-age=0, must-revalidate''')
            header('''Pragma''', '''no-cache''')
            header('''Expires''', '''0''')
            header('''X-Content-Type-Options''', '''nosniff''')
            header('''X-Frame-Options''', '''DENY''')
            header('''X-XSS-Protection''', '''1 ; mode=block''')
        }
    }
}