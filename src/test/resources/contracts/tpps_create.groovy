import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        method 'POST'
        url '/api/tpps'
        headers {
            header(authorization(), $(p('Bearer draven'), c('Bearer BANK_USER')))
            header(contentType(), applicationJsonUtf8())
        }
        body('''
            {
              "name" : "Defense of the ancients bank",
              "comments" : "Not a bank for casuals",
              "contact" : {
                "email" : "dota'‘®mail.com",
                "phoneNumber" : "1337",
                "city" : "Seattle",
                "addressLines" : [ "random line", "road 10" ],
                "postCode" : "4321",
                "country" : "USA"
              },
              "state" : {
                "status" : "ACTIVE",
                "monitored" : true
              },
              "accreditations" : [ "PIS", "AIS" ],
              "webSite" : "dota.com",
              "nationalAccreditationData" : {
                "organizationIdentifier" : "PSDUS-BDE-DOTA123",
                "competentAuthorityCountryCode" : "US",
                "competentAuthorityIdentifier" : "BDE",
                "competentAuthorityName" : "USA registry",
                "authorizationNumber" : "DOTA123",
                "competentAuthorityStatus": "AUTHORISED",
                "registrationDate" : "2018-02-28",
                "accreditations" : [ "PIS", "AIS" ]
              },
              "physicalContacts" : [ {
                "contactType" : "BUSINESS_CONTACT",
                "name" : "Doe",
                "firstName" : "John",
                "jobTitle" : "CEO",
                "email" : "john.doe@dota.com",
                "phoneNumber" : "0123456789"
              }, {
                "contactType" : "TECHNICAL_CONTACT",
                "name" : "forest",
                "firstName" : "Jim",
                "jobTitle" : "CTO",
                "email" : "jim.forest@dota.com",
                "phoneNumber" : "0123456789"
              } ]
            }
        ''')
    }
    response {
        status 201
        body('''
        {
          "id": "5c0646e38f32ab0945a3e00f",
          "name" : "Defense of the ancients bank",
          "comments" : "Not a bank for casuals",
          "contact" : {
            "email" : "dota'‘®mail.com",
            "phoneNumber" : "1337",
            "city" : "Seattle",
            "addressLines" : [ "random line", "road 10" ],
            "postCode" : "4321",
            "country" : "USA"
          },
          "state" : {
            "status" : "ACTIVE",
            "monitored" : true
          },
          "accreditations" : [ "PIS", "AIS" ],
          "webSite" : "dota.com",
          "nationalAccreditationData" : {
            "organizationIdentifier" : "PSDUS-BDE-DOTA123",
            "competentAuthorityCountryCode" : "US",
            "competentAuthorityIdentifier" : "BDE",
            "competentAuthorityName" : "USA registry",
            "authorizationNumber" : "DOTA123",
            "competentAuthorityStatus": "AUTHORISED",
            "registrationDate" : "2018-02-28",
            "accreditations" : [ "PIS", "AIS" ]
          },
          "physicalContacts" : [ {
            "contactType" : "BUSINESS_CONTACT",
            "name" : "Doe",
            "firstName" : "John",
            "jobTitle" : "CEO",
            "email" : "john.doe@dota.com",
            "phoneNumber" : "0123456789"
          }, {
            "contactType" : "TECHNICAL_CONTACT",
            "name" : "forest",
            "firstName" : "Jim",
            "jobTitle" : "CTO",
            "email" : "jim.forest@dota.com",
            "phoneNumber" : "0123456789"
          } ],
          "credentials": {
            "trustedRedirectUrls":[]
          }
        }
        ''')
        headers {
            header('''Content-Type''', '''application/json;charset=UTF-8''')
        }
        bodyMatchers {
            jsonPath('$.id', byRegex("^[0-9a-f]{24}\$"))
        }
    }
}