import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        method 'PATCH'
        url '/api/tpps/5b47500c8933d317d6ed9c2d'
        headers {
            header(authorization(), $(p('Bearer draven'), c('Bearer BANK_USER')))
            header(contentType(), applicationJsonUtf8())
        }
        body(
                accreditations: [],
                webSite: null,
                nationalAccreditationData: null,
                physicalContacts: []
        )
    }
    response {
        status 404
        body('''
        {
  "returnCode" : "ERROR",
  "traceId" : "bfc65ea5aec11994",
  "ticketId" : "3i7ukk",
  "feedbacks" : [ {
    "code" : "RESOURCE_NOT_FOUND",
    "label" : "The specified resource does not exist",
    "severity" : "ERROR",
    "type" : "BUS",
    "spanId" : "bfc65ea5aec11994",
    "origin" : "service-tpp-management",
    "source" : null,
    "parameters" : [ ],
    "internal" : {
      "message" : "Cannot find tpp with id 5bd9ca86909ea63fee0437da",
      "parameters" : [ ]
    }
  } ]
}
        ''')
        bodyMatchers {
            jsonPath('$.ticketId', byRegex(nonEmpty()))
            jsonPath('$.feedbacks[*].spanId', byRegex(nonEmpty()))
            jsonPath('$.traceId', byRegex(nonEmpty()))
            jsonPath('$.feedbacks[*].internal.message', byRegex(nonEmpty()))
        }
        headers {
            header('''Content-Type''', '''application/json;charset=UTF-8''')
            header('''Cache-Control''', '''no-cache, no-store, max-age=0, must-revalidate''')
            header('''Pragma''', '''no-cache''')
            header('''Expires''', '''0''')
            header('''X-Content-Type-Options''', '''nosniff''')
            header('''X-Frame-Options''', '''DENY''')
            header('''X-XSS-Protection''', '''1 ; mode=block''')
        }
    }
}