import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        method 'GET'
        url '/api/tpps?name=Solidarity'
        headers {
            header(authorization(), $(
                    p('Bearer draven'),
                    c('Bearer BANK_USER'))
            )
        }
    }
    response {
        status 206
        body('''
        [ {
          "id" : "5b47500c8933d317d6ed9c0f",
          "name" : "Solidarity bank",
          "comments" : "Strange bank",
          "contact" : {
            "email" : "solidarity@mail.com",
            "phoneNumber" : "0123456789",
            "city" : "Toulouse",
            "addressLines" : [ "random line", "road 10" ],
            "postCode" : "4321",
            "country" : "France"
          },
          "state" : {
            "status" : "BLOCKED",
            "monitored" : true
          },
          "accreditations" : [ "PIS", "AIS" ],
          "webSite" : "solidarity.com",
          "credentials" : {
            "trustedRedirectUrls":[ "api.redirectUrl.com" ]
          },
          "nationalAccreditationData" : {
            "organizationIdentifier" : "PSDFR-BDE-SOLIDARITYBANK",
            "competentAuthorityCountryCode" : "FR",
            "competentAuthorityIdentifier" : "BDE",
            "competentAuthorityName" : "French registry",
            "authorizationNumber" : "SOLIDARITYBANK",
            "competentAuthorityStatus": "AUTHORISED",
            "registrationDate" : "2018-02-28",
            "accreditations" : [ "PIS", "AIS" ]
          },
          "physicalContacts" : [ {
            "contactType" : "BUSINESS_CONTACT",
            "name" : "Doe",
            "firstName" : "John",
            "jobTitle" : "CEO",
            "email" : "john.doe@solidarity.com",
            "phoneNumber" : "0123456789"
          }, {
            "contactType" : "TECHNICAL_CONTACT",
            "name" : "forest",
            "firstName" : "Jim",
            "jobTitle" : "CTO",
            "email" : "jim.forest@solidarity.com",
            "phoneNumber" : "0123456789"
          } ]
        } ]
        ''')
        headers {
            header('''Content-Type''', '''application/json;charset=UTF-8''')
        }
        bodyMatchers { jsonPath('$.[*].id', byRegex("^[0-9a-f]{24}\$")) }
    }
}