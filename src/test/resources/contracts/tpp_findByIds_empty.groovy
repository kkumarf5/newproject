import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        method 'GET'
        url '/api/tpps?ids=a99999999999999999999999'
        headers {
            header(authorization(), $(p('Bearer ashe'), c('Bearer PSU_BY_BANK')))
        }
    }
    response {
        status 200
        body('''
        [ ]
        ''')
        headers {
            header('''Content-Type''', '''application/json;charset=UTF-8''')
        }
    }
}