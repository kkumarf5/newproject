import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        method 'GET'
        url '/api/tpps?organizationIdentifier=PSDFR-BDE-SENSODYNE'
        headers {
            header(authorization(), $(
                    p('Bearer draven'),
                    c('Bearer BANK_USER'))
            )
        }
    }
    response {
        status 200
        body('''
        {
          "id" : "a00000000000000000000002",
          "name" : "Sensodyne",
          "comments" : "Tpp active with all accreditations",
          "webSite" : "sensodyne.com",
          "state" : {
            "status" : "ACTIVE",
            "monitored" : true
          },
          "contact" : {
            "email" : "sensodyne@mail.com",
            "phoneNumber" : "0123456789",
            "city" : "Nancy",
            "addressLines" : [ "24 boulevard de la Couronne", "12 rue de l'email" ],
            "postCode" : "4321",
            "country" : "France"
          },
          "physicalContacts" : [ {
            "contactType" : "BUSINESS_CONTACT",
            "name" : "Mabel",
            "firstName" : "Dentition",
            "jobTitle" : "CEO",
            "email" : "mabel.dentition@sensodyne.com",
            "phoneNumber" : "0123456789"
          }, {
            "contactType" : "TECHNICAL_CONTACT",
            "name" : "Jean",
            "firstName" : "Cive",
            "jobTitle" : "CTO",
            "email" : "jean.cive@sensodyne.com",
            "phoneNumber" : "0123456789"
          } ],
          "accreditations" : [ "PIS", "AIS", "CIS" ],
          "nationalAccreditationData" : {
            "organizationIdentifier" : "PSDFR-BDE-SENSODYNE",
            "competentAuthorityIdentifier" : "BDE",
            "competentAuthorityName" : "French registry",
            "competentAuthorityCountryCode" : "FR",
            "authorizationNumber" : "SENSODYNE",
            "competentAuthorityStatus" : "AUTHORISED",
            "registrationDate" : "2018-02-28",
            "accreditations" : [ "PIS", "AIS", "CIS" ]
          },
          "credentials" : {
            "trustedRedirectUrls" : [ "api.redirectUrl.com" ]
          }
        }
        ''')
        headers {
            header('''Content-Type''', '''application/json;charset=UTF-8''')
        }
    }
}