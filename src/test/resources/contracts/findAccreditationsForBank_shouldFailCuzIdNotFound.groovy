import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        method 'GET'
        url '/api/tpps/5c10b3d306f6f15cdc7b6541/accreditations'
        headers {
            header(authorization(), $(p('Bearer draven'), c('Bearer BANK_USER')))
            header(contentType(), applicationJsonUtf8())
        }
    }
    response {
        status 404
        body('''
        {
  "returnCode" : "ERROR",
  "traceId" : "65c5b2001aec35db",
  "ticketId" : "bk1qa0",
  "feedbacks" : [ {
    "code" : "RESOURCE_NOT_FOUND",
    "label" : "The specified resource does not exist",
    "severity" : "ERROR",
    "type" : "BUS",
    "spanId" : "65c5b2001aec35db",
    "origin" : "service-tpp-management",
    "source" : null,
    "parameters" : [ ],
    "internal" : {
      "message" : "Cannot find tpp with id 5c10b3d306f6f15cdc7b6541",
      "parameters" : [ ]
    }
  } ]
}
        ''')

        headers {
            header('''Content-Type''', '''application/json;charset=UTF-8''')
        }
        bodyMatchers {
            jsonPath('$.ticketId', byRegex(nonEmpty()))
            jsonPath('$.feedbacks[*].spanId', byRegex(nonEmpty()))
            jsonPath('$.traceId', byRegex(nonEmpty()))
            jsonPath('$.feedbacks[*].internal.message', byRegex(nonEmpty()))
        }
    }
}