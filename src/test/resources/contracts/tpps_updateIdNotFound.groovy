import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        method 'PUT'
        url '/api/tpps/5b4750098933d317d6ed9ba7'
        headers {
            header(authorization(), $(
                    p('Bearer draven'),
                    c('Bearer BANK_USER'))
            )
            header(contentType(), applicationJsonUtf8())
        }
        body('''
        {
          "id" : "5b4750098933d317d6ed9ba7",
          "name" : "Oral B",
          "comments" : "Oral B is a blocked TPP.",
          "contact" : {
            "email" : "oralb@mail.com",
            "phoneNumber" : "0123456789",
            "city" : "Paris",
            "addressLines" : [ "32 rue des Chicos.", "1 rue de la Carie." ],
            "postCode" : "4321",
            "country" : "France"
          },
          "state" : {
            "status" : "BLOCKED",
            "monitored" : true
          },
          "credentials" : {
            "trustedRedirectUrls":[ "api.redirectUrl.com" ]
          },
          "accreditations" : [ ],
          "webSite" : "oralb.com",
          "nationalAccreditationData" : {
            "organizationIdentifier" : "PSDFR-BDE-ORALB",
            "competentAuthorityCountryCode" : "FR",
            "competentAuthorityIdentifier" : "BDE",
            "competentAuthorityName" : "French registry",
            "authorizationNumber" : "ORALB",
            "competentAuthorityStatus": "AUTHORISED",
            "registrationDate" : "2018-02-28",
            "accreditations" : [ ]
          },
          "physicalContacts" : [ {
            "contactType" : "BUSINESS_CONTACT",
            "name" : "Morandini",
            "firstName" : "Jean-marc",
            "jobTitle" : "Clown",
            "email" : "jeanmarc.morandini@oralb.com",
            "phoneNumber" : "0123456789"
          }, {
            "contactType" : "TECHNICAL_CONTACT",
            "name" : "Lively",
            "firstName" : "Blake",
            "jobTitle" : "CTO",
            "email" : "blake.lively@oralb.com",
            "phoneNumber" : "0123456789"
          } ]
        }
        ''')
    }
    response {
        status 404
        body('''
        {
  "returnCode" : "ERROR",
  "traceId" : "95eaa0c80e84b7fe",
  "ticketId" : "dlpjiu",
  "feedbacks" : [ {
    "code" : "RESOURCE_NOT_FOUND",
    "label" : "The specified resource does not exist",
    "severity" : "ERROR",
    "type" : "BUS",
    "spanId" : "95eaa0c80e84b7fe",
    "origin" : "service-tpp-management",
    "source" : null,
    "parameters" : [ ],
    "internal" : {
      "message" : "Cannot find tpp with id 5bd9ca83909ea63fee0437d7",
      "parameters" : [ ]
    }
  } ]
}
        ''')
        bodyMatchers {
            jsonPath('$.ticketId', byRegex(nonEmpty()))
            jsonPath('$.feedbacks[*].spanId', byRegex(nonEmpty()))
            jsonPath('$.traceId', byRegex(nonEmpty()))
            jsonPath('$.feedbacks[*].internal.message', byRegex(nonEmpty()))
        }
        headers {
            header('''Content-Type''', '''application/json;charset=UTF-8''')
        }
    }
}