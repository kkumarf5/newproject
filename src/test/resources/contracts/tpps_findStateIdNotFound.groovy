import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        method 'GET'
        url '/api/tpps/5c10d0b306f6f12f2464e261/state'
        headers {
            header(authorization(), $(p('Bearer draven'), c('Bearer BANK_USER')))
            header(contentType(), applicationJsonUtf8())
        }
    }
    response {
        status 404
        body('''
        {
  "returnCode" : "ERROR",
  "traceId" : "b0bdfe6c3cd20138",
  "ticketId" : "yche96",
  "feedbacks" : [ {
    "code" : "RESOURCE_NOT_FOUND",
    "label" : "The specified resource does not exist",
    "severity" : "ERROR",
    "type" : "BUS",
    "spanId" : "b0bdfe6c3cd20138",
    "origin" : "service-tpp-management",
    "source" : null,
    "parameters" : [ ],
    "internal" : {
      "message" : "Cannot find tpp with id 5c258ed96399401a741bce7d",
      "parameters" : [ ]
    }
  } ]
}
''')
        headers {
            header('''Content-Type''', '''application/json;charset=UTF-8''')
        }
        bodyMatchers {
            jsonPath('$.ticketId', byRegex(nonEmpty()))
            jsonPath('$.feedbacks[*].spanId', byRegex(nonEmpty()))
            jsonPath('$.traceId', byRegex(nonEmpty()))
            jsonPath('$.feedbacks[*].internal.message', byRegex(nonEmpty()))
        }
    }
}