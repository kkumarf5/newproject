import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        method 'POST'
        url '/api/tpps'
        headers {
            header(authorization(), $(
                    p('Bearer draven'),
                    c('Bearer BANK_USER'))
            )
            header(contentType(), applicationJsonUtf8())
        }
        body('''
            {
  "id" : null,
  "name" : "Defense of the ancients bank",
  "comments" : "Not a bank for casuals",
  "webSite" : "dota.com",
  "state" : {
    "status" : "ACTIVE",
    "monitored" : true
  },
  "contact" : {
    "email" : "dota'‘®mail.com",
    "phoneNumber" : "1337",
    "city" : "Seattle",
    "addressLines" : [ "random line", "road 10" ],
    "postCode" : "4321",
    "country" : "USA"
  },
  "physicalContacts" : [ {
    "contactType" : "BUSINESS_CONTACT",
    "name" : "Doe",
    "firstName" : "John",
    "jobTitle" : "CEO",
    "email" : "john.doe@dota.com",
    "phoneNumber" : "0123456789"
  }, {
    "contactType" : "TECHNICAL_CONTACT",
    "name" : "forest",
    "firstName" : "Jim",
    "jobTitle" : "CTO",
    "email" : "jim.forest@dota.com",
    "phoneNumber" : "0123456789"
  } ],
  "accreditations" : [ "AIS", "PIS" ],
  "nationalAccreditationData" : {
    "organizationIdentifier" : null,
    "competentAuthorityIdentifier" : null,
    "competentAuthorityName" : null,
    "competentAuthorityCountryCode" : null,
    "authorizationNumber" : null,
    "competentAuthorityStatus" : null,
    "registrationDate" : null,
    "accreditations" : [ ]
  },
  "credentials" : {
    "tppId" : null,
    "clientId" : null,
    "secret" : null,
    "trustedRedirectUrls" : [ "api.redirectUrls.com" ],
    "scopes" : [ ]
  }
}
        ''')
    }
    response {
        status 400
        body('''
        {
  "returnCode" : "ERROR",
  "traceId" : "24aacb0283253ec1",
  "ticketId" : "l6tixd",
  "feedbacks" : [ {
    "code" : "INVALID_CONTENT",
    "label" : "Invalid request payload",
    "severity" : "ERROR",
    "type" : "BUS",
    "spanId" : "24aacb0283253ec1",
    "origin" : "service-tpp-management",
    "source" : "nationalAccreditationData.competentAuthorityStatus",
    "parameters" : [ ],
    "internal" : {
      "message" : "Validation failed for field nationalAccreditationData.competentAuthorityStatus with code NotNull because must not be null",
      "parameters" : [ ]
    }
  }, {
    "code" : "INVALID_CONTENT",
    "label" : "Invalid request payload",
    "severity" : "ERROR",
    "type" : "BUS",
    "spanId" : "24aacb0283253ec1",
    "origin" : "service-tpp-management",
    "source" : "nationalAccreditationData.competentAuthorityIdentifier",
    "parameters" : [ ],
    "internal" : {
      "message" : "Validation failed for field nationalAccreditationData.competentAuthorityIdentifier with code NotBlank because must not be blank",
      "parameters" : [ ]
    }
  }, {
    "code" : "INVALID_CONTENT",
    "label" : "Invalid request payload",
    "severity" : "ERROR",
    "type" : "BUS",
    "spanId" : "24aacb0283253ec1",
    "origin" : "service-tpp-management",
    "source" : "nationalAccreditationData.organizationIdentifier",
    "parameters" : [ ],
    "internal" : {
      "message" : "Validation failed for field nationalAccreditationData.organizationIdentifier with code NotBlank because must not be blank",
      "parameters" : [ ]
    }
  }, {
    "code" : "INVALID_CONTENT",
    "label" : "Invalid request payload",
    "severity" : "ERROR",
    "type" : "BUS",
    "spanId" : "24aacb0283253ec1",
    "origin" : "service-tpp-management",
    "source" : "nationalAccreditationData.authorizationNumber",
    "parameters" : [ ],
    "internal" : {
      "message" : "Validation failed for field nationalAccreditationData.authorizationNumber with code NotBlank because must not be blank",
      "parameters" : [ ]
    }
  }, {
    "code" : "INVALID_CONTENT",
    "label" : "Invalid request payload",
    "severity" : "ERROR",
    "type" : "BUS",
    "spanId" : "24aacb0283253ec1",
    "origin" : "service-tpp-management",
    "source" : "nationalAccreditationData.competentAuthorityName",
    "parameters" : [ ],
    "internal" : {
      "message" : "Validation failed for field nationalAccreditationData.competentAuthorityName with code NotBlank because must not be blank",
      "parameters" : [ ]
    }
  }, {
    "code" : "INVALID_CONTENT",
    "label" : "Invalid request payload",
    "severity" : "ERROR",
    "type" : "BUS",
    "spanId" : "24aacb0283253ec1",
    "origin" : "service-tpp-management",
    "source" : "nationalAccreditationData.competentAuthorityCountryCode",
    "parameters" : [ ],
    "internal" : {
      "message" : "Validation failed for field nationalAccreditationData.competentAuthorityCountryCode with code NotBlank because must not be blank",
      "parameters" : [ ]
    }
  } ]
}
        ''')
        bodyMatchers {
            jsonPath('$.ticketId', byRegex(nonEmpty()))
            jsonPath('$.feedbacks[*].spanId', byRegex(nonEmpty()))
            jsonPath('$.traceId', byRegex(nonEmpty()))
            jsonPath('$.feedbacks[*].internal.message', byRegex(nonEmpty()))
        }
        headers {
            header('''Content-Type''', '''application/json;charset=UTF-8''')
            header('''Cache-Control''', '''no-cache, no-store, max-age=0, must-revalidate''')
            header('''Pragma''', '''no-cache''')
            header('''Expires''', '''0''')
            header('''X-Content-Type-Options''', '''nosniff''')
            header('''X-Frame-Options''', '''DENY''')
            header('''X-XSS-Protection''', '''1 ; mode=block''')
        }
    }
}