import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        method 'GET'
        url '/api/tpps?clientId=PSDFR-BDE-SENSODYNE'
        headers {
            header(authorization(), $(
                    p('Bearer heimerdinger'),
                    c('Bearer DXP_TECHNICAL'))
            )
        }
    }
    response {
        status 200
        body('''
        {
          "id" : "a00000000000000000000002",
          "name" : "Sensodyne",
          "comments" : "Tpp active with all accreditations",
          "contact" : {
            "email" : "sensodyne@mail.com",
            "phoneNumber" : "0123456789",
            "city" : "Nancy",
            "addressLines" : [ "24 boulevard de la Couronne", "12 rue de l'email" ],
            "postCode" : "4321",
            "country" : "France"
          },
          "state" : {
            "status" : "ACTIVE",
            "monitored" : true
          },
          "credentials" : {
            "tppId" : "a00000000000000000000002",
            "clientId" : "PSDFR-BDE-SENSODYNE",
            "secret" : "123456",
            "trustedRedirectUrls":[ "api.redirectUrl.com" ],
            "scopes" : [ "pisp", "openid", "aisp", "cisp" ]
          },
          "accreditations" : [ "AIS", "CIS", "PIS" ],
          "webSite" : "sensodyne.com",
          "nationalAccreditationData" : {
            "organizationIdentifier" : "PSDFR-BDE-SENSODYNE",
            "competentAuthorityCountryCode" : "FR",
            "competentAuthorityIdentifier" : "BDE",
            "competentAuthorityName" : "French registry",
            "authorizationNumber" : "SENSODYNE",
            "competentAuthorityStatus": "AUTHORISED",
            "registrationDate" : "2018-02-28",
            "accreditations" : [ "AIS", "CIS", "PIS" ]
          },
          "physicalContacts" : [ {
            "contactType" : "BUSINESS_CONTACT",
            "name" : "Mabel",
            "firstName" : "Dentition",
            "jobTitle" : "CEO",
            "email" : "mabel.dentition@sensodyne.com",
            "phoneNumber" : "0123456789"
          }, {
            "contactType" : "TECHNICAL_CONTACT",
            "name" : "Jean",
            "firstName" : "Cive",
            "jobTitle" : "CTO",
            "email" : "jean.cive@sensodyne.com",
            "phoneNumber" : "0123456789"
          } ]
        }
        ''')
        headers {
            header('''Content-Type''', '''application/json;charset=UTF-8''')
        }
    }
}