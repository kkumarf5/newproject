import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        method 'GET'
        url '/api/tpps/a00000000000000000000003'
        headers {
            header(authorization(), $(
                    p('Bearer heimerdinger'),
                    c('Bearer DXP_TECHNICAL'))
            )
        }
    }
    response {
        status 200
        body('''
        {
          "id" : "a00000000000000000000003",
          "name" : "Bookee",
          "comments" : "Tpp active with all accreditations but no client",
          "contact" : {
            "email" : "bookee@mail.com",
            "phoneNumber" : "0123456789",
            "city" : "Liege",
            "addressLines" : [ "20 rue du client douteux", "1 rue de solitude" ],
            "postCode" : "4321",
            "country" : "Belgium"
          },
          "state" : {
            "status" : "ACTIVE",
            "monitored" : true
          },
          "accreditations" : [ "AIS", "PIS", "CIS" ],
          "webSite" : "bookee.com",
          "credentials" : {
            "trustedRedirectUrls":[ "api.redirectUrl.com" ]
          },
          "nationalAccreditationData" : {
            "organizationIdentifier" : "PSDBE-BDE-BOOKEE",
            "competentAuthorityCountryCode" : "BE",
            "competentAuthorityIdentifier" : "BDE",
            "competentAuthorityName" : "Belgium registry",
            "authorizationNumber" : "BOOKEE",
            "competentAuthorityStatus": "AUTHORISED",
            "registrationDate" : "2018-02-28",
            "accreditations" : [ "AIS", "CIS", "PIS" ]
          },
          "physicalContacts" : [ {
            "contactType" : "BUSINESS_CONTACT",
            "name" : "Client",
            "firstName" : "Jépade",
            "jobTitle" : "CEO",
            "email" : "jépade.client@bookee.com",
            "phoneNumber" : "0123456789"
          }, {
            "contactType" : "TECHNICAL_CONTACT",
            "name" : "Crute",
            "firstName" : "Sareuh",
            "jobTitle" : "CTO",
            "email" : "sareuh.crute@sensodyne.com",
            "phoneNumber" : "0123456789"
          } ]
        }
        ''')
        headers {
            header('''Content-Type''', '''application/json;charset=UTF-8''')
        }
    }
}