import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        method 'GET'
        url '/api/tpps?ids=a00000000000000000000000,a00000000000000000000001'
        headers {
            header(authorization(), $(p('Bearer ashe'), c('Bearer PSU_BY_BANK')))
        }
    }
    response {
        status 200
        body('''
        [
            {
              "id" : "a00000000000000000000000",
              "name" : "Oral B",
              "contact" : {
                "email" : "oralb@mail.com",
                "phoneNumber" : "0123456789",
                "city" : "Paris",
                "addressLines" : [ "32 rue des Chicos.", "1 rue de la Carie." ],
                "postCode" : "4321",
                "country" : "France"
              }
            },
            {
              "id" : "a00000000000000000000001",
              "name" : "Activia",
              "contact" : {
                "email" : "activia@mail.com",
                "phoneNumber" : "0123456789",
                "city" : "Dunkerque",
                "addressLines" : [ "1 rue de la Pie.", "2 boulevard de la legereté." ],
                "postCode" : "4321",
                "country" : "France"
              }
            }
        ]
        ''')
        headers {
            header('''Content-Type''', '''application/json;charset=UTF-8''')
        }
    }
}