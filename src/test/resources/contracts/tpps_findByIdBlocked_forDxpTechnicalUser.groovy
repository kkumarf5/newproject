import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        method 'GET'
        url '/api/tpps/a00000000000000000000000'
        headers {
            header(authorization(), $(
                    p('Bearer heimerdinger'),
                    c('Bearer DXP_TECHNICAL'))
            )
        }
    }
    response {
        status 200
        body('''
        {
          "id" : "a00000000000000000000000",
          "name" : "Oral B",
          "comments" : "Oral B is a blocked TPP.",
          "contact" : {
            "email" : "oralb@mail.com",
            "phoneNumber" : "0123456789",
            "city" : "Paris",
            "addressLines" : [ "32 rue des Chicos.", "1 rue de la Carie." ],
            "postCode" : "4321",
            "country" : "France"
          },
          "state" : {
            "status" : "BLOCKED",
            "monitored" : true
          },
          "credentials" : {
            "trustedRedirectUrls":[ "api.redirectUrl.com" ]
          },
          "accreditations" : [ ],
          "webSite" : "oralb.com",
          "nationalAccreditationData" : {
            "organizationIdentifier" : "PSDFR-BDE-ORALB",
            "competentAuthorityCountryCode" : "FR",
            "competentAuthorityIdentifier" : "BDE",
            "competentAuthorityName" : "French registry",
            "authorizationNumber" : "ORALB",
            "competentAuthorityStatus": "AUTHORISED",
            "registrationDate" : "2018-02-28",
            "accreditations" : [ ]
          },
          "physicalContacts" : [ {
            "contactType" : "BUSINESS_CONTACT",
            "name" : "Morandini",
            "firstName" : "Jean-marc",
            "jobTitle" : "Clown",
            "email" : "jeanmarc.morandini@oralb.com",
            "phoneNumber" : "0123456789"
          }, {
            "contactType" : "TECHNICAL_CONTACT",
            "name" : "Lively",
            "firstName" : "Blake",
            "jobTitle" : "CTO",
            "email" : "blake.lively@oralb.com",
            "phoneNumber" : "0123456789"
          } ]
        }
        ''')
        headers {
            header('''Content-Type''', '''application/json;charset=UTF-8''')
        }
    }
}