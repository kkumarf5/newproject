import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        method 'GET'
        url '/api/tpps/59b13f31fb7b7dcdeb0becfe'
        headers {
            header(authorization(), $(
                    p('Bearer draven'),
                    c('Bearer BANK_USER'))
            )
        }
    }
    response {
        status 200
        body('''
        {
          "id" : "59b13f31fb7b7dcdeb0becfe",
          "name" : "ABC bank",
          "comments" : "ABC first bank",
          "contact" : {
            "email" : "abc@mail.com",
            "phoneNumber" : "0123456789",
            "city" : "Paris",
            "addressLines" : [ "random line", "road 10" ],
            "postCode" : "4321",
            "country" : "France"
          },
          "state" : {
            "status" : "ACTIVE",
            "monitored" : false
          },
          "accreditations" : [ "CIS", "AIS" ],
          "webSite" : "abc.com",
          "nationalAccreditationData" : {
            "organizationIdentifier" : "PSDFR-BDE-ABCBANK",
            "competentAuthorityCountryCode" : "FR",
            "competentAuthorityIdentifier" : "BDE",
            "competentAuthorityName" : "French registry",
            "authorizationNumber" : "ABCBANK",
            "competentAuthorityStatus": "AUTHORISED",
            "registrationDate" : "2018-02-28",
            "accreditations" : [ "CIS", "AIS" ]
          },
          "physicalContacts" : [ {
            "contactType" : "BUSINESS_CONTACT",
            "name" : "Doe",
            "firstName" : "John",
            "jobTitle" : "CEO",
            "email" : "john.doe@solidarity.com",
            "phoneNumber" : "0123456789"
          }, {
            "contactType" : "TECHNICAL_CONTACT",
            "name" : "forest",
            "firstName" : "Jim",
            "jobTitle" : "CTO",
            "email" : "jim.forest@solidarity.com",
            "phoneNumber" : "0123456789"
          } ],
          "credentials" : {
            "trustedRedirectUrls":[ "api.redirectUrl.com" ]
          }
        }
        ''')
        headers {
            header('''Content-Type''', '''application/json;charset=UTF-8''')
        }
        bodyMatchers { jsonPath('$.id', byRegex("^[0-9a-f]{24}\$")) }
    }
}