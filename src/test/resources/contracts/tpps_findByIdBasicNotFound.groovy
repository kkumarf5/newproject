import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        method 'GET'
        url '/api/tpps/5b47500c8933d317d6ed9c2d'
        headers {
            header(authorization(), $(
                    p('Bearer draven'),
                    c('Bearer BANK_USER'))
            )
        }
    }
    response {
        status 404
        body('''
        {
  "returnCode" : "ERROR",
  "traceId" : "dc60dbf80f8eb3cf",
  "ticketId" : "8cvue2",
  "feedbacks" : [ {
    "code" : "RESOURCE_NOT_FOUND",
    "label" : "The specified resource does not exist",
    "severity" : "ERROR",
    "type" : "BUS",
    "spanId" : "dc60dbf80f8eb3cf",
    "origin" : "service-tpp-management",
    "source" : null,
    "parameters" : [ ],
    "internal" : {
      "message" : "Cannot find tpp with id 5bed449340ba174bb93908fa",
      "parameters" : [ ]
    }
  } ]
}
        ''')
        headers {
            header('''Content-Type''', '''application/json;charset=UTF-8''')
            header('''Cache-Control''', '''no-cache, no-store, max-age=0, must-revalidate''')
            header('''Pragma''', '''no-cache''')
            header('''Expires''', '''0''')
            header('''X-Content-Type-Options''', '''nosniff''')
            header('''X-Frame-Options''', '''DENY''')
            header('''X-XSS-Protection''', '''1 ; mode=block''')
        }
        bodyMatchers {
            jsonPath('$.ticketId', byRegex(nonEmpty()))
            jsonPath('$.feedbacks[*].spanId', byRegex(nonEmpty()))
            jsonPath('$.traceId', byRegex(nonEmpty()))
            jsonPath('$.feedbacks[*].internal.message', byRegex(nonEmpty()))
        }
    }
}