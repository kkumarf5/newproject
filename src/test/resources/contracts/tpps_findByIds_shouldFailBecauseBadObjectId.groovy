import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        method 'GET'
        url '/api/tpps?ids=BAD_OBJECT_ID'
        headers {
            header(authorization(), $(p('Bearer ashe'), c('Bearer PSU_BY_BANK')))
        }
    }
    response {
        status 400
        body('''
        {
  "returnCode" : "ERROR",
  "traceId" : "5efdf7735926a726",
  "ticketId" : "e120vc",
  "feedbacks" : [ {
    "code" : "INVALID_CONTENT",
    "label" : "Invalid request payload",
    "severity" : "ERROR",
    "type" : "BUS",
    "spanId" : "5efdf7735926a726",
    "origin" : "service-tpp-management",
    "source" : null,
    "parameters" : [ ],
    "internal" : {
      "message" : "Invalid identifier format BAD_OBJECT_ID",
      "parameters" : [ ]
    }
  } ]
}
        ''')
        bodyMatchers {
            jsonPath('$.ticketId', byRegex(nonEmpty()))
            jsonPath('$.feedbacks[*].spanId', byRegex(nonEmpty()))
            jsonPath('$.traceId', byRegex(nonEmpty()))
            jsonPath('$.feedbacks[*].internal.message', byRegex(nonEmpty()))
        }
        headers {
            header('''Content-Type''', '''application/json;charset=UTF-8''')
        }
    }
}