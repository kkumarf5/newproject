=== Retrieve a list of TPP accreditations and status

==== Description

A `GET` request is used to retrieve  a list of tpp's containing only accreditations and status.
When a microservice is started, for instance AIS or PIS, it will update its accreditation list with this view.

This API is available for a Dxp Technical user.

==== Request Header

include::{snippets}/tpps_findAll_AccreditationsAndStatusView/request-headers.adoc[]

==== Response Header

==== Fields Format

**Request Structure**

include::{snippets}/tpps_findAllAccreditationsAndStatusView/http-request.adoc[]

**Response Structure**

include::{snippets}/tpps_findAllAccreditationsAndStatusView/response-fields.adoc[]

include::{snippets}/tpps_findAllAccreditationsAndStatusView/http-response.adoc[]

==== Return & Error Codes

|===
| HTTP Status | Description

| `401 Unauthorized`
| Authorization header missing or invalid token.

| `400 Bad request`
| Path identifier could be found but the TPP id was not found in database.

|===

==== Pagination

This endpoint does support pagination.