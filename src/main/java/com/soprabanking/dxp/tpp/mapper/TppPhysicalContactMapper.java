package com.soprabanking.dxp.tpp.mapper;

import com.soprabanking.dxp.tpp.model.api.TppPhysicalContactApiDTO;
import com.soprabanking.dxp.tpp.model.entity.TppPhysicalContact;

public final class TppPhysicalContactMapper {

    private TppPhysicalContactMapper() {
    }

    public static TppPhysicalContactApiDTO map(TppPhysicalContact input) {
        return new TppPhysicalContactApiDTO()
                .setContactType(input.getContactType())
                .setName(input.getName())
                .setFirstName(input.getFirstName())
                .setJobTitle(input.getJobTitle())
                .setEmail(input.getEmail())
                .setPhoneNumber(input.getPhoneNumber());

    }

    public static TppPhysicalContact map(TppPhysicalContactApiDTO input) {
        return new TppPhysicalContact()
                .setContactType(input.getContactType())
                .setName(input.getName())
                .setFirstName(input.getFirstName())
                .setJobTitle(input.getJobTitle())
                .setEmail(input.getEmail())
                .setPhoneNumber(input.getPhoneNumber());

    }
}
