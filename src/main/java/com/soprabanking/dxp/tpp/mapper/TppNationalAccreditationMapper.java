package com.soprabanking.dxp.tpp.mapper;

import com.soprabanking.dxp.tpp.model.api.TppNationalAccreditationApiDTO;
import com.soprabanking.dxp.tpp.model.entity.TppNationalAccreditation;

public final class TppNationalAccreditationMapper {

    private TppNationalAccreditationMapper() {
    }

    public static TppNationalAccreditationApiDTO map(TppNationalAccreditation input) {
        return new TppNationalAccreditationApiDTO()
                .setOrganizationIdentifier(input.getOrganizationIdentifier())
                .setCompetentAuthorityIdentifier(input.getCompetentAuthorityIdentifier())
                .setCompetentAuthorityName(input.getCompetentAuthorityName())
                .setCompetentAuthorityCountryCode(input.getCompetentAuthorityCountryCode())
                .setAuthorizationNumber(input.getAuthorizationNumber())
                .setCompetentAuthorityStatus(input.getCompetentAuthorityStatus())
                .setRegistrationDate(input.getRegistrationDate())
                .setAccreditations(input.getAccreditations());
    }

    public static TppNationalAccreditation map(TppNationalAccreditationApiDTO input) {
        return new TppNationalAccreditation()
                .setOrganizationIdentifier(input.getOrganizationIdentifier())
                .setCompetentAuthorityIdentifier(input.getCompetentAuthorityIdentifier())
                .setCompetentAuthorityName(input.getCompetentAuthorityName())
                .setCompetentAuthorityCountryCode(input.getCompetentAuthorityCountryCode())
                .setAuthorizationNumber(input.getAuthorizationNumber())
                .setCompetentAuthorityStatus(input.getCompetentAuthorityStatus())
                .setRegistrationDate(input.getRegistrationDate())
                .setAccreditations(input.getAccreditations());
    }
}
