package com.soprabanking.dxp.tpp.mapper;

import com.soprabanking.dxp.tpp.model.api.TppContactApiDTO;
import com.soprabanking.dxp.tpp.model.entity.TppContact;

/**
 * Mappings related to {@link TppContact}
 */
final class TppContactMapper {

    private TppContactMapper() {
    }

    static TppContact map(TppContactApiDTO input) {
        return new TppContact()
                .setEmail(input.getEmail())
                .setPhoneNumber(input.getPhoneNumber())
                .setAddressLines(input.getAddressLines())
                .setPostCode(input.getPostCode())
                .setCountry(input.getCountry())
                .setCity(input.getCity());
    }

    static TppContactApiDTO map(TppContact input) {
        return new TppContactApiDTO()
                .setEmail(input.getEmail())
                .setPhoneNumber(input.getPhoneNumber())
                .setAddressLines(input.getAddressLines())
                .setPostCode(input.getPostCode())
                .setCountry(input.getCountry())
                .setCity(input.getCity());
    }
}
