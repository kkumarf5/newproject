package com.soprabanking.dxp.tpp.mapper;

import com.soprabanking.dxp.tpp.model.api.TppStateApiDTO;
import com.soprabanking.dxp.tpp.model.entity.TppState;
import com.soprabanking.dxp.tpp.model.entity.TppStatus;

/**
 * Mappings related to {@link TppState}
 */
public final class TppStateMapper {

    private TppStateMapper() {
    }

    public static TppState map(TppStateApiDTO input) {
        return new TppState()
                .setStatus(mapDefaultStatus(input))
                .setMonitored(input.isMonitored());
    }

    public static TppStateApiDTO map(TppState input) {
        return new TppStateApiDTO()
                .setStatus(input.getStatus())
                .setMonitored(input.isMonitored());
    }

    private static TppStatus mapDefaultStatus(TppStateApiDTO input) {
        TppStatus status = input.getStatus();
        return status != null ? status : TppStatus.ACTIVE;
    }

}
