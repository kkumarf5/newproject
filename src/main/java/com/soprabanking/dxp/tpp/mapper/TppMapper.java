package com.soprabanking.dxp.tpp.mapper;

import com.soprabanking.dxp.commons.error.DxpSbsException;
import com.soprabanking.dxp.commons.error.DxpSbsFeedback;
import com.soprabanking.dxp.commons.mongodb.ObjectIdExtensions;
import com.soprabanking.dxp.tpp.model.api.TppApiDTO;
import com.soprabanking.dxp.tpp.model.api.TppContactApiDTO;
import com.soprabanking.dxp.tpp.model.api.TppCredentialApiDTO;
import com.soprabanking.dxp.tpp.model.api.TppNationalAccreditationApiDTO;
import com.soprabanking.dxp.tpp.model.api.TppPhysicalContactApiDTO;
import com.soprabanking.dxp.tpp.model.api.TppStateApiDTO;
import com.soprabanking.dxp.tpp.model.entity.Tpp;
import com.soprabanking.dxp.tpp.model.entity.TppContact;
import com.soprabanking.dxp.tpp.model.entity.TppCredentials;
import com.soprabanking.dxp.tpp.model.entity.TppNationalAccreditation;
import com.soprabanking.dxp.tpp.model.entity.TppPhysicalContact;
import com.soprabanking.dxp.tpp.model.entity.TppState;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.soprabanking.dxp.commons.error.DxpBusinessCode.INVALID_CONTENT;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * Mappings related to {@link Tpp}
 */
public final class TppMapper {

    private static final Logger LOGGER = getLogger(TppMapper.class);

    private static final DxpSbsException MISSING_ORGANIZATION_IDENTIFIER =
            new DxpSbsFeedback("Required Organization Identifier data is missing.", INVALID_CONTENT, LOGGER::debug).toException();

    private TppMapper() {
    }

    public static Tpp map(TppApiDTO apiDto) {
        return new Tpp()
                .withId(mapIdIfExist(apiDto))
                .setName(apiDto.getName())
                .setComments(apiDto.getComments())
                .setWebSite(apiDto.getWebSite())
                .setState(mapStateIfExist(apiDto))
                .setContact(mapContactIfExist(apiDto))
                .setPhysicalContacts(mapPhysicalContactListToEntity(apiDto))
                .setAccreditations(apiDto.getAccreditations())
                .setCredentials(mapCredentialsIfExist(apiDto))
                .setNationalAccreditationData(mapINationalAccreditationDataIfExist(apiDto));
    }

    public static TppApiDTO map(Tpp input) {
        return new TppApiDTO()
                .withId(mapIdIfExist(input))
                .setName(input.getName())
                .setComments(input.getComments())
                .setWebSite(input.getWebSite())
                .setState(mapStateIfExist(input))
                .setContact(mapContactIfExist(input))
                .setPhysicalContacts(mapPhysicalContactListToDto(input))
                .setCredentials(mapCredentialsIfExist(input))
                .setAccreditations(input.getAccreditations())
                .setNationalAccreditationData(mapINationalAccreditationDataIfExist(input));
    }

    private static List<TppPhysicalContactApiDTO> mapPhysicalContactListToDto(Tpp input) {
        return input.getPhysicalContacts().stream().map(TppPhysicalContactMapper::map).collect(Collectors.toList());
    }

    private static List<TppPhysicalContact> mapPhysicalContactListToEntity(TppApiDTO input) {
        return input.getPhysicalContacts().stream().map(TppPhysicalContactMapper::map).collect(Collectors.toList());
    }

    private static TppNationalAccreditation mapINationalAccreditationDataIfExist(TppApiDTO input) {
        return Optional.ofNullable(input)
                       .map(TppApiDTO::getNationalAccreditationData)
                       .map(TppNationalAccreditationMapper::map)
                       .orElse(null);
    }

    private static TppNationalAccreditationApiDTO mapINationalAccreditationDataIfExist(Tpp input) {
        return Optional.ofNullable(input)
                       .map(Tpp::getNationalAccreditationData)
                       .map(TppNationalAccreditationMapper::map)
                       .orElse(null);
    }

    public static Tpp merge(Tpp patch, Tpp existing) {
        BeanUtils.copyProperties(patch, existing, MappingUtils.findNullFieldNamesValues(patch));
        return existing;
    }

    private static String mapIdIfExist(Tpp input) {
        return input.getId() != null ? input.getId().toString() : null;
    }

    private static ObjectId mapIdIfExist(TppApiDTO input) {
        return input.getId() != null ? ObjectIdExtensions.toObjectId(input.getId()) : null;
    }

    private static TppContactApiDTO mapContactIfExist(Tpp input) {
        return input.getContact() != null ? TppContactMapper.map(input.getContact()) : null;
    }

    private static TppContact mapContactIfExist(TppApiDTO input) {
        return input.getContact() != null ? TppContactMapper.map(input.getContact()) : null;
    }

    private static TppStateApiDTO mapStateIfExist(Tpp input) {
        return input.getState() != null ? TppStateMapper.map(input.getState()) : null;
    }

    private static TppState mapStateIfExist(TppApiDTO input) {
        return input.getState() != null ? TppStateMapper.map(input.getState()) : null;
    }

    private static TppCredentialApiDTO mapCredentialsIfExist(Tpp input) {
        return input.getCredentials() != null ? TppCredentialsMapper.map(input.getCredentials(), input.getId()) : null;
    }

    private static TppCredentials mapCredentialsIfExist(TppApiDTO input) {
        return input.getCredentials() != null ?
                new TppCredentials().setTrustedRedirectUrls(input.getCredentials().getTrustedRedirectUrls()) : null;
    }

    public static String extractOrganizationIdentifier(Tpp input) {
        return Optional.of(input)
                       .map(Tpp::getNationalAccreditationData)
                       .map(TppNationalAccreditation::getOrganizationIdentifier)
                       .orElseThrow(() -> MISSING_ORGANIZATION_IDENTIFIER);

    }
}
