package com.soprabanking.dxp.tpp.mapper;

import com.soprabanking.dxp.tpp.model.api.TppCredentialApiDTO;
import com.soprabanking.dxp.tpp.model.entity.Tpp;
import com.soprabanking.dxp.tpp.model.entity.TppCredentials;
import org.bson.types.ObjectId;

/**
 * Mappings related to {@link TppCredentials}
 */
public final class TppCredentialsMapper {

    private TppCredentialsMapper() {
    }

    public static TppCredentials map(TppCredentialApiDTO input) {
        return new TppCredentials()
                .setClientId(input.getClientId())
                .setSecret(input.getSecret())
                .setTrustedRedirectUrls(input.getTrustedRedirectUrls())
                .setScopes(input.getScopes());
    }

    public static TppCredentialApiDTO map(TppCredentials input, ObjectId tppId) {
        return new TppCredentialApiDTO()
                .setTppId(tppId.toString())
                .setClientId(input.getClientId())
                .setSecret(input.getSecret())
                .setTrustedRedirectUrls(input.getTrustedRedirectUrls())
                .setScopes(input.getScopes());
    }

    public static TppCredentialApiDTO map(Tpp tpp) {
        return map(tpp.getCredentials(), tpp.getId());
    }
}
