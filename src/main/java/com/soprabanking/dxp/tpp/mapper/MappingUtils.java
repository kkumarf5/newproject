package com.soprabanking.dxp.tpp.mapper;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

import java.beans.FeatureDescriptor;
import java.util.Collection;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * Mapping utilities
 */
final class MappingUtils {

    private MappingUtils() {
    }

    static String[] findNullFieldNamesValues(Object source) {
        return findFieldNamesValuesMatching(source, p -> nullOrEmptyCollection(p.getValue()));
    }

    private static boolean nullOrEmptyCollection(Object source) {
        return isNull(source) || isEmptyCollection(source);
    }

    private static boolean isNull(Object value) {
        return value == null;
    }

    private static boolean isEmptyCollection(Object value) {
        if (value instanceof Collection) {
            Collection collection = (Collection) value;
            return collection.isEmpty();
        }
        return false;
    }

    private static String[] findFieldNamesValuesMatching(Object source, Predicate<Pair<String, Object>> predicate) {
        BeanWrapper wrappedSource = new BeanWrapperImpl(source);
        return Stream.of(wrappedSource.getPropertyDescriptors())
                     .map(FeatureDescriptor::getName)
                     .map(n -> Pair.of(n, wrappedSource.getPropertyValue(n)))
                     .filter(predicate)
                     .map(Pair::getKey)
                     .toArray(String[]::new);
    }
}