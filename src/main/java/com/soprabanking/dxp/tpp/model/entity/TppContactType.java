package com.soprabanking.dxp.tpp.model.entity;

public enum TppContactType {
    BUSINESS_CONTACT,
    TECHNICAL_CONTACT;
}
