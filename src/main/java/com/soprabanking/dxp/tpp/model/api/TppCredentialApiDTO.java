package com.soprabanking.dxp.tpp.model.api;

import com.fasterxml.jackson.annotation.JsonView;
import com.soprabanking.dxp.commons.constants.DTO;

import javax.validation.constraints.Null;
import java.util.HashSet;
import java.util.Set;

import static com.soprabanking.dxp.tpp.model.api.TppGroups.Creation;
import static com.soprabanking.dxp.tpp.model.api.TppGroups.Patch;
import static com.soprabanking.dxp.tpp.model.api.TppGroups.Update;
import static com.soprabanking.dxp.tpp.model.api.TppViews.Basic;
import static com.soprabanking.dxp.tpp.model.api.TppViews.WithCredentials;

/**
 * API representation of TPP credentials
 */
public class TppCredentialApiDTO implements DTO {

    @Null(groups = {Creation.class, Update.class, Patch.class})
    @JsonView(WithCredentials.class)
    private String tppId;

    @Null(groups = {Creation.class, Update.class, Patch.class})
    @JsonView(WithCredentials.class)
    private String clientId;

    @Null(groups = {Creation.class, Update.class, Patch.class})
    @JsonView(WithCredentials.class)
    private String secret;

    @JsonView(Basic.class)
    private Set<String> trustedRedirectUrls = new HashSet<>();

    @JsonView(WithCredentials.class)
    private Set<String> scopes = new HashSet<>();

    public String getTppId() {
        return tppId;
    }

    public TppCredentialApiDTO setTppId(String tppId) {
        this.tppId = tppId;
        return this;
    }

    public String getClientId() {
        return clientId;
    }

    public TppCredentialApiDTO setClientId(String clientId) {
        this.clientId = clientId;
        return this;
    }

    public String getSecret() {
        return secret;
    }

    public TppCredentialApiDTO setSecret(String secret) {
        this.secret = secret;
        return this;
    }

    public Set<String> getTrustedRedirectUrls() {
        return trustedRedirectUrls;
    }

    public TppCredentialApiDTO setTrustedRedirectUrls(Set<String> trustedRedirectUrls) {
        this.trustedRedirectUrls = trustedRedirectUrls;
        return this;
    }

    public Set<String> getScopes() {
        return scopes;
    }

    public TppCredentialApiDTO setScopes(Set<String> scopes) {
        this.scopes = scopes;
        return this;
    }
}
