package com.soprabanking.dxp.tpp.model.api;

import com.fasterxml.jackson.annotation.JsonView;
import com.soprabanking.dxp.tpp.model.api.TppViews.Basic;
import com.soprabanking.dxp.tpp.model.entity.TppStatus;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.validation.constraints.NotNull;

import static com.soprabanking.dxp.tpp.model.api.TppGroups.Creation;
import static com.soprabanking.dxp.tpp.model.api.TppGroups.Update;

/**
 * API representation of the state of a TPP
 */
public class TppStateApiDTO {

    @NotNull(groups = {Creation.class, Update.class})
    @JsonView(Basic.class)
    private TppStatus status;

    @JsonView(Basic.class)
    private boolean monitored;

    public TppStatus getStatus() {
        return status;
    }

    public TppStateApiDTO setStatus(TppStatus status) {
        this.status = status;
        return this;
    }

    public boolean isMonitored() {
        return monitored;
    }

    public TppStateApiDTO setMonitored(boolean monitored) {
        this.monitored = monitored;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("status", status)
                .append("monitored", monitored)
                .toString();
    }
}
