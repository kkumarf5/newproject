package com.soprabanking.dxp.tpp.model.entity;

/**
 * Enumeration of the TPP accreditation
 */
public enum TppAccreditation {
    AIS,
    CIS,
    PIS
}
