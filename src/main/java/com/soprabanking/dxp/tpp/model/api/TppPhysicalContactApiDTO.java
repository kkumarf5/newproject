package com.soprabanking.dxp.tpp.model.api;

import com.fasterxml.jackson.annotation.JsonView;
import com.soprabanking.dxp.tpp.model.entity.TppContactType;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class TppPhysicalContactApiDTO {

    @JsonView(TppViews.Basic.class)
    private TppContactType contactType;

    @JsonView(TppViews.Basic.class)
    private String name;

    @JsonView(TppViews.Basic.class)
    private String firstName;

    @JsonView(TppViews.Basic.class)
    private String jobTitle;

    @JsonView(TppViews.Basic.class)
    private String email;

    @JsonView(TppViews.Basic.class)
    private String phoneNumber;

    public String getName() {
        return name;
    }

    public TppPhysicalContactApiDTO setName(String name) {
        this.name = name;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public TppPhysicalContactApiDTO setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public TppPhysicalContactApiDTO setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public TppPhysicalContactApiDTO setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public TppPhysicalContactApiDTO setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    public TppContactType getContactType() {
        return contactType;
    }

    public TppPhysicalContactApiDTO setContactType(TppContactType contactType) {
        this.contactType = contactType;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("contactType", contactType)
                .append("name", name)
                .toString();
    }
}
