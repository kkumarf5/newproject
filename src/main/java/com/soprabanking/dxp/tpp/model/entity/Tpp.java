package com.soprabanking.dxp.tpp.model.entity;

import com.soprabanking.dxp.commons.mongodb.MongoAuditable;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Business representation of a TPP
 */
@Document
public class Tpp extends MongoAuditable {

    @Id
    private ObjectId id;

    @Indexed
    private String name;

    private String comments;

    private String webSite;

    private TppState state;

    private TppContact contact;

    private List<TppPhysicalContact> physicalContacts = new ArrayList<>();

    private Set<TppAccreditation> accreditations = new HashSet<>();

    @Indexed
    private TppNationalAccreditation nationalAccreditationData;

    private TppCredentials credentials;

    @Override
    public ObjectId getId() {
        return id;
    }

    @Override
    public void setId(ObjectId id) {
        this.id = id;
    }

    @Override
    public Tpp withId(ObjectId id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Tpp setName(String name) {
        this.name = name;
        return this;
    }

    public String getComments() {
        return comments;
    }

    public Tpp setComments(String comments) {
        this.comments = comments;
        return this;
    }

    public TppState getState() {
        return state;
    }

    public Tpp setState(TppState state) {
        this.state = state;
        return this;
    }

    public TppContact getContact() {
        return contact;
    }

    public Tpp setContact(TppContact contact) {
        this.contact = contact;
        return this;
    }

    public TppCredentials getCredentials() {
        return credentials;
    }

    public Tpp setCredentials(TppCredentials credentials) {
        this.credentials = credentials;
        return this;
    }

    public Set<TppAccreditation> getAccreditations() {
        return accreditations;
    }

    public Tpp setAccreditations(Set<TppAccreditation> accreditations) {
        this.accreditations = accreditations;
        return this;
    }

    public TppNationalAccreditation getNationalAccreditationData() {
        return nationalAccreditationData;
    }

    public Tpp setNationalAccreditationData(TppNationalAccreditation nationalAccreditationData) {
        this.nationalAccreditationData = nationalAccreditationData;
        return this;
    }

    public List<TppPhysicalContact> getPhysicalContacts() {
        return physicalContacts;
    }

    public Tpp setPhysicalContacts(List<TppPhysicalContact> physicalContacts) {
        this.physicalContacts = physicalContacts;
        return this;
    }

    public String getWebSite() {
        return webSite;
    }

    public Tpp setWebSite(String webSite) {
        this.webSite = webSite;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("name", name)
                .append("state", state)
                .append("accreditations", accreditations)
                .toString();
    }
}
