package com.soprabanking.dxp.tpp.model.entity;

/**
 * Status of the TPP in the Competent Authority Register
 */
public enum CompetentAuthorityStatus {
    AUTHORISED,
    WITHDRAWN
}
