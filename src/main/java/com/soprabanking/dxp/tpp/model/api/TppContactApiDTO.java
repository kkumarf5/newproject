package com.soprabanking.dxp.tpp.model.api;

import com.fasterxml.jackson.annotation.JsonView;
import com.soprabanking.dxp.tpp.model.api.TppViews.Basic;

import java.util.ArrayList;
import java.util.List;

import static com.soprabanking.dxp.tpp.model.api.TppViews.ContactAndName;

/**
 * API representation of a contact related to a TPP
 */
public class TppContactApiDTO {

    @JsonView({Basic.class, ContactAndName.class})
    private String email;

    @JsonView({Basic.class, ContactAndName.class})
    private String phoneNumber;

    @JsonView({Basic.class, ContactAndName.class})
    private String city;

    @JsonView({Basic.class, ContactAndName.class})
    private List<String> addressLines = new ArrayList<>();

    @JsonView({Basic.class, ContactAndName.class})
    private String postCode;

    @JsonView({Basic.class, ContactAndName.class})
    private String country;

    public String getEmail() {
        return email;
    }

    public TppContactApiDTO setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public TppContactApiDTO setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    public String getCity() {
        return city;
    }

    public TppContactApiDTO setCity(String city) {
        this.city = city;
        return this;
    }

    public List<String> getAddressLines() {
        return addressLines;
    }

    public TppContactApiDTO setAddressLines(List<String> addressLines) {
        this.addressLines = addressLines;
        return this;
    }

    public String getPostCode() {
        return postCode;
    }

    public TppContactApiDTO setPostCode(String postCode) {
        this.postCode = postCode;
        return this;
    }

    public String getCountry() {
        return country;
    }

    public TppContactApiDTO setCountry(String country) {
        this.country = country;
        return this;
    }
}
