package com.soprabanking.dxp.tpp.model.entity;

/**
 * Enumeration of TPP statuses
 */
public enum TppStatus {
    ACTIVE,
    BLOCKED
}
