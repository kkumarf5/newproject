package com.soprabanking.dxp.tpp.model.entity;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Contact information related to TPP
 */
public class TppContact implements Serializable {

    private String email;

    private String phoneNumber;

    private List<String> addressLines = new ArrayList<>();

    private String postCode;

    private String country;

    private String city;

    public String getEmail() {
        return email;
    }

    public TppContact setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public TppContact setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    public List<String> getAddressLines() {
        return addressLines;
    }

    public TppContact setAddressLines(List<String> addressLines) {
        this.addressLines = addressLines;
        return this;
    }

    public String getPostCode() {
        return postCode;
    }

    public TppContact setPostCode(String postCode) {
        this.postCode = postCode;
        return this;
    }

    public String getCountry() {
        return country;
    }

    public TppContact setCountry(String country) {
        this.country = country;
        return this;
    }

    public String getCity() {
        return city;
    }

    public TppContact setCity(String city) {
        this.city = city;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("email", email)
                .append("phoneNumber", phoneNumber)
                .toString();
    }
}
