package com.soprabanking.dxp.tpp.model.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * OAuth2 credentials related to a TPP
 */
public class TppCredentials implements Serializable {

    private String clientId;

    private String secret;

    private Set<String> trustedRedirectUrls = new HashSet<>();

    private Set<String> scopes = new HashSet<>();

    public String getClientId() {
        return clientId;
    }

    public TppCredentials setClientId(String clientId) {
        this.clientId = clientId;
        return this;
    }

    public String getSecret() {
        return secret;
    }

    public TppCredentials setSecret(String secret) {
        this.secret = secret;
        return this;
    }

    public Set<String> getTrustedRedirectUrls() {
        return trustedRedirectUrls;
    }

    public TppCredentials setTrustedRedirectUrls(Set<String> trustedRedirectUrls) {
        this.trustedRedirectUrls = trustedRedirectUrls;
        return this;
    }

    public Set<String> getScopes() {
        return scopes;
    }

    public TppCredentials setScopes(Set<String> scopes) {
        this.scopes = scopes;
        return this;
    }
}
