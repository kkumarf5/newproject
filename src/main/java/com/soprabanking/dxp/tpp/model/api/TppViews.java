package com.soprabanking.dxp.tpp.model.api;

import com.fasterxml.jackson.annotation.JsonView;

/**
 * {@link JsonView} related to TPPs
 */
public interface TppViews {

    interface Basic {

    }

    interface WithCredentials extends Basic {

    }

    interface ContactAndName {

    }
}
