package com.soprabanking.dxp.tpp.model.entity;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.data.mongodb.core.index.Indexed;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

public class TppNationalAccreditation implements Serializable {

    @Indexed
    private String organizationIdentifier;

    private String competentAuthorityIdentifier;

    private String competentAuthorityName;

    private String competentAuthorityCountryCode;

    private String authorizationNumber;

    private CompetentAuthorityStatus competentAuthorityStatus;

    private LocalDate registrationDate;

    private Set<TppAccreditation> accreditations = new HashSet<>();

    public String getOrganizationIdentifier() {
        return organizationIdentifier;
    }

    public TppNationalAccreditation setOrganizationIdentifier(String organizationIdentifier) {
        this.organizationIdentifier = organizationIdentifier;
        return this;
    }

    public String getCompetentAuthorityIdentifier() {
        return competentAuthorityIdentifier;
    }

    public TppNationalAccreditation setCompetentAuthorityIdentifier(String competentAuthorityIdentifier) {
        this.competentAuthorityIdentifier = competentAuthorityIdentifier;
        return this;
    }

    public String getCompetentAuthorityName() {
        return competentAuthorityName;
    }

    public TppNationalAccreditation setCompetentAuthorityName(String competentAuthorityName) {
        this.competentAuthorityName = competentAuthorityName;
        return this;
    }

    public String getCompetentAuthorityCountryCode() {
        return competentAuthorityCountryCode;
    }

    public TppNationalAccreditation setCompetentAuthorityCountryCode(String competentAuthorityCountryCode) {
        this.competentAuthorityCountryCode = competentAuthorityCountryCode;
        return this;
    }

    public String getAuthorizationNumber() {
        return authorizationNumber;
    }

    public TppNationalAccreditation setAuthorizationNumber(String authorizationNumber) {
        this.authorizationNumber = authorizationNumber;
        return this;
    }

    public CompetentAuthorityStatus getCompetentAuthorityStatus() {
        return competentAuthorityStatus;
    }

    public TppNationalAccreditation setCompetentAuthorityStatus(CompetentAuthorityStatus competentAuthorityStatus) {
        this.competentAuthorityStatus = competentAuthorityStatus;
        return this;
    }

    public LocalDate getRegistrationDate() {
        return registrationDate;
    }

    public TppNationalAccreditation setRegistrationDate(LocalDate registrationDate) {
        this.registrationDate = registrationDate;
        return this;
    }

    public Set<TppAccreditation> getAccreditations() {
        return accreditations;
    }

    public TppNationalAccreditation setAccreditations(Set<TppAccreditation> accreditations) {
        this.accreditations = accreditations;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("organizationIdentifier", organizationIdentifier)
                .append("competentAuthorityIdentifier", competentAuthorityIdentifier)
                .append("competentAuthorityCountryCode", competentAuthorityCountryCode)
                .append("authorizationNumber", authorizationNumber)
                .toString();
    }
}
