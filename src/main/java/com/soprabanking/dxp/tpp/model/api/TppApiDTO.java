package com.soprabanking.dxp.tpp.model.api;

import com.fasterxml.jackson.annotation.JsonView;
import com.soprabanking.dxp.commons.constants.IdentifiableDTO;
import com.soprabanking.dxp.tpp.model.entity.TppAccreditation;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import static com.soprabanking.dxp.tpp.model.api.TppGroups.Creation;
import static com.soprabanking.dxp.tpp.model.api.TppGroups.Update;
import static com.soprabanking.dxp.tpp.model.api.TppViews.Basic;
import static com.soprabanking.dxp.tpp.model.api.TppViews.ContactAndName;

/**
 * API representation of a TPP
 */
public class TppApiDTO implements IdentifiableDTO {

    @Null(groups = Creation.class)
    @JsonView({Basic.class, ContactAndName.class})
    private String id;

    @NotBlank(groups = {Creation.class, Update.class})
    @JsonView({Basic.class, ContactAndName.class})
    private String name;

    @JsonView(Basic.class)
    private String comments;

    @JsonView(Basic.class)
    private String webSite;

    @Valid
    @NotNull(groups = {Creation.class, Update.class})
    @JsonView(Basic.class)
    private TppStateApiDTO state;

    @JsonView({Basic.class, ContactAndName.class})
    private TppContactApiDTO contact;

    @JsonView(Basic.class)
    private List<TppPhysicalContactApiDTO> physicalContacts = new ArrayList<>();

    @JsonView(Basic.class)
    private Set<TppAccreditation> accreditations = new HashSet<>();

    @Valid
    @NotNull(groups = {Creation.class, Update.class})
    @JsonView(Basic.class)
    private TppNationalAccreditationApiDTO nationalAccreditationData;

    @Valid
    @JsonView(Basic.class)
    private TppCredentialApiDTO credentials;

    public TppNationalAccreditationApiDTO getNationalAccreditationData() {
        return nationalAccreditationData;
    }

    public TppApiDTO setNationalAccreditationData(TppNationalAccreditationApiDTO nationalAccreditationData) {
        this.nationalAccreditationData = nationalAccreditationData;
        return this;
    }

    public List<TppPhysicalContactApiDTO> getPhysicalContacts() {
        return physicalContacts;
    }

    public TppApiDTO setPhysicalContacts(List<TppPhysicalContactApiDTO> physicalContacts) {
        this.physicalContacts = physicalContacts;
        return this;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public TppApiDTO withId(String id) {
        setId(id);
        return this;
    }

    public String getName() {
        return name;
    }

    public TppApiDTO setName(String name) {
        this.name = name;
        return this;
    }

    public String getComments() {
        return comments;
    }

    public TppApiDTO setComments(String comments) {
        this.comments = comments;
        return this;
    }

    public TppContactApiDTO getContact() {
        return contact;
    }

    public TppApiDTO setContact(TppContactApiDTO contact) {
        this.contact = contact;
        return this;
    }

    public TppStateApiDTO getState() {
        return state;
    }

    public TppApiDTO setState(TppStateApiDTO state) {
        this.state = state;
        return this;
    }

    public TppCredentialApiDTO getCredentials() {
        return credentials;
    }

    public TppApiDTO setCredentials(TppCredentialApiDTO credentials) {
        this.credentials = credentials;
        return this;
    }

    public Set<TppAccreditation> getAccreditations() {
        return accreditations;
    }

    public TppApiDTO setAccreditations(Set<TppAccreditation> accreditations) {
        this.accreditations = accreditations;
        return this;
    }

    public String getWebSite() {
        return webSite;
    }

    public TppApiDTO setWebSite(String webSite) {
        this.webSite = webSite;
        return this;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TppApiDTO)) {
            return false;
        }
        TppApiDTO tppApiDTO = (TppApiDTO) o;
        return Objects.equals(id, tppApiDTO.id);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("name", name)
                .append("state", state)
                .toString();
    }
}
