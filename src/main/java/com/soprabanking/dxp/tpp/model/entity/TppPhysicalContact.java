package com.soprabanking.dxp.tpp.model.entity;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

public class TppPhysicalContact implements Serializable {

    private TppContactType contactType;

    private String name;

    private String firstName;

    private String jobTitle;

    private String email;

    private String phoneNumber;

    public TppContactType getContactType() {
        return contactType;
    }

    public TppPhysicalContact setContactType(TppContactType contactType) {
        this.contactType = contactType;
        return this;
    }

    public String getName() {
        return name;
    }

    public TppPhysicalContact setName(String name) {
        this.name = name;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public TppPhysicalContact setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public TppPhysicalContact setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public TppPhysicalContact setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public TppPhysicalContact setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("contactType", contactType)
                .append("name", name)
                .append("firstName", firstName)
                .append("email", email)
                .toString();
    }
}
