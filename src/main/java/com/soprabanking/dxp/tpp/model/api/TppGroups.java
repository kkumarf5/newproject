package com.soprabanking.dxp.tpp.model.api;

/**
 * Validation groups
 */
public interface TppGroups {

    interface Creation {

    }

    interface Update {

    }

    interface Patch {

    }
}
