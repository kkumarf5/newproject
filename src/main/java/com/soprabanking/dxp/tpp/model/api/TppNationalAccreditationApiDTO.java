package com.soprabanking.dxp.tpp.model.api;

import com.fasterxml.jackson.annotation.JsonView;
import com.soprabanking.dxp.tpp.model.entity.CompetentAuthorityStatus;
import com.soprabanking.dxp.tpp.model.entity.TppAccreditation;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import static com.soprabanking.dxp.tpp.model.api.TppGroups.Creation;
import static com.soprabanking.dxp.tpp.model.api.TppGroups.Patch;
import static com.soprabanking.dxp.tpp.model.api.TppGroups.Update;
import static com.soprabanking.dxp.tpp.model.api.TppViews.Basic;

public class TppNationalAccreditationApiDTO {

    @NotBlank(groups = {Creation.class, Update.class, Patch.class})
    @JsonView(Basic.class)
    private String organizationIdentifier;

    @NotBlank(groups = {Creation.class, Update.class, Patch.class})
    @JsonView(Basic.class)
    private String competentAuthorityIdentifier;

    @NotBlank(groups = {Creation.class, Update.class, Patch.class})
    @JsonView(Basic.class)
    private String competentAuthorityName;

    @NotBlank(groups = {Creation.class, Update.class, Patch.class})
    @JsonView(Basic.class)
    private String competentAuthorityCountryCode;

    @NotBlank(groups = {Creation.class, Update.class, Patch.class})
    @JsonView(Basic.class)
    private String authorizationNumber;

    @NotNull(groups = {Creation.class, Update.class, Patch.class})
    @JsonView(Basic.class)
    private CompetentAuthorityStatus competentAuthorityStatus;

    @JsonView(Basic.class)
    private LocalDate registrationDate;

    @JsonView(Basic.class)
    private Set<TppAccreditation> accreditations = new HashSet<>();

    public String getOrganizationIdentifier() {
        return organizationIdentifier;
    }

    public TppNationalAccreditationApiDTO setOrganizationIdentifier(String organizationIdentifier) {
        this.organizationIdentifier = organizationIdentifier;
        return this;
    }

    public String getCompetentAuthorityIdentifier() {
        return competentAuthorityIdentifier;
    }

    public TppNationalAccreditationApiDTO setCompetentAuthorityIdentifier(String competentAuthorityIdentifier) {
        this.competentAuthorityIdentifier = competentAuthorityIdentifier;
        return this;
    }

    public String getCompetentAuthorityName() {
        return competentAuthorityName;
    }

    public TppNationalAccreditationApiDTO setCompetentAuthorityName(String competentAuthorityName) {
        this.competentAuthorityName = competentAuthorityName;
        return this;
    }

    public String getCompetentAuthorityCountryCode() {
        return competentAuthorityCountryCode;
    }

    public TppNationalAccreditationApiDTO setCompetentAuthorityCountryCode(String competentAuthorityCountryCode) {
        this.competentAuthorityCountryCode = competentAuthorityCountryCode;
        return this;
    }

    public String getAuthorizationNumber() {
        return authorizationNumber;
    }

    public TppNationalAccreditationApiDTO setAuthorizationNumber(String authorizationNumber) {
        this.authorizationNumber = authorizationNumber;
        return this;
    }

    public CompetentAuthorityStatus getCompetentAuthorityStatus() {
        return competentAuthorityStatus;
    }

    public TppNationalAccreditationApiDTO setCompetentAuthorityStatus(CompetentAuthorityStatus competentAuthorityStatus) {
        this.competentAuthorityStatus = competentAuthorityStatus;
        return this;
    }

    public LocalDate getRegistrationDate() {
        return registrationDate;
    }

    public TppNationalAccreditationApiDTO setRegistrationDate(LocalDate registrationDate) {
        this.registrationDate = registrationDate;
        return this;
    }

    public Set<TppAccreditation> getAccreditations() {
        return accreditations;
    }

    public TppNationalAccreditationApiDTO setAccreditations(Set<TppAccreditation> accreditations) {
        this.accreditations = accreditations;
        return this;
    }
}
