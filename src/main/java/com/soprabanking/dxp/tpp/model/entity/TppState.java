package com.soprabanking.dxp.tpp.model.entity;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

/**
 * Tpp state information
 */
public class TppState implements Serializable {

    private TppStatus status;

    private boolean monitored;

    public TppStatus getStatus() {
        return status;
    }

    public TppState setStatus(TppStatus status) {
        this.status = status;
        return this;
    }

    public boolean isMonitored() {
        return monitored;
    }

    public TppState setMonitored(boolean monitored) {
        this.monitored = monitored;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("status", status)
                .append("monitored", monitored)
                .toString();
    }
}