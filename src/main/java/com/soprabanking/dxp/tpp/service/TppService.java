package com.soprabanking.dxp.tpp.service;

import com.soprabanking.dxp.commons.cache.SimpleDxpCache;
import com.soprabanking.dxp.commons.error.DxpSbsFeedback;
import com.soprabanking.dxp.commons.page.Range;
import com.soprabanking.dxp.tpp.messaging.TppMessaging;
import com.soprabanking.dxp.tpp.model.entity.Tpp;
import com.soprabanking.dxp.tpp.model.entity.TppAccreditation;
import com.soprabanking.dxp.tpp.model.entity.TppCredentials;
import com.soprabanking.dxp.tpp.model.entity.TppState;
import com.soprabanking.dxp.tpp.repository.TppRepository;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.function.Function;
import java.util.regex.Pattern;

import static com.soprabanking.dxp.commons.cache.CacheExtensionsKt.cacheOrError;
import static com.soprabanking.dxp.commons.error.DxpBusinessCode.INVALID_CONTENT;
import static com.soprabanking.dxp.commons.error.DxpBusinessCode.RESOURCE_ALREADY_EXIST;
import static com.soprabanking.dxp.commons.error.DxpBusinessCode.RESOURCE_NOT_FOUND;
import static com.soprabanking.dxp.tpp.mapper.TppMapper.extractOrganizationIdentifier;
import static com.soprabanking.dxp.tpp.mapper.TppMapper.merge;
import static com.soprabanking.dxp.tpp.service.CredentialsHelper.generateCredentials;
import static java.lang.String.format;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * Business logic related to {@link Tpp} entity
 */
@Service
public class TppService {

    private static final Logger LOGGER = getLogger(TppService.class);

    private final TppRepository tppRepository;

    private final TppMessaging tppMessaging;

    private final SimpleDxpCache<String, Tpp> cacheId;

    private final SimpleDxpCache<String, Tpp> cacheClientId;

    public TppService(TppRepository tppRepository, TppMessaging tppMessaging, CacheManager cacheManager) {
        this.tppRepository = tppRepository;
        this.tppMessaging = tppMessaging;
        cacheId = new SimpleDxpCache<>(cacheOrError(cacheManager, "tppsById"), t -> String.valueOf(t.getId()));
        cacheClientId = new SimpleDxpCache<>(cacheOrError(cacheManager, "tppsByClientId"), t -> t.getCredentials().getClientId());
    }

    public Flux<Tpp> findAll(Range range) {
        return tppRepository.findAll(range)
                            .doOnSubscribe(o -> LOGGER.debug("Searching TPPs from {}", range))
                            .doOnComplete(() -> LOGGER.debug("TPPs within {} retrieved", range));
    }

    public Mono<Tpp> findById(ObjectId id) {
        return cacheId.switchIfEmpty(id.toString(), this::findByIdFromRepository)
                      .switchIfEmpty(idNotFoundError(id));
    }

    public Flux<TppAccreditation> findAccreditations(ObjectId id) {
        return findById(id)
                .map(Tpp::getAccreditations)
                .doOnNext(a -> LOGGER.debug("Accreditations {} retrieved with TPP id {}", a, id))
                .flatMapIterable(Function.identity());
    }

    public Mono<Tpp> create(Tpp tpp) {
        return Mono.just(tpp)
                   .doOnSubscribe(t -> LOGGER.info("Creating {}", tpp))
                   .map(t -> t.setCredentials(generateCredentials(t)))
                   .flatMap(this::validateTpp)
                   .flatMap(tppRepository::insert)
                   .doOnNext(t -> LOGGER.info("{} successfully created", t))
                   .flatMap(tppMessaging::sendCreation)
                   .flatMap(this::storeInCache);
    }

    public Mono<Tpp> patch(ObjectId id, Tpp newTpp) {
        return findById(id)
                .doOnSubscribe(t -> LOGGER.debug("Patching TPP fields {}", newTpp))
                .flatMap(oldTpp -> update(merge(newTpp, oldTpp), oldTpp))
                .doOnNext(t -> LOGGER.info("{} successfully patched", t));
    }

    public Mono<Tpp> update(ObjectId id, Tpp newTpp) {
        return findById(id)
                .doOnSubscribe(t -> LOGGER.debug("Updating {}", newTpp))
                .flatMap(oldTpp -> update(newTpp.withId(id), oldTpp))
                .doOnNext(t -> LOGGER.info("{} successfully updated", t));
    }

    public Mono<TppState> findState(ObjectId id) {
        return findById(id)
                .map(Tpp::getState)
                .doOnNext(s -> LOGGER.debug("Status {} retrieved for TPP id {}", s, id));
    }

    public Mono<Tpp> findByClientId(String clientId) {
        return cacheClientId.switchIfEmpty(clientId, this::findByClientIdFromRepository);
    }

    public Flux<Tpp> findByNameContaining(String name, Range range) {
        return tppRepository.findByNameIgnoreCaseContaining(name, range)
                            .doOnSubscribe(c -> LOGGER.debug("Searching TPPs with name containing {}", name))
                            .doOnNext(t -> LOGGER.debug("{} retrieved by name", t));
    }

    public Mono<Tpp> findByOrganizationIdentifier(String organizationIdentifier) {
        return tppRepository.findByNationalAccreditationDataOrganizationIdentifier(organizationIdentifier)
                            .doOnSubscribe(t -> LOGGER.debug("Searching TPP with organizationIdentifier {}", organizationIdentifier))
                            .doOnNext(t -> LOGGER.debug("{} retrieved using organizationIdentifier {}", t, organizationIdentifier))
                            .switchIfEmpty(noTppWithOrganizationIdentifierError(organizationIdentifier));
    }

    public Flux<Tpp> findByIds(List<ObjectId> ids) {
        return tppRepository.findAllById(ids)
                            .doOnSubscribe(s -> LOGGER.debug("Searching all TPPs with id matching {}", ids));
    }

    private Mono<Tpp> findByIdFromRepository(String id) {
        return tppRepository.findById(new ObjectId(id))
                            .doOnSubscribe(o -> LOGGER.debug("Searching TPP with id {}", id))
                            .doOnNext(t -> LOGGER.debug("{} retrieved by id", t));
    }

    private Mono<Tpp> findByClientIdFromRepository(String clientId) {
        return tppRepository.findByCredentials_ClientIdIgnoreCase(clientId)
                            .doOnSubscribe(t -> LOGGER.debug("Searching TPP with clientId {}", clientId))
                            .doOnNext(t -> LOGGER.debug("{} retrieved using clientId {}", t, clientId));
    }

    private Mono<Tpp> update(Tpp toUpdate, Tpp old) {
        return validateTpp(copyCredentials(toUpdate, old))
                .flatMap(b -> tppRepository.save(toUpdate))
                .doOnNext(t -> LOGGER.info("{} successfully updated", t))
                .flatMap(t -> tppMessaging.sendUpdate(t, toUpdate))
                .flatMap(this::storeInCache);
    }

    private Mono<Tpp> storeInCache(Tpp tpp) {
        return Mono.just(tpp)
                   .doOnSubscribe(t -> LOGGER.debug("Add following TPP in cache {}", tpp))
                   .flatMap(cacheId::store)
                   .flatMap(cacheClientId::store)
                   .doOnNext(t -> LOGGER.debug("Tpp {} stored in cache", tpp));
    }

    private Tpp copyCredentials(Tpp toUpdate, Tpp old) {
        TppCredentials oldCredentials = old.getCredentials();
        return toUpdate.setCredentials(oldCredentials.setTrustedRedirectUrls(toUpdate.getCredentials() != null ?
                                                                                     toUpdate.getCredentials().getTrustedRedirectUrls() :
                                                                                     oldCredentials.getTrustedRedirectUrls()));

    }

    private Mono<Tpp> validateTpp(Tpp tpp) {
        return checkOrganizationIdentifierAvailable(tpp)
                .map(this::checkOrganizationIdentifierStructure)
                .map(this::checkAccreditations)
                .flatMap(this::checkNameAvailable)
                .flatMap(this::checkCredentials);
    }

    private Mono<Tpp> checkOrganizationIdentifierAvailable(Tpp tpp) {
        return findByOrganizationIdentifier(extractOrganizationIdentifier(tpp))
                .filter(t -> tpp.getId() == null || !tpp.getId().equals(t.getId()))
                .flatMap(t -> errorOrganizationIdentifierTaken(extractOrganizationIdentifier(tpp), t))
                .switchIfEmpty(Mono.just(tpp));
    }

    private Tpp checkOrganizationIdentifierStructure(Tpp tpp) {
        boolean competentAuthorityCountryCodeOk = Arrays.stream(Locale.getISOCountries())
                                                        .map(String::toUpperCase)
                                                        .anyMatch(code -> code.equals(tpp.getNationalAccreditationData()
                                                                                         .getCompetentAuthorityCountryCode()));
        boolean competentAuthorityIdentifierOk = Pattern.matches("[A-Z]{2,8}",
                                                                 tpp.getNationalAccreditationData().getCompetentAuthorityIdentifier());
        if (!competentAuthorityCountryCodeOk || !competentAuthorityIdentifierOk) {
            return errorBadCompetentAuthority(tpp);
        }
        return buildTppOrganizationIdentifier(tpp)
                .equals(tpp.getNationalAccreditationData().getOrganizationIdentifier()) ? tpp : errorBadCompetentAuthority(tpp);
    }

    private String buildTppOrganizationIdentifier(Tpp tpp) {
        return String.join("-",
                           "PSD" + tpp.getNationalAccreditationData().getCompetentAuthorityCountryCode(),
                           tpp.getNationalAccreditationData().getCompetentAuthorityIdentifier(),
                           tpp.getNationalAccreditationData().getAuthorizationNumber());
    }

    private Tpp checkAccreditations(Tpp tpp) {
        return tpp.getNationalAccreditationData()
                  .getAccreditations()
                  .containsAll(tpp.getAccreditations()) ? tpp : errorBankAccreditationNotInNationalAccreditations(tpp);
    }

    private Mono<Tpp> checkNameAvailable(Tpp tpp) {
        return findByName(tpp.getName())
                .filter(t -> tpp.getId() == null || !tpp.getId().equals(t.getId()))
                .flatMap(t -> errorNameTaken(tpp.getName(), t))
                .switchIfEmpty(Mono.just(tpp));
    }

    private Mono<Tpp> findByName(String name) {
        return tppRepository.findByNameIgnoreCase(name)
                            .doOnSubscribe(t -> LOGGER.debug("Searching TPP with name {}", name))
                            .doOnNext(t -> LOGGER.debug("{} retrieved using name {}", t, name))
                            .switchIfEmpty(noTppWithNameError(name));
    }

    private Mono<Tpp> checkCredentials(Tpp tpp) {
        return findByClientId(tpp.getCredentials().getClientId())
                .filter(t -> tpp.getId() == null || !tpp.getId().equals(t.getId()))
                .flatMap(t -> errorClientIdTaken(tpp.getName(), t))
                .switchIfEmpty(Mono.just(tpp));
    }

    private Mono<Tpp> noTppWithOrganizationIdentifierError(String organizationIdentifier) {
        return Mono.<Tpp>empty().doOnTerminate(
                () -> LOGGER.debug("No TPP matching organizationIdentifier {} found", organizationIdentifier));
    }

    private Mono<Tpp> noTppWithNameError(String name) {
        return Mono.<Tpp>empty().doOnTerminate(() -> LOGGER.debug("No TPP matching name {} found", name));
    }

    private Mono<Tpp> errorOrganizationIdentifierTaken(String ebaId, Tpp tpp) {
        return Mono.error(
                new DxpSbsFeedback(format("Organization Identifier %s is already taken by %s", ebaId, tpp), RESOURCE_ALREADY_EXIST,
                                   LOGGER::info).toException());
    }

    private Mono<Tpp> errorNameTaken(String name, Tpp tpp) {
        return Mono.error(new DxpSbsFeedback(format("Name %s is already taken by %s", name, tpp), RESOURCE_ALREADY_EXIST,
                                             LOGGER::info).toException());
    }

    private Mono<Tpp> errorClientIdTaken(String clientId, Tpp tpp) {
        return Mono.error(new DxpSbsFeedback(format("ClientId %s is already taken by %s", clientId, tpp), RESOURCE_ALREADY_EXIST,
                                             LOGGER::info).toException());
    }

    private Tpp errorBankAccreditationNotInNationalAccreditations(Tpp tpp) {
        String msg = format("Tpp accreditations %s does not match national accreditations %s",
                            tpp.getAccreditations(),
                            tpp.getNationalAccreditationData().getAccreditations());
        throw new DxpSbsFeedback(msg, INVALID_CONTENT, LOGGER::info).toException();
    }

    private Tpp errorBadCompetentAuthority(Tpp tpp) {
        String exceptionMsg =
                format("Malformed competent authority data: %s Should match : " +
                       "PSD{competentAuthorityCountryCode}-{competentAuthorityIdentifier}-{authorizationNumber}",
                       tpp.getNationalAccreditationData());
        throw new DxpSbsFeedback.Builder(exceptionMsg, INVALID_CONTENT, LOGGER::info).withSource(
                "nationalAccreditationData.organizationIdentifier").build().toException();
    }

    private Mono<Tpp> idNotFoundError(ObjectId id) {
        String message = format("Cannot find tpp with id %s", id);
        return Mono.error(new DxpSbsFeedback(message, RESOURCE_NOT_FOUND, LOGGER::info).toException());
    }
}