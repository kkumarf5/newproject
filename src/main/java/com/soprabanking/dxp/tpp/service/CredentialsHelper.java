package com.soprabanking.dxp.tpp.service;

import com.soprabanking.dxp.tpp.model.entity.Tpp;
import com.soprabanking.dxp.tpp.model.entity.TppCredentials;
import org.slf4j.Logger;

import java.util.HashSet;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;
import static org.apache.commons.lang3.StringUtils.deleteWhitespace;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.apache.commons.lang3.StringUtils.stripAccents;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * Business logic related to credentials
 */
final class CredentialsHelper {

    private static final Logger LOGGER = getLogger(CredentialsHelper.class);

    private CredentialsHelper() {
    }

    static TppCredentials generateCredentials(Tpp tpp) {
        String clientId = tpp.getNationalAccreditationData().getOrganizationIdentifier();
        LOGGER.info("Generating {} credentials", clientId);
        return new TppCredentials().setClientId(normalizeClientId(clientId))
                                   .setSecret(generateSecret())
                                   .setTrustedRedirectUrls(
                                           tpp.getCredentials() != null ? tpp.getCredentials().getTrustedRedirectUrls() : new HashSet<>());
    }

    private static String normalizeClientId(String organizationIdentifier) {
        String noWhiteSpace = deleteWhitespace(organizationIdentifier);
        String noAccents = stripAccents(noWhiteSpace);
        String normalized = noAccents.replaceAll("[^a-zA-Z0-9-_]", "");
        return defaultIfEmpty(normalized);
    }

    private static String defaultIfEmpty(String clientId) {
        return isEmpty(clientId) ? randomAlphabetic(6) : clientId;
    }

    private static String generateSecret() {
        return randomAlphanumeric(6);
    }
}