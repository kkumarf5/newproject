package com.soprabanking.dxp.tpp;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import static com.soprabanking.dxp.commons.constants.DxpConstants.BASE_PACKAGE;
import static org.springframework.boot.SpringApplication.run;

/**
 * Class starting service tpp management application
 */
@SpringBootApplication(scanBasePackages = BASE_PACKAGE)
public class ServiceTppManagementApplication {

    public static void main(String[] args) {
        run(ServiceTppManagementApplication.class, args);
    }
}