package com.soprabanking.dxp.tpp.resource;

/**
 * URL constants
 */
final class UrlConstants {

    static final String ID = "/{id}";

    static final String CREDENTIALS = "/credentials";

    static final String ACCREDITATIONS = "/accreditations";

    static final String STATE = "/state";

    static final String CLIENT_ID = "clientId";

    static final String ORGANIZATION_IDENTIFIER = "organizationIdentifier";

    static final String NAME = "name";

    static final String ID_LIST = "ids";

    private static final String API = "/api";

    static final String TPPS = API + "/tpps";

    private UrlConstants() {
    }

}
