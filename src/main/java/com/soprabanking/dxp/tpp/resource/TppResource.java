package com.soprabanking.dxp.tpp.resource;

import com.fasterxml.jackson.annotation.JsonView;
import com.soprabanking.dxp.commons.mongodb.ObjectIdExtensions;
import com.soprabanking.dxp.commons.page.Range;
import com.soprabanking.dxp.commons.psd2.security.bankuser.model.BankUser;
import com.soprabanking.dxp.commons.psd2.security.psubybank.model.PsuByBankUser;
import com.soprabanking.dxp.commons.psd2.security.tpp.model.TppUser;
import com.soprabanking.dxp.commons.security.TypeAllowed;
import com.soprabanking.dxp.commons.security.model.DxpTechnicalBusinessContext;
import com.soprabanking.dxp.tpp.mapper.TppMapper;
import com.soprabanking.dxp.tpp.mapper.TppStateMapper;
import com.soprabanking.dxp.tpp.model.api.TppApiDTO;
import com.soprabanking.dxp.tpp.model.api.TppStateApiDTO;
import com.soprabanking.dxp.tpp.model.entity.TppAccreditation;
import com.soprabanking.dxp.tpp.service.TppService;
import org.bson.types.ObjectId;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

import static com.soprabanking.dxp.commons.api.ApiValidationsKt.setSameId;
import static com.soprabanking.dxp.commons.api.RangeResponseKt.toResponse;
import static com.soprabanking.dxp.commons.psd2.security.tpp.TppValidationsKt.checkSameTppId;
import static com.soprabanking.dxp.commons.security.ReactiveSecurityContextKt.currentUserOfType;
import static com.soprabanking.dxp.tpp.mapper.TppMapper.map;
import static com.soprabanking.dxp.tpp.model.api.TppGroups.Creation;
import static com.soprabanking.dxp.tpp.model.api.TppGroups.Patch;
import static com.soprabanking.dxp.tpp.model.api.TppGroups.Update;
import static com.soprabanking.dxp.tpp.model.api.TppViews.Basic;
import static com.soprabanking.dxp.tpp.model.api.TppViews.ContactAndName;
import static com.soprabanking.dxp.tpp.model.api.TppViews.WithCredentials;
import static com.soprabanking.dxp.tpp.resource.UrlConstants.ACCREDITATIONS;
import static com.soprabanking.dxp.tpp.resource.UrlConstants.CLIENT_ID;
import static com.soprabanking.dxp.tpp.resource.UrlConstants.ID;
import static com.soprabanking.dxp.tpp.resource.UrlConstants.ID_LIST;
import static com.soprabanking.dxp.tpp.resource.UrlConstants.NAME;
import static com.soprabanking.dxp.tpp.resource.UrlConstants.ORGANIZATION_IDENTIFIER;
import static com.soprabanking.dxp.tpp.resource.UrlConstants.STATE;
import static com.soprabanking.dxp.tpp.resource.UrlConstants.TPPS;
import static java.util.stream.Collectors.toList;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.OK;

/**
 * REST resource publishing TPP endpoint
 */
@RestController
@RequestMapping(TPPS)
public class TppResource {

    private final TppService tppService;

    public TppResource(TppService tppService) {
        this.tppService = tppService;
    }

    @GetMapping
    @JsonView(Basic.class)
    @TypeAllowed({BankUser.class, DxpTechnicalBusinessContext.class})
    public Mono<ResponseEntity<List<TppApiDTO>>> findAll(Range range) {
        return tppService.findAll(range)
                         .map(TppMapper::map)
                         .as(f -> toResponse(f, TppApiDTO.class, range));
    }

    @GetMapping(params = ID_LIST)
    @JsonView(ContactAndName.class)
    @TypeAllowed(PsuByBankUser.class)
    public Flux<TppApiDTO> findByIds(@RequestParam List<String> ids) {
        List<ObjectId> objectIds = ids.stream().map(ObjectIdExtensions::toObjectId).collect(toList());
        return tppService.findByIds(objectIds)
                         .map(TppMapper::map);
    }

    @GetMapping(ID)
    @JsonView(Basic.class)
    @TypeAllowed({BankUser.class, DxpTechnicalBusinessContext.class})
    public Mono<TppApiDTO> findById(@PathVariable String id) {
        return tppService.findById(new ObjectId(id)).map(TppMapper::map);
    }

    @GetMapping(params = CLIENT_ID)
    @JsonView(WithCredentials.class)
    @TypeAllowed(DxpTechnicalBusinessContext.class)
    public Mono<ResponseEntity<TppApiDTO>> findByClientId(@RequestParam String clientId) {
        return tppService.findByClientId(clientId)
                         .map(TppMapper::map)
                         .map(t -> new ResponseEntity<>(t, OK))
                         .defaultIfEmpty(new ResponseEntity<>(NO_CONTENT))
                         .single();
    }

    @GetMapping(params = ORGANIZATION_IDENTIFIER)
    @JsonView(Basic.class)
    @TypeAllowed({BankUser.class, DxpTechnicalBusinessContext.class})
    public Mono<TppApiDTO> findByOrganizationIdentifier(@RequestParam String organizationIdentifier) {
        return tppService.findByOrganizationIdentifier(organizationIdentifier).map(TppMapper::map);
    }

    @JsonView(Basic.class)
    @GetMapping(params = NAME)
    @TypeAllowed(BankUser.class)
    public Mono<ResponseEntity<List<TppApiDTO>>> findByName(@RequestParam String name, Range range) {
        return tppService.findByNameContaining(name, range)
                         .map(TppMapper::map)
                         .as(f -> toResponse(f, TppApiDTO.class, range));
    }

    @GetMapping(ID + STATE)
    @TypeAllowed(BankUser.class)
    public Mono<TppStateApiDTO> findState(@PathVariable String id) {
        return tppService.findState(new ObjectId(id))
                         .map(TppStateMapper::map);
    }

    @GetMapping(ID + ACCREDITATIONS)
    @TypeAllowed(BankUser.class)
    public Flux<TppAccreditation> findAccreditationsAsBank(@PathVariable String id) {
        return tppService.findAccreditations(new ObjectId(id));
    }

    @GetMapping(ID + ACCREDITATIONS)
    @TypeAllowed(TppUser.class)
    public Flux<TppAccreditation> findAccreditationsAsTpp(@PathVariable String id) {
        return currentUserOfType(TppUser.class)
                .map(t -> checkSameTppId(t, id))
                .flatMapMany(p -> tppService.findAccreditations(new ObjectId(id)));
    }

    @PostMapping
    @JsonView(Basic.class)
    @ResponseStatus(CREATED)
    @TypeAllowed(BankUser.class)
    public Mono<TppApiDTO> create(@RequestBody @Validated(Creation.class) TppApiDTO tpp) {
        return tppService.create(map(tpp)).map(TppMapper::map);
    }

    @PutMapping(ID)
    @JsonView(Basic.class)
    @TypeAllowed(BankUser.class)
    public Mono<TppApiDTO> update(@PathVariable String id, @RequestBody @Validated(Update.class) TppApiDTO tpp) {
        return tppService.update(new ObjectId(id), map(setSameId(tpp, id))).map(TppMapper::map);
    }

    @PatchMapping(ID)
    @JsonView(Basic.class)
    @TypeAllowed(BankUser.class)
    public Mono<TppApiDTO> patch(@PathVariable String id, @RequestBody @Validated(Patch.class) TppApiDTO tpp) {
        return tppService.patch(new ObjectId(id), map(setSameId(tpp, id))).map(TppMapper::map);
    }
}