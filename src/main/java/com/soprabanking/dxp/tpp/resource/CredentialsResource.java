package com.soprabanking.dxp.tpp.resource;

import com.soprabanking.dxp.commons.page.Range;
import com.soprabanking.dxp.commons.security.TypeAllowed;
import com.soprabanking.dxp.commons.security.model.DxpBusinessContext;
import com.soprabanking.dxp.tpp.mapper.TppCredentialsMapper;
import com.soprabanking.dxp.tpp.model.api.TppCredentialApiDTO;
import com.soprabanking.dxp.tpp.service.TppService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.List;

import static com.soprabanking.dxp.commons.api.RangeResponseKt.toResponse;
import static com.soprabanking.dxp.tpp.resource.UrlConstants.CREDENTIALS;
import static com.soprabanking.dxp.tpp.resource.UrlConstants.TPPS;

/**
 * REST resource publishing TPP credentials
 */
@RestController
@RequestMapping(TPPS)
public class CredentialsResource {

    private final TppService tppService;

    public CredentialsResource(TppService tppService) {
        this.tppService = tppService;
    }

    @GetMapping(CREDENTIALS)
    @TypeAllowed(DxpBusinessContext.class)
    public Mono<ResponseEntity<List<TppCredentialApiDTO>>> findAll(Range range) {
        return tppService.findAll(range)
                         .map(TppCredentialsMapper::map)
                         .as(f -> toResponse(f, TppCredentialApiDTO.class, range));
    }
}