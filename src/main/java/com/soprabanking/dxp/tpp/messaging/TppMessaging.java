package com.soprabanking.dxp.tpp.messaging;

import com.soprabanking.dxp.commons.data.CrudEventRollback;
import com.soprabanking.dxp.commons.kafka.DefaultKafkaHeaders;
import com.soprabanking.dxp.commons.kafka.DefaultKafkaPayload;
import com.soprabanking.dxp.commons.kafka.KafkaProducer;
import com.soprabanking.dxp.commons.kafka.VersionedKafkaPayload;
import com.soprabanking.dxp.commons.security.model.TokenDTOWrapper;
import com.soprabanking.dxp.tpp.model.entity.Tpp;
import com.soprabanking.dxp.tpp.repository.TppRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

import static com.soprabanking.dxp.commons.constants.CrudType.CREATE;
import static com.soprabanking.dxp.commons.constants.CrudType.UPDATE;
import static com.soprabanking.dxp.commons.security.ReactiveSecurityContextKt.justToken;
import static com.soprabanking.dxp.tpp.mapper.TppMapper.map;

/**
 * Message bus connection related to TPPs
 */
@Repository
public class TppMessaging {

    private static final Logger LOGGER = LoggerFactory.getLogger(TppMessaging.class);

    private static final String TPP_CREATE_TOPIC = "tpp-create";

    private static final String TPP_UPDATE_TOPIC = "tpp-update";

    private final KafkaProducer kafkaProducer;

    private final TppRepository tppRepository;

    public TppMessaging(KafkaProducer kafkaProducer, TppRepository tppRepository) {
        this.kafkaProducer = kafkaProducer;
        this.tppRepository = tppRepository;
    }

    public Mono<Tpp> sendCreation(Tpp tpp) {
        return justToken()
                .map(t -> new TokenDTOWrapper<>(t, map(tpp)))
                .flatMap(w -> kafkaProducer.send(TPP_CREATE_TOPIC,
                                                 new DefaultKafkaHeaders(CREATE, w.getToken(), w.getDto().getId()),
                                                 new DefaultKafkaPayload<>(w.getDto())))
                .doOnNext(r -> LOGGER.debug("TPP creation event sent {} with id {}", tpp, r.correlationMetadata()))
                .map(r -> tpp)
                .onErrorResume(e -> new CrudEventRollback<>(tppRepository).rollback(CREATE, tpp).flatMap(t -> Mono.error(e)));
    }

    public Mono<Tpp> sendUpdate(Tpp actualTpp, Tpp oldTpp) {
        return justToken()
                .map(t -> new TokenDTOWrapper<>(t, map(actualTpp)))
                .flatMap(w -> kafkaProducer.send(TPP_UPDATE_TOPIC,
                                                 new DefaultKafkaHeaders(UPDATE, w.getToken(), w.getDto().getId()),
                                                 new VersionedKafkaPayload<>(w.getDto(), map(oldTpp))))
                .doOnNext(r -> LOGGER.debug("TPP update event sent {} with id {}", actualTpp, r.correlationMetadata()))
                .map(r -> actualTpp);
    }
}