package com.soprabanking.dxp.tpp.repository;

import com.soprabanking.dxp.commons.mongodb.DxpReactiveMongoRepository;
import com.soprabanking.dxp.tpp.model.entity.Tpp;
import org.springframework.data.domain.Pageable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Repository managing {@link Tpp} entity
 */
public interface TppRepository extends DxpReactiveMongoRepository<Tpp> {

    Mono<Tpp> findByNationalAccreditationDataOrganizationIdentifier(String organizationIdentifier);

    Mono<Tpp> findByNameIgnoreCase(String name);

    Mono<Tpp> findByCredentials_ClientIdIgnoreCase(String clientId);

    Flux<Tpp> findByNameIgnoreCaseContaining(String name, Pageable pageable);
}