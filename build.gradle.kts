import com.soprabanking.dxp.*

val commonsPsd2Version by extra { "1.0.0-RC39" }
val commonsVersion by extra { "1.0.0-RC26" }

dxpJavaApplication()
dependencies {
    implementation(dxp("api"))
    implementation(dxp("cache"))
    implementation(dxp("error"))
    implementation(dxp("kafka"))
    implementation(dxp("mongodb"))
    implementation(psd2("security-bankuser"))
    implementation(psd2("security-psubytpp"))
    implementation(psd2("security-tpp"))
    runtimeOnly(dxp("config"))
    runtimeOnly(dxp("monitor"))
    testImplementation(dxp("test"))
    testImplementation(psd2("security-test"))
}

