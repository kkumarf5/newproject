# DxP Gradle Pipeline Test Project

The aim of this project is to validate gradle template provided by DxP CICD Team

[gitlab-ci-dxp-templates-builder](https://innersource.soprasteria.com/dxp/dxp-cicd/gitlab-ci-toolset/gitlab-ci-dxp-templates-builder) project status : [![pipeline status](https://innersource.soprasteria.com/dxp/dxp-cicd/gitlab-ci-toolset/gitlab-ci-dxp-templates-builder/badges/master/pipeline.svg)](https://innersource.soprasteria.com/dxp/dxp-cicd/gitlab-ci-toolset/gitlab-ci-dxp-templates-builder/commits/master)



# Service Tpp Management - v1
- [Service Tpp Management - v1](#service-tpp-management---v1)
	- [Table of contents](#table-of-contents)
	- [Overview](#overview)
		- [Description](#description)
		- [Actors](#actors)
		- [Sequence diagram](#sequence-diagram)
		- [Pagination and sorting](#pagination-and-sorting)
		    - [Request parametres](#request-parametres)
		    - [Response attributes](#response-attributes)
	- [Endpoints](#endpoints)
		- [GET /tpps  : Retrieve a list of TPP](#get-tpps-retrieve-a-list-of-tpp)
		- [GET  /tpps/{id}  : Retrieve a TPP](#get-tppsid-retrieve-a-tpp)
		- [GET /tpps/{id}/state (internal): Retrieve TPP State](#get-tppsidstate-retrieve-tpp-state)
		- [GET /tpps/{id}/accreditations (internal): Retrieve TPP accreditations](#get-tppsidaccreditations-retrieve-tpp-accreditations)
		- [POST  /tpps : Create a new TPP](#post-tpps-create-a-new-tpp)
		- [PUT /tpps/{id} : Update all properties of a TPP](#put-tppsid-update-all-properties-of-a-tpp)
		- [PATCH /tpps/{id} : Update selected properties of a TPP](#patch-tppsid-update-selected-properties-of-a-tpp)



# Description

**TPP management** is a component that manages the relations that an ASPSP has with a TPP.

The v1 release of this component implements :
- Registration relations between ASPSP and TPP.
- Trigger the generation of credentials that the TPP will use in order to call ASPSP APIs.
- Update the status of the relation.
- Management of services that can be accessed by the TPP.
- Check that a TPP has the right to use one of the ASPSP services.
- Monitor use of services by TPP.

 
# Actors

- **Account Servicing Payment Service Provider (ASPSP)**: An ASPSP is a PSP that provides and maintains a payment account for a payment services user (PSD 2 Article 4(15)).
- **Third Party Providers / Trusted Third Parties (TPP)**: A party other than an ASPSP that provides payment related services.
- **BackOffice User (BO)**: A worker of the ASPSP. To access all these resources, the BO must have an access token (Bank user token) issued by the ASPSP using a client credentials grant.

# Pagination and sorting

## Request parameters

Whenever an endpoint supports pagination and sorting, the following parameters can be provided :

| **Param**   | **Type**      | **Description**                                                       |
|-------------|---------------|-----------------------------------------------------------------------|
| `range`     | `String`      | Range of page requested                                               |
| `sort`      | `String`      | List of attributes where the list of resource will be sorted          |

## Example request

``` http
GET /cc/v1/tpps?range=0-50&sort=id HTTP/1.1
```

## Response attributes

| **Path**                                        | **Type**  | **Description**                                                                                                                                                                                                                                                                                                           |
| ----------------------------------------------- | --------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `[]`                                            | `Array`   | An array of object corresponding to the resource                                                                                                                                                                                                                                                                      |
     
## Response Headers
| **Header** | **Possible values** | 
|-------|-----------------|
| `Accept-Ranges` | `consents` |
| `Content-Range` | `consents range/*` or `tpps range/0`  if empty |

## Return Codes
| **HTTP Status** | **Description** |  
|-----------|------------|
| `206 Partial Content` | The request completed successfully|

# Endpoints

## GET /tpps  : Retrieve a list of TPP  

### Description

This endpoint allows to get all TPPs in relation with the ASPSP.

It is only accessible by a Back-Office User of the ASPSP (BO).

### Request Headers

| **Name**        | **Description**     | **Required**
| --------------- |---------------------|----------------
| `Authorization` |Holds bearer information.| *True*


### Response Headers

| **Header** | **Possible values** | 
|------------|-----------------|
| `Content-Type` | application/json |

### Pagination

This endpoint supports pagination. See the pagination request structure (here [Pagination section](#pagination-and-sorting))


### Response structure

| **Field** | **Occurence**| **Type** | **Description**|
|-------|----|-----------------|-------------|
|	`id`	| 1..1 |	String	|	Auto generated TPP identifier.
|	`name`	| 1..1 |	String	|	Name of the TPP.
|	`comments`	| 1..1 |	String	|	Some comments.
|	`accreditations`	| 1..1 |	Array	|	Array of accreditations. Could be any of these values : `AIS,PIS,CIS`.
|	`state.status`	| 1..1 |	String	|	TPP status could be any of these values `ACTIVE,BLOCKED`.
|	`state.monitored`	| 1..1 |	Boolean	|	Indicator checking if the TPP is monitored.
|	`contact.email`	| 1..1 |	String	|	Contact email address.
|	`contact.phoneNumber`	| 1..1 |	String	|	Contact phone number.
|	`contact.city`	| 1..1 |	String	|	Contact city.
|   `nationalAccreditationData.nationalId`                | 1..1 | String  | National identifier of the tpp                                                  |
|   `nationalAccreditationData.competentAuthorityName`    | 1..1 | String  | Name of the competent authority delivering the tpp national accreditation       |
|   `nationalAccreditationData.competentAuthorityCountry` | 1..1 | String  | Country of the competent authority delivering the tpp accreditation             |
|   `nationalAccreditationData.registrationDate`          | 1..1 | String  | Registration date of the national accreditation                                 |
|   `nationalAccreditationData.accreditations`            | 1..1 | Array   | Array of accreditations. Could be any of these values : AIS,PIS,CIS            |

### Pagination 

This endpoint supports pagination. See the pagination request structure (here [Pagination section](#pagination-and-sorting))

### Return & Error Codes

| **HTTP Status** | **Description** |  
|-----------|------------|
| `206 Partial Content` | The request completed successfully|
| `401 Unauthorized` | Authorization header missing or invalid token.|
| `400 Bad request` | Path identifier could be found but the TPP id was not found in database. |

### Example request

```http
GET /api/tpps?range=0-2 HTTP/1.1
Authorization: Bearer token
Host: localhost:8080
```
### Example response

```http
HTTP/1.1 206 Partial Content
Accept-Ranges: tpps
Content-Range: tpps 0-1/*
Content-Type: application/json;charset=UTF-8

[ {
  "id" : "a00000000000000000000000",
  "name" : "Oral B",
  "comments" : "Oral B is a blocked TPP.",
  "contact" : {
    "email" : "oralb@mail.com",
    "phoneNumber" : "0123456789",
    "city" : "Paris",
    "addressLines" : [ "32 rue des Chicos.", "1 rue de la Carie." ],
    "postCode" : "4321",
    "country" : "France"
  },
  "state" : {
    "status" : "BLOCKED",
    "monitored" : true
  },
  "accreditations" : [ ],
  "webSite" : "oralb.com",
  "nationalAccreditationData" : {
    "nationalId" : "oralBId",
    "competentAuthorityName" : "French registry",
    "competentAuthorityCountry" : "France",
    "registrationDate" : "2018-02-28",
    "accreditations" : [ ]
  },
  "physicalContacts" : [ {
    "contactType" : "BUSINESS_CONTACT",
    "name" : "Morandini",
    "firstName" : "Jean-marc",
    "jobTitle" : "Clown",
    "email" : "jeanmarc.morandini@oralb.com",
    "phoneNumber" : "0123456789"
  }, {
    "contactType" : "TECHNICAL_CONTACT",
    "name" : "Lively",
    "firstName" : "Blake",
    "jobTitle" : "CTO",
    "email" : "blake.lively@oralb.com",
    "phoneNumber" : "0123456789"
  } ]
}, {
  "id" : "a00000000000000000000001",
  "name" : "Activia",
  "comments" : "Activia is a TPP active without accreditations",
  "contact" : {
    "email" : "activia@mail.com",
    "phoneNumber" : "0123456789",
    "city" : "Dunkerque",
    "addressLines" : [ "1 rue de la Pie.", "2 boulevard de la legereté." ],
    "postCode" : "4321",
    "country" : "France"
  },
  "state" : {
    "status" : "ACTIVE",
    "monitored" : true
  },
  "accreditations" : [ ],
  "webSite" : "activia.com",
  "nationalAccreditationData" : {
    "nationalId" : "activiaId",
    "competentAuthorityName" : "French registry",
    "competentAuthorityCountry" : "France",
    "registrationDate" : "2018-01-27",
    "accreditations" : [ ]
  },
  "physicalContacts" : [ {
    "contactType" : "BUSINESS_CONTACT",
    "name" : "Charolaise",
    "firstName" : "Othman",
    "jobTitle" : "CEO",
    "email" : "othman.charolaise@activia.com",
    "phoneNumber" : "0123456789"
  }, {
    "contactType" : "TECHNICAL_CONTACT",
    "name" : "Angus",
    "firstName" : "Alex",
    "jobTitle" : "CTO",
    "email" : "angus.alex@activia.com",
    "phoneNumber" : "0123456789"
  } ]
} ]    
```

## GET  /tpps/{id}  : Retrieve a TPP 

### Description     

This endpoint allows to get a TPP, in relationship with the ASPSP, with its TPP identifier (tppId).

It is only accessible by a Back-Office User of the ASPSP (BO).

### Request header structure

| **Name**        | **Description**     | **Required**
| --------------- |---------------------|----------------
| `Authorization` |Holds bearer information. | *True*

### URI structure

`/tpps/{id}`

### Response Headers

| **Header** | **Possible values** | 
|------------|---------------------|
|`Content-Type`| application/json  |

### Response structure

|**Field** | **Possible values** | **Description** | 
|-------|-----------------|-------------|
|`id`	|String	|Auto generated TPP identifier.	|
|	`name`	|String	|Name of the TPP.	|
|	`comments`	|String	|Some comments.	|
|	`accreditations`	|String	|Array of accreditations. Could be any of these values : `AIS,PIS,CIS`.	|
|	`contact.email`	|String	|Contact email address.	|
|	`contact.phoneNumber`	|String	|Contact phone number.	|
|	`contact.city`	|String	|Contact city.	|
|   `nationalAccreditationData.nationalId`                | String  | National identifier of the tpp                                                  |
|   `nationalAccreditationData.competentAuthorityName`    | String  | Name of the competent authority delivering the tpp national accreditation       |
|   `nationalAccreditationData.competentAuthorityCountry` | String  | Country of the competent authority delivering the tpp accreditation             |
|   `nationalAccreditationData.registrationDate`          | String  | Registration date of the national accreditation                                 |
|   `nationalAccreditationData.accreditations`            | Array   | Array of accreditations. Could be any of these values : AIS,PIS,CIS            |
|	`state.status`	|String	|TPP status could be any of these values `ACTIVE,BLOCKED`.	|
|	`state.monitored`	|String	|Indicator checking if the TPP is monitored.	|

### Return & Error Codes

| **HTTP Status** | **Description** |  
|-----------|------------|
| `200 OK` | The request completed successfully|
| `401 Unauthorized` | Authorization header missing or invalid token.|
| `400 Bad request` | Path identifier could be found but the TPP id was not found in database. |

### Example request

```http
GET /tpps/59b13f31fb7b7dcdeb0becfe HTTP/1.1
Authorization: Bearer token
Host: localhost:8080
```
### Example response

```http
HTTP/1.1 200 OK
Content-Type: application/json;charset=UTF-8

        {
  "id" : "59b13f31fb7b7dcdeb0becfe",
  "name" : "ABC bank",
  "comments" : "ABC first bank",
  "contact" : {
    "email" : "abc@mail.com",
    "phoneNumber" : "0123456789",
    "city" : "Paris",
    "addressLines" : [ "random line", "road 10" ],
    "postCode" : "4321",
    "country" : "France"
  },
  "state" : {
    "status" : "ACTIVE",
    "monitored" : false
  },
  "accreditations" : [ "CIS", "AIS" ],
  "webSite" : "abc.com",
  "nationalAccreditationData" : {
    "nationalId" : "abcid",
    "competentAuthorityName" : "French registry",
    "competentAuthorityCountry" : "France",
    "registrationDate" : "2018-03-29",
    "accreditations" : [ "CIS", "AIS" ]
  },
  "physicalContacts" : [ {
    "contactType" : "BUSINESS_CONTACT",
    "name" : "Doe",
    "firstName" : "John",
    "jobTitle" : "CEO",
    "email" : "john.doe@solidarity.com",
    "phoneNumber" : "0123456789"
  }, {
    "contactType" : "TECHNICAL_CONTACT",
    "name" : "forest",
    "firstName" : "Jim",
    "jobTitle" : "CTO",
    "email" : "jim.forest@solidarity.com",
    "phoneNumber" : "0123456789"
  } ]
}
```

## GET /tpps/{id}/state : Retrieve TPP State

### Description 

This endpoint allows to retrieve a TPP state using the TPP identifier (tppId).

It is only accessible by a Back-Office User of the ASPSP (BO).

### Request header structure

| **Name**        | **Description**     | **Required**
| --------------- |---------------------|----------------
| `Authorization` |Holds bearer information. | *True*

### URI structure

`/tpps/{id}/state`

| **Parameter** | **Description**                       |
| --------- | --------------------------------- |
| `id`      | The TPP identifier (tppId). |


### Request body structure

No body.

### Response header structure

| **Header** | **Possible values** | 
|------------|---------------------|
| `Content-Type`| application/json |

### Response structure

| Field | Type 		| Description                   |
| ----------- | --------- | ------------------------------------------------------ |
| `status`    | `String`  | TPP status could be any of these values `ACTIVE,BLOCKED`. |
| `monitored` | `Boolean` | Indicator checking if the TPP is monitored.             |

### Return & Error Codes

| **HTTP Status** | **Description** |  
|-----------|------------|
| `200 OK` | The request completed successfully|
| `401 Unauthorized` | Authorization header missing or invalid token. |
| `400 Bad request` | Path identifier could be found but the TPP id was not found in database. |

### Example request

```http 
GET /api/tpps/a00000000000000000000000/state HTTP/1.1
Authorization: Bearer token
Host: localhost:8080
```

### Example response

```http
HTTP/1.1 200 OK
Content-Type: application/json;charset=UTF-8
{
  "status" : "BLOCKED",
  "monitored" : true
}
```


## GET /tpps/{id}/accreditations : Retrieve TPP accreditations

### Description 

This endpoint allows to retrieve a TPP state using the TPP identifier (tppId).

It is only accessible by a Back-Office User of the ASPSP (BO) and the TPP with an access token (_TPP token_).

### Request header structure

| **Name**        | **Description**     | **Required**
| --------------- |---------------------|----------------
| `Authorization` |Holds bearer information. | *True*

### URI structure

`/tpps/{id}/accreditations`

| **Parameter** | **Description**                       |
| --------- | --------------------------------- |
| `id`      | The TPP identifier (tppId). |


### Request body structure

No body.

### Response header structure

| **Header** | **Possible values** | 
|------------|---------------------|
| `Content-Type`| application/json |

### Response structure

| Field | Type 		| Description                   |
| ----------- | --------- | ------------------------------------------------------ |
| `[]`    | `Array`   | Array of accreditations. Could be any of these values : `AIS,PIS,CIS`. |


### Return & Error Codes

| **HTTP Status** | **Description** |  
|-----------|------------|
| `200 OK` | The request completed successfully|
| `401 Unauthorized` | Authorization header missing or invalid token. |
| `400 Bad request` | Path identifier could be found but the TPP id was not found in database. |

#### Example request

```http 
GET /api/tpps/a00000000000000000000002/accreditations HTTP/1.1
Authorization: Bearer token
Host: localhost:8080
```
#### Example response

```http
HTTP/1.1 200 OK
Content-Type: application/json;charset=UTF-8

[ "CIS", "AIS", "PIS" ]
```

## POST  /tpps : Create a new TPP

### Description

This endpoint allows a BO to create a new TPP.

It is only accessible by a Back-Office User of the ASPSP (BO).

### Request header structure

| **Name**        | **Description**     | **Required**
| --------------- |---------------------|----------------
| `Authorization` |Holds bearer information. | *True*
| `Content-Type`  |The type of object being sent : *application/json;charset=UTF-8*.| *True*

### Request body structure

| **Field**                  | **Possible Values**     | **Description**                                   |
| --------------------- | --------- | -------------------------------------------------------------------- |
| `id`                  | Null      | The id will be generated.                                             |
| `name`                | `String`  | Name of the TPP.                                                      |
| `comments`            | `String`  | Some comments.                                                        |
| `accreditations`      | `Array`   | Array of accreditations. Could be any of these values : `AIS,PIS,CIS`. |
| `contact.email`       | `String`  | Contact email address.                                                |
| `contact.phoneNumber` | `String`  | Contact phone number.                                                 |
| `contact.city`        | `String`  | Contact city.                                                         |
| `state.status`        | `String`  | TPP status could be any of these values `ACTIVE,BLOCKED`.               |
| `state.monitored`     | `Boolean` | Indicator checking if the TPP is monitored.                           |
| `nationalAccreditationData.nationalId`                | `String`  | National identifier of the tpp                                                  |
| `nationalAccreditationData.competentAuthorityName`    | `String`  | Name of the competent authority delivering the tpp national accreditation       |
| `nationalAccreditationData.competentAuthorityCountry` | `String`  | Country of the competent authority delivering the tpp accreditation             |
| `nationalAccreditationData.registrationDate`          | `String`  | Registration date of the national accreditation                                 |
| `nationalAccreditationData.accreditations`            | `Array`   | Array of accreditations. Could be any of these values : AIS,PIS,CIS            |


### Response header structure

| **Header** | **Possible values** | 
|-------|-----------------|
| `Content-Type` | application/json |

### Response structure

|Path                  | Type      | Description                                                          |
| --------------------- | --------- | -------------------------------------------------------------------- |
| `id`                  | `String`  | Auto generated TPP identifier.                                        |
| `name`                | `String`  | Name of the TPP.                                                      |
| `comments`            | `String`  | Some comments.                                                        |
| `accreditations`      | `Array`   | Array of accreditations. Could be any of these values : `AIS,PIS,CIS`. |
| `contact.email`       | `String`  | Contact email address.                                                |
| `contact.phoneNumber` | `String`  | Contact phone number.                                                 |
| `contact.city`        | `String`  | Contact city.                                                         |
| `state.status`        | `String`  | TPP status could be any of these values `ACTIVE,BLOCKED`.             |
| `state.monitored`     | `Boolean` | Indicator checking if the TPP is monitored.                           |
| `nationalAccreditationData.nationalId`                | `String`  | National identifier of the tpp                                                  |
| `nationalAccreditationData.competentAuthorityName`    | `String`  | Name of the competent authority delivering the tpp national accreditation       |
| `nationalAccreditationData.competentAuthorityCountry` | `String`  | Country of the competent authority delivering the tpp accreditation             |
| `nationalAccreditationData.registrationDate`          | `String`  | Registration date of the national accreditation                                 |
| `nationalAccreditationData.accreditations`            | `Array`   | Array of accreditations. Could be any of these values : AIS,PIS,CIS            |


### Return & Error Codes

| **HTTP Status** | **Description** |  
|-----------|------------|
| `201 Created` | Tpp created, completed successfully|
| `401 Unauthorized` | Authorization header missing or invalid token. |
| `400 Bad request` | Path identifier could be found but the TPP id was not found in database. |
| `409 API_ALREADY_EXIST` | The resource that a client tried to create already exists, try to use another nationalId or name. |

### Example request

```http
POST /api/tpps HTTP/1.1
Authorization: Bearer token
Content-Type: application/json;charset=UTF-8
Host: localhost:8080

{
  "id" : null,
  "name" : "Defense of the ancients bank",
  "comments" : "Not a bank for casuals",
  "contact" : {
    "email" : "dota'‘®mail.com",
    "phoneNumber" : "1337",
    "city" : "Seattle",
    "addressLines" : [ "random line", "road 10" ],
    "postCode" : "4321",
    "country" : "USA"
  },
  "state" : {
    "status" : "ACTIVE",
    "monitored" : true
  },
  "accreditations" : [ "AIS", "PIS" ],
  "webSite" : "dota.com",
  "nationalAccreditationData" : {
    "nationalId" : "dotaBank",
    "competentAuthorityName" : "USA registry",
    "competentAuthorityCountry" : "USA",
    "registrationDate" : "2018-03-29",
    "accreditations" : [ "AIS", "PIS" ]
  },
  "physicalContacts" : [ {
    "contactType" : "BUSINESS_CONTACT",
    "name" : "Doe",
    "firstName" : "John",
    "jobTitle" : "CEO",
    "email" : "john.doe@dota.com",
    "phoneNumber" : "0123456789"
  }, {
    "contactType" : "TECHNICAL_CONTACT",
    "name" : "forest",
    "firstName" : "Jim",
    "jobTitle" : "CTO",
    "email" : "jim.forest@dota.com",
    "phoneNumber" : "0123456789"
  } ]
}
```

### Example response

```http
HTTP/1.1 201 Created
Content-Type: application/json;charset=UTF-8

{
  "id" : "5b5ae164c19704b78d9dc9a4",
  "name" : "Defense of the ancients bank",
  "comments" : "Not a bank for casuals",
  "contact" : {
    "email" : "dota'‘®mail.com",
    "phoneNumber" : "1337",
    "city" : "Seattle",
    "addressLines" : [ "random line", "road 10" ],
    "postCode" : "4321",
    "country" : "USA"
  },
  "state" : {
    "status" : "ACTIVE",
    "monitored" : true
  },
  "accreditations" : [ "AIS", "PIS" ],
  "webSite" : "dota.com",
  "nationalAccreditationData" : {
    "nationalId" : "dotaBank",
    "competentAuthorityName" : "USA registry",
    "competentAuthorityCountry" : "USA",
    "registrationDate" : "2018-03-29",
    "accreditations" : [ "AIS", "PIS" ]
  },
  "physicalContacts" : [ {
    "contactType" : "BUSINESS_CONTACT",
    "name" : "Doe",
    "firstName" : "John",
    "jobTitle" : "CEO",
    "email" : "john.doe@dota.com",
    "phoneNumber" : "0123456789"
  }, {
    "contactType" : "TECHNICAL_CONTACT",
    "name" : "forest",
    "firstName" : "Jim",
    "jobTitle" : "CTO",
    "email" : "jim.forest@dota.com",
    "phoneNumber" : "0123456789"
  } ]
}
```




## PUT /tpps/{id} : Update all properties of a TPP

### Description

This endpoint allows to update every field of the TPP.

It is only accessible by a Back-Office User of the ASPSP (BO).

### Request header structure

| **Name**        | **Description**     | **Required**
| --------------- |---------------------|----------------
| `Authorization` |Holds bearer information. | *True*
| `Content-Type`  |The type of object being sent : *application/json;charset=UTF-8*.| *True*

### URI structure

`/tpps/{id}`

| **Parameter** | **Description**                       |
| --------- | --------------------------------- |
| `id`      | The TPP identifier (tppId). |

### Request body structure

| Path                  | Type      | Description                                                          |
| --------------------- | --------- | -------------------------------------------------------------------- |
| `id`                  | `String`  | Auto generated TPP identifier.                                        |
| `name`                | `String`  | Name of the TPP.                                                      |
| `comments`            | `String`  | Some comments.                                                        |
| `accreditations`      | `Array`   | Array of accreditations. Could be any of these values : `AIS,PIS,CIS`. |
| `contact.email`       | `String`  | Contact email address.                                                |
| `contact.phoneNumber` | `String`  | Contact phone number.                                                 |
| `contact.city`        | `String`  | Contact city.                                                         |
| `state.status`        | `String`  | TPP status could be any of these values `ACTIVE,BLOCKED`.              |
| `state.monitored`     | `Boolean` | Indicator checking if the TPP is monitored.                           |
| `nationalAccreditationData.nationalId`                | `String`  | National identifier of the tpp                                                  |
| `nationalAccreditationData.competentAuthorityName`    | `String`  | Name of the competent authority delivering the tpp national accreditation       |
| `nationalAccreditationData.competentAuthorityCountry` | `String`  | Country of the competent authority delivering the tpp accreditation             |
| `nationalAccreditationData.registrationDate`          | `String`  | Registration date of the national accreditation                                 |
| `nationalAccreditationData.accreditations`            | `Array`   | Array of accreditations. Could be any of these values : AIS,PIS,CIS            |


### Response header structure

| **Header** | **Possible values** | 
|-------|-----------------|
| `Content-Type` | application/json |

### Response structure

| Path                  | Type      | Description                                                          |
| --------------------- | --------- | -------------------------------------------------------------------- |
| `id`                  | `String`  | Auto generated TPP identifier.                                        |
| `name`                | `String`  | Name of the TPP.                                                      |
| `comments`            | `String`  | Some comments.                                                       |
| `accreditations`      | `Array`   | Array of accreditations. Could be any of these values : `AIS,PIS,CIS`. |
| `contact.email`       | `String`  | Contact email address.                                                |
| `contact.phoneNumber` | `String`  | Contact phone number.                                                 |
| `contact.city`        | `String`  | Contact city.                                                         |
| `state.status`        | `String`  | TPP status could be any of these values `ACTIVE,BLOCKED`.              |
| `state.monitored`     | `Boolean` | Indicator checking if the TPP is monitored.                           |
| `nationalAccreditationData.nationalId`                | `String`  | National identifier of the tpp                                                  |
| `nationalAccreditationData.competentAuthorityName`    | `String`  | Name of the competent authority delivering the tpp national accreditation       |
| `nationalAccreditationData.competentAuthorityCountry` | `String`  | Country of the competent authority delivering the tpp accreditation             |
| `nationalAccreditationData.registrationDate`          | `String`  | Registration date of the national accreditation                                 |
| `nationalAccreditationData.accreditations`            | `Array`   | Array of accreditations. Could be any of these values : AIS,PIS,CIS            |


### Return & Error Codes

| **HTTP Status** | **Description** |  
|-----------|------------|
| `200 OK` | The request completed successfully|
| `401 Unauthorized` | Authorization header missing or invalid token. |
| `400 Bad request` | Path identifier could be found but the TPP id was not found in database. |
| `409 API_ALREADY_EXIST` | The resource that a client tried to create already exists, try to use another nationalId or name. |


### Example request

```http
PUT /api/tpps/a00000000000000000000000 HTTP/1.1
Authorization: Bearer token
Content-Type: application/json;charset=UTF-8
Host: localhost:8080

{
  "id" : "a00000000000000000000000",
  "name" : "Updated name",
  "comments" : "Oral B is a blocked TPP.",
  "contact" : {
    "email" : "oralb@mail.com",
    "phoneNumber" : "0123456789",
    "city" : "Paris",
    "addressLines" : [ "32 rue des Chicos.", "1 rue de la Carie." ],
    "postCode" : "4321",
    "country" : "France"
  },
  "state" : {
    "status" : "BLOCKED",
    "monitored" : true
  },
  "accreditations" : [ ],
  "webSite" : "oralb.com",
  "nationalAccreditationData" : {
    "nationalId" : "updatedEbaId",
    "competentAuthorityName" : "French registry",
    "competentAuthorityCountry" : "France",
    "registrationDate" : "2018-02-28",
    "accreditations" : [ ]
  },
  "physicalContacts" : [ {
    "contactType" : "BUSINESS_CONTACT",
    "name" : "Morandini",
    "firstName" : "Jean-marc",
    "jobTitle" : "Clown",
    "email" : "jeanmarc.morandini@oralb.com",
    "phoneNumber" : "0123456789"
  }, {
    "contactType" : "TECHNICAL_CONTACT",
    "name" : "Lively",
    "firstName" : "Blake",
    "jobTitle" : "CTO",
    "email" : "blake.lively@oralb.com",
    "phoneNumber" : "0123456789"
  } ]
}
```

### Example response

```http
HTTP/1.1 200 OK
Content-Type: application/json;charset=UTF-8

{
  "id" : "a00000000000000000000000",
  "name" : "Updated name",
  "comments" : "Oral B is a blocked TPP.",
  "contact" : {
    "email" : "oralb@mail.com",
    "phoneNumber" : "0123456789",
    "city" : "Paris",
    "addressLines" : [ "32 rue des Chicos.", "1 rue de la Carie." ],
    "postCode" : "4321",
    "country" : "France"
  },
  "state" : {
    "status" : "BLOCKED",
    "monitored" : true
  },
  "accreditations" : [ ],
  "webSite" : "oralb.com",
  "nationalAccreditationData" : {
    "nationalId" : "updatedEbaId",
    "competentAuthorityName" : "French registry",
    "competentAuthorityCountry" : "France",
    "registrationDate" : "2018-02-28",
    "accreditations" : [ ]
  },
  "physicalContacts" : [ {
    "contactType" : "BUSINESS_CONTACT",
    "name" : "Morandini",
    "firstName" : "Jean-marc",
    "jobTitle" : "Clown",
    "email" : "jeanmarc.morandini@oralb.com",
    "phoneNumber" : "0123456789"
  }, {
    "contactType" : "TECHNICAL_CONTACT",
    "name" : "Lively",
    "firstName" : "Blake",
    "jobTitle" : "CTO",
    "email" : "blake.lively@oralb.com",
    "phoneNumber" : "0123456789"
  } ]
}
```

## PATCH /tpps/{id} : Update selected properties of a TPP

### Description

This endpoint allows to udpate choosen TPP fields. For instance, it can be used by the BO to modify only the state of the TPP.

It is only accessible by a Back-Office User of the ASPSP (BO).

### Request header structure

| **Name**        | **Description**     | **Required**
| --------------- |---------------------|----------------
| `Authorization` |Holds bearer information. | *True*
| `Content-Type`  |The type of object being sent : *application/json;charset=UTF-8*.| *True*

### URI structure

`/tpps/{id}`

| **Parameter** | **Description**                       |
| --------- | --------------------------------- |
| `id`      | The TPP identifier (tppId). |

### Request body structure

| Field                 | Possible Values      | Description                                                          |
| --------------------- | --------- | -------------------------------------------------------------------- |
| `id`                  | `String`  | Auto generated TPP identifier.                                        |
| `name`                | `String`  | Name of the TPP.                                                      |
| `comments`            | `String`  | Some comments.                                                        |
| `accreditations`      | `Array`   | Array of accreditations. Could be any of these values : `AIS,PIS,CIS`.|
| `contact`             | `Object`  | Contact details.                                                      |
| `state`               | `Object`  | State details.                                                        |
| `contact.email`       | `String`  | Contact email address.                                                |
| `contact.phoneNumber` | `String`  | Contact phone number .                                                |
| `contact.city`        | `String`  | Contact city.                                                         |
| `state.status`        | `String`  | TPP status could be any of these values `ACTIVE,BLOCKED`.             |
| `state.monitored`     | `Boolean` | Indicator checking if the TPP is monitored.                           |
| `nationalAccreditationData.nationalId`                | `String`  | National identifier of the tpp                                                  |
| `nationalAccreditationData.competentAuthorityName`    | `String`  | Name of the competent authority delivering the tpp national accreditation       |
| `nationalAccreditationData.competentAuthorityCountry` | `String`  | Country of the competent authority delivering the tpp accreditation             |
| `nationalAccreditationData.registrationDate`          | `String`  | Registration date of the national accreditation                                 |
| `nationalAccreditationData.accreditations`            | `Array`   | Array of accreditations. Could be any of these values : AIS,PIS,CIS            |

### Response header structure

| **Header** | **Possible values** | 
|------------|-----------------|
| `Content-Type` | application/json |

### Response structure

| Path                  | Type      | Description                                                       |
| --------------------- | --------- | -------------------------------------------------------------------- |
| `id`                  | `String`  | Auto generated TPP identifier.                                        |
| `name`                | `String`  | Name of the TPP.                                                      |
| `comments`            | `String`  | Some comments.                                                        |
| `accreditations`      | `Array`   | Array of accreditations. Could be any of these values : `AIS,PIS,CIS`.|
| `contact.email`       | `String`  | Contact email address.                                                |
| `contact.phoneNumber` | `String`  | Contact phone number.                                                |
| `contact.city`        | `String`  | Contact city.                                                        |
| `state.status`        | `String`  | TPP status could be any of these values `ACTIVE,BLOCKED`.              |
| `state.monitored`     | `Boolean` | Indicator checking if the TPP is monitored.                           |
| `nationalAccreditationData.nationalId`                | `String`  | National identifier of the tpp                                                  |
| `nationalAccreditationData.competentAuthorityName`    | `String`  | Name of the competent authority delivering the tpp national accreditation       |
| `nationalAccreditationData.competentAuthorityCountry` | `String`  | Country of the competent authority delivering the tpp accreditation             |
| `nationalAccreditationData.registrationDate`          | `String`  | Registration date of the national accreditation                                 |
| `nationalAccreditationData.accreditations`            | `Array`   | Array of accreditations. Could be any of these values : AIS,PIS,CIS            |


### Return & Error Codes

| **HTTP Status** | **Description** |  
|-----------|------------|
| `200 OK` | The request completed successfully|
| `401 Unauthorized` | Authorization header missing or invalid token. |
| `400 Bad request` | Path identifier could be found but the TPP id was not found in database. |
| `409 API_ALREADY_EXIST` | The resource that a client tried to create already exists, try to use another nationalId or name. |

### Example request

A request patching 'name' field

```http
PATCH /api/tpps/a00000000000000000000000 HTTP/1.1
Authorization: Bearer token
Content-Type: application/json;charset=UTF-8
Host: localhost:8080

{
  "id" : null,
  "name" : "updateName",
  "comments" : "UpDateCom",
  "contact" : null,
  "state" : null,
  "accreditations" : [ ],
  "webSite" : null,
  "nationalAccreditationData" : null,
  "physicalContacts" : [ ]
}
```

### Example response

```http
HTTP/1.1 200 OK
Content-Type: application/json;charset=UTF-8

{
  "id" : "a00000000000000000000000",
  "name" : "updateName",
  "comments" : "UpDateCom",
  "contact" : {
    "email" : "oralb@mail.com",
    "phoneNumber" : "0123456789",
    "city" : "Paris",
    "addressLines" : [ "32 rue des Chicos.", "1 rue de la Carie." ],
    "postCode" : "4321",
    "country" : "France"
  },
  "state" : {
    "status" : "BLOCKED",
    "monitored" : true
  },
  "accreditations" : [ ],
  "webSite" : "oralb.com",
  "nationalAccreditationData" : {
    "nationalId" : "ID_TPP_ORAL_B",
    "competentAuthorityName" : "French registry",
    "competentAuthorityCountry" : "France",
    "registrationDate" : "2018-02-28",
    "accreditations" : [ ]
  },
  "physicalContacts" : [ {
    "contactType" : "BUSINESS_CONTACT",
    "name" : "Morandini",
    "firstName" : "Jean-marc",
    "jobTitle" : "Clown",
    "email" : "jeanmarc.morandini@oralb.com",
    "phoneNumber" : "0123456789"
  }, {
    "contactType" : "TECHNICAL_CONTACT",
    "name" : "Lively",
    "firstName" : "Blake",
    "jobTitle" : "CTO",
    "email" : "blake.lively@oralb.com",
    "phoneNumber" : "0123456789"
  } ]
}
```


